import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rupp_application/app/modules/invoice_screen/bloc/get_invoice_bloc.dart';
import 'package:rupp_application/app/modules/login_screen/bloc/login_bloc.dart';
import 'package:rupp_application/app/modules/register/choose_department/bloc/get_major_bloc.dart';
import 'package:rupp_application/app/modules/register/choose_provinces/bloc/get_province_bloc.dart';
import 'package:rupp_application/app/modules/register/choose_secession/bloc/getsession_bloc.dart';
import 'package:rupp_application/app/modules/register/final_summary/bloc/create_registration_bloc.dart';
import 'package:rupp_application/app/modules/welcome_screen/bloc/user_info_bloc.dart';
import 'package:rupp_application/services/apis/get_invoice/get_invoice_api.dart';
import 'package:rupp_application/services/apis/get_province/get_province_api.dart';
import 'package:rupp_application/services/apis/get_session/get_session.dart';
import 'package:rupp_application/services/apis/get_user_profile/get_user_profile.dart';

import '../services/apis/create_registration/create_register_api.dart';
import '../services/apis/create_registration/create_registration_other_api.dart';
import '../services/apis/get_major_api/get_major_api.dart';
import '../services/apis/login_api/login_api.dart';

List<BlocProvider> _listBlocProvider = [
  BlocProvider<GetMajorBloc>(create: ((context) => GetMajorBloc( getMajor: GetMajorApi()))),
  BlocProvider<GetsessionBloc>(create: ((context) => GetsessionBloc( getSession: GetSession()))),
  BlocProvider<GetProvinceBloc>(create: ((context) => GetProvinceBloc( getProvinceApi: GetProvinceApi()))),
  BlocProvider<UserInfoBloc>(create: ((context) => UserInfoBloc( getUserInfoApi: GetUserInfoApi()))),
  BlocProvider<CreateRegistrationBloc>(create: ((context) => CreateRegistrationBloc(createRigis: CreateRegistration()))),
  BlocProvider<CreateRegistrationOtherBloc>(create: ((context) => CreateRegistrationOtherBloc(createRegisOther: CreateRegistrationOther()))),
  BlocProvider<LoginBloc>(create: ((context) => LoginBloc(loginApi: LoginApi()))),
  BlocProvider<GetInvoiceBloc>(create: ((context) => GetInvoiceBloc(getInvoiceApi: GetInvoice()))),
];

List<BlocProvider> get listBlocProvider => _listBlocProvider;
