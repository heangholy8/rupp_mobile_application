import 'package:flutter/material.dart';
import '../core/thems/thems_constands.dart';
import 'model_bottomSheet.dart';


void showtoastInternet(context){
  showModalBottomSheet(
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    context: context,
    isDismissible: true,
    builder: (context) => BuildBottomSheet(
      initialChildSize: 0.45,
      child:Container(
        margin: EdgeInsets.only(top: 18.0),
       child: Image.asset(
          "assets/gifs/no_connection.gif",
          width: MediaQuery.of(context).size.width/3,
          height: MediaQuery.of(context).size.height/3,
        ),
      ),
      expanded: Container(
        alignment: Alignment.center,
        child: Text("មិនមានអ៊ីនធឺណិត!",style: ThemeConstant.texttheme.headline6),
      ),
      
    ),
  );
}