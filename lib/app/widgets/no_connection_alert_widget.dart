import 'package:flutter/material.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:shimmer/shimmer.dart';
import '../core/thems/thems_constands.dart';

class NoConnectWidget extends StatelessWidget {
  const NoConnectWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          AnimatedSize(
            duration: const Duration(milliseconds: 800),
            reverseDuration: const Duration(milliseconds: 800),
            curve: Curves.easeInOut,
            child: Shimmer.fromColors(
              baseColor:const Color(0xFFC93D24),
              highlightColor:const Color(0xFFEC834B),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color:const Color(0xFFE64A1F),
                padding:const EdgeInsets.symmetric(vertical: 8.0),
                child: Text("មិនមានអ៊ីនធឺណិត",style: ThemeConstant.texttheme.headline6!.copyWith(color: Colors.transparent)),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.symmetric(vertical: 8.0,horizontal: 22.0),
            child: Row(
              children: [
                const Icon(Icons.wifi_off,size: 22.0,color: ColorContant.whiteColor,),
                const SizedBox(width: 8.0,),
                Text("មិនមានអ៊ីនធឺណិត",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.whiteColor)),
              ],
            ),
          ), 
        ],
      ),
    );
  }
}