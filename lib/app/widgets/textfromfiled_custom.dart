import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../core/constands/constands_color.dart';
import '../core/thems/thems_constands.dart';

class TextFromFiledWidget extends StatelessWidget {
  final ValueChanged<String>? onChange;
  final TextEditingController? controller;
  final TextInputType? keytype;
  final FocusNode? focusNode;
  const TextFromFiledWidget({Key? key, this.onChange, this.controller, this.keytype, this.focusNode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChange,
      controller: controller,
      textAlign: TextAlign.center,
      style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor),
      keyboardType: keytype,
      autofocus: true,
      focusNode: focusNode,
      decoration:const InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
          borderSide: BorderSide(color: ColorContant.primaryColor),
          ),
          focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
          borderSide: BorderSide(color: ColorContant.primaryColor),
          ),
          contentPadding: EdgeInsets.only(
            top: 12.0, bottom: 12.0),
              ),
    );
  }
}