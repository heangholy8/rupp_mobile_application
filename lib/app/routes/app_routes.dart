// ignore_for_file: constant_identifier_names

import 'package:rupp_application/app/modules/splash_screen/splash_screen.dart';

abstract class Routes {
  Routes._();
  static const OPTIONREGISANDLOGINSCREEN = _Paths.OPTIONREGISANDLOGIN;
  static const CHOOSEDEPARTMENTSCREEN = _Paths.CHOOSEDEPARTMENT;
  static const CHOOSESECESSIONSCREEN = _Paths.CHOOSESECESSION;
  static const CHOOSEPRICESCREEN = _Paths.CHOOSEPRICE;
  static const SUMMARYMAJORSCREEN = _Paths.SUMMARYMAJOR;
  static const SUMMARYGREADSCREEN = _Paths.SUMMARYGREAD;
  static const CHOOSEGREADSUBJSCREEN = _Paths.CHOOSEGREADSUBJ;
  static const CHOOSEGREADSCREEN = _Paths.CHOOSEGREAD;
  static const WELLCOMESCREEN = _Paths.WELLCOME;
  static const INVOICESCREEN = _Paths.INVOICE;
  //static const INVOICEDETAILSCREEN = _Paths.INVOICEDETAIL;
  static const FINALSUMMARYSCREEN = _Paths.FINALSUMMARY;
  static const CHOOSEPROVINCESCREEN = _Paths.CHOOSEPROVINCE;
  static const GENARATEUSERPASSSCREEN = _Paths.GENARATEUSERPASS;
  static const CHOOSEGENDERSCREEN = _Paths.CHOOSEGENDER;
  static const INPUTFRISTNAMESCREEN = _Paths.INPUTFRISTNAME;
  static const INPUTLASTNAMESCREEN = _Paths.INPUTLASTNAME;
  static const INPUTFRISTNAMELATINSCREEN = _Paths.INPUTFRISTNAMELATIN;
  static const INPUTLASTNAMELATINSCREEN = _Paths.INPUTLASTNAMELATIN;
  static const INPUTHIGHTSCHOOLSCREEN = _Paths.INPUTHIGHTSCHOOL;
  static const INPUTEXAMYYEARSCREEN = _Paths.INPUTEXAMYYEAR;
  static const INPUTIDENTITYSCREEN = _Paths.INPUTIDENTITY;
  static const INPUTIPHONENUMBERSCREEN = _Paths.INPUTIPHONENUMBER;
  static const INPUTIDATEOFBIRTHSCREEN = _Paths.INPUTIDATEOFBIRTH;
  static const VERIFICATIONSCREEN = _Paths.VERIFICATION;
  static const LOGNSCREEN = _Paths.LOGIN;
  static const SPLASHSCREEN = _Paths.SPLASH;
  static const INPUTAVERAGEEXAMSCREEN = _Paths.INPUTAVERAGEEXAM;
}

abstract class _Paths {
  static const OPTIONREGISANDLOGIN = '/option_register_and_login';
  static const CHOOSEDEPARTMENT = '/choose_department';
  static const CHOOSESECESSION = '/choose_secession';
  static const CHOOSEPRICE = '/choose_frice';
  static const SUMMARYMAJOR = '/summary_major';
  static const SUMMARYGREAD = '/summary_grade';
  static const CHOOSEGREADSUBJ = '/choose_grade_subject';
  static const CHOOSEGREAD = '/choose_grade';
  static const WELLCOME = '/wellcome';
  static const INVOICE = '/invoice_screen';
  //static const INVOICEDETAIL = '/invoice_detail_screen';
  static const FINALSUMMARY = '/final_summary_screen';
  static const CHOOSEPROVINCE = '/choose_province_screen';
  static const GENARATEUSERPASS = '/genarate_user_pass_screen';
  static const CHOOSEGENDER = '/choose_GENDER_subject';
  static const INPUTFRISTNAME = '/input_firstname';
  static const INPUTLASTNAME = '/input_lastname';
  static const INPUTFRISTNAMELATIN = '/input_firstname_latin';
  static const INPUTLASTNAMELATIN = '/input_lastname_latin';
  static const INPUTHIGHTSCHOOL = '/input_hight_school';
  static const INPUTEXAMYYEAR = '/input_exam_year';
  static const INPUTIDENTITY = '/input_identity';
  static const INPUTIPHONENUMBER= '/input_phone_number';
  static const INPUTIDATEOFBIRTH= '/input_date_of_birth';
  static const VERIFICATION= '/verification';
  static const LOGIN= '/login_screen';
  static const SPLASH= '/splash_screen';
  static const INPUTAVERAGEEXAM= '/input_average_exam';
}
