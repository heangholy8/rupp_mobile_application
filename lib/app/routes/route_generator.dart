import 'package:flutter/material.dart';
import 'package:rupp_application/app/modules/genarate_use_pass_screen/view/genarate_use_pass_screen.dart';
import 'package:rupp_application/app/modules/invoice_screen/view/invoice_screen.dart';
import 'package:rupp_application/app/modules/register/choose_department/view/choose_department_screen.dart';
import 'package:rupp_application/app/modules/register/choose_gender/view/choose_gender_screen.dart';
import 'package:rupp_application/app/modules/register/choose_price/view/choose_price_screen.dart';
import 'package:rupp_application/app/modules/register/choose_provinces/view/choose_province_screen.dart';
import 'package:rupp_application/app/modules/register/choose_secession/view/choose_secession_screen.dart';
import 'package:rupp_application/app/modules/register/final_summary/view/final_summary_screen.dart';
import 'package:rupp_application/app/modules/register/input_date_of_birth/view/input_date_of_birth_screen.dart';
import 'package:rupp_application/app/modules/register/input_fristname_latin/view/input_fristname_latin_screen.dart';
import 'package:rupp_application/app/modules/register/input_hight_school/view/input_hight_school_screen.dart';
import 'package:rupp_application/app/modules/register/input_identity/view/input_identity_screen.dart';
import 'package:rupp_application/app/modules/register/input_lastname/view/input_lastname_screen.dart';
import 'package:rupp_application/app/modules/register/input_lastname_latin/view/input_lastname_latin_screen.dart';
import 'package:rupp_application/app/modules/register/input_phone_number/view/input_phone_number_screen.dart';
import 'package:rupp_application/app/modules/register/input_year_exam/view/input_year_exam_screen.dart';
import 'package:rupp_application/app/modules/register/summary_grade_exam/view/summary_grade_exam.dart';
import 'package:rupp_application/app/modules/register/summary_major/view/summary_major_screen.dart';
import 'package:rupp_application/app/modules/register/verification_otp/view/verification_otp_screen.dart';
import 'package:rupp_application/app/modules/register_and_login/views/registration_and_login_screen.dart';
import 'package:rupp_application/app/modules/splash_screen/splash_screen.dart';
import 'package:rupp_application/app/modules/welcome_screen/view/welcome_screen.dart';
import 'package:rupp_application/app/routes/app_routes.dart';
import '../modules/login_screen/views/login_screen.dart';
import '../modules/register/choose_grade_exam/view/choose_grade_exam.dart';
import '../modules/register/choose_grade_exam_sub/view/choose_grade_exam_sub.dart';
import '../modules/register/input_average_exam/view/input_average_exam.dart';
import '../modules/register/input_fristname/view/input_fristname_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.OPTIONREGISANDLOGINSCREEN:
        return MaterialPageRoute(builder: (_) => const RegisterAndLoginOptionScreen());
      case Routes.CHOOSEDEPARTMENTSCREEN:
        return MaterialPageRoute(builder: (_) => const ChooseDepartmentScreen());
      case Routes.CHOOSESECESSIONSCREEN:
        return MaterialPageRoute(builder: (_) => const ChooseSecessionScreen());
      case Routes.CHOOSEPRICESCREEN:
        return MaterialPageRoute(builder: (_) => const ChoosePriceScreen());
      case Routes.SUMMARYMAJORSCREEN:
        return MaterialPageRoute(builder: (_) => const SummaryMajorScreen());
      case Routes.SUMMARYGREADSCREEN:
        return MaterialPageRoute(builder: (_) => const SummaryGradeScreen());
      case Routes.CHOOSEGREADSUBJSCREEN:
        return MaterialPageRoute(builder: (_) => const ChooseGradeSubScreen());
      case Routes.CHOOSEGREADSCREEN:
        return MaterialPageRoute(builder: (_) => const ChooseGradeScreen());
      case Routes.WELLCOMESCREEN:
        return MaterialPageRoute(builder: (_) => const WellcomeScreen());
      case Routes.INVOICESCREEN:
        return MaterialPageRoute(builder: (_) =>  InvoiceScreen(routType: 1,));
      // case Routes.INVOICEDETAILSCREEN:
      //   return MaterialPageRoute(builder: (_) => const InvoiceDetailScreen());
      case Routes.FINALSUMMARYSCREEN:
        return MaterialPageRoute(builder: (_) => const SummaryFinalScreen());
      case Routes.CHOOSEPROVINCESCREEN:
        return MaterialPageRoute(builder: (_) => const ChooseProvinceScreen());
      case Routes.GENARATEUSERPASSSCREEN:
        return MaterialPageRoute(builder: (_) => const GenarateUserPassScreen());
      case Routes.CHOOSEGENDERSCREEN:
        return MaterialPageRoute(builder: (_) => const ChooseGenderScreen());
      case Routes.INPUTFRISTNAMESCREEN:
        return MaterialPageRoute(builder: (_) => const InputFristnameScreen());
      case Routes.INPUTLASTNAMESCREEN:
        return MaterialPageRoute(builder: (_) => const InputLastnameScreen());
      case Routes.INPUTFRISTNAMELATINSCREEN:
        return MaterialPageRoute(builder: (_) => const InputFristnameLatinScreen());
      case Routes.INPUTLASTNAMELATINSCREEN:
        return MaterialPageRoute(builder: (_) => const InputLastnameLatinScreen());
      case Routes.INPUTHIGHTSCHOOLSCREEN:
        return MaterialPageRoute(builder: (_) => const InputHightSchoolScreen());
      case Routes.INPUTEXAMYYEARSCREEN:
        return MaterialPageRoute(builder: (_) => const InputYearExamScreen());
      case Routes.INPUTIDENTITYSCREEN:
        return MaterialPageRoute(builder: (_) => const InputIdentityScreen());
      case Routes.INPUTIPHONENUMBERSCREEN:
        return MaterialPageRoute(builder: (_) => const InputPhoneNumberScreen());
      case Routes.INPUTIDATEOFBIRTHSCREEN:
        return MaterialPageRoute(builder: (_) => const InputDateOfBirthScreen());
      case Routes.VERIFICATIONSCREEN:
        return MaterialPageRoute(builder: (_) => const VerificationScren());
      case Routes.LOGNSCREEN:
        return MaterialPageRoute(builder: (_) => const LoginScreen());
      case Routes.SPLASHSCREEN:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      case Routes.INPUTAVERAGEEXAMSCREEN:
        return MaterialPageRoute(builder: (_) => const InputAveragrExamScreen());
      // case Routes.SUMMARYGREADSCREEN:
      //   return PageRouteBuilder(pageBuilder: (context, animation, _) => const SummaryGradeScreen(),
      //   transitionsBuilder: (context, animation, _, child) {
      //       const begin = Offset(0.0, 1.0);
      //       const end = Offset.zero;
      //       const curve = Curves.ease;

      //       var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      //       return SlideTransition(
      //         position: animation.drive(tween),
      //         child: child,
      //       );
      //     },
      //   );
      default:
        return _errorRoute();
    }
  }
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (context) => Scaffold(
        appBar: AppBar(
          title: const Text("Error"),
          centerTitle: true,
        ),
        body: const Center(
          child: Text("Page not found"),
        ),
      ),
    );
  }
}
