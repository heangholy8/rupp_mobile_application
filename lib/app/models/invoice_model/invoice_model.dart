// To parse this JSON data, do
//
//     final invoiceModel = invoiceModelFromJson(jsonString);

import 'dart:convert';

InvoiceModel invoiceModelFromJson(String str) => InvoiceModel.fromJson(json.decode(str));

String invoiceModelToJson(InvoiceModel data) => json.encode(data.toJson());

class InvoiceModel {
    InvoiceModel({
        this.status,
        this.data,
    });

    bool? status;
    List<Datum>? data;

    factory InvoiceModel.fromJson(Map<String, dynamic> json) => InvoiceModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.enrollmentId,
        this.studentId,
        this.studentFullname,
        this.majorId,
        this.majorName,
        this.departmentId,
        this.departmentName,
        this.stage,
        this.academicyear,
        this.shiftSessionId,
        this.shiftSessionName,
        this.registrationDate,
        this.expiredDate,
        this.payments,
    });

    int? enrollmentId;
    int? studentId;
    String? studentFullname;
    int? majorId;
    String? majorName;
    int? departmentId;
    String? departmentName;
    String? stage;
    String? academicyear;
    int? shiftSessionId;
    String? shiftSessionName;
    String? registrationDate;
    String? expiredDate;
    List<Payment>? payments;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        enrollmentId: json["enrollment_id"],
        studentId: json["student_id"],
        studentFullname: json["student_fullname"],
        majorId: json["major_id"],
        majorName: json["major_name"],
        departmentId: json["department_id"],
        departmentName: json["department_name"],
        stage: json["stage"],
        academicyear: json["academicyear"],
        shiftSessionId: json["shift_session_id"],
        shiftSessionName: json["shift_session_name"],
        registrationDate: json["registration_date"],
        expiredDate: json["expired_date"],
        payments: List<Payment>.from(json["payments"].map((x) => Payment.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "enrollment_id": enrollmentId,
        "student_id": studentId,
        "student_fullname": studentFullname,
        "major_id": majorId,
        "major_name": majorName,
        "department_id": departmentId,
        "department_name": departmentName,
        "stage": stage,
        "academicyear": academicyear,
        "shift_session_id": shiftSessionId,
        "shift_session_name": shiftSessionName,
        "registration_date": registrationDate,
        "expired_date": expiredDate,
        "payments": List<dynamic>.from(payments!.map((x) => x.toJson())),
    };
}

class Payment {
    Payment({
        this.paymentCode,
        this.invoiceNumber,
        this.totalAmount,
        this.status,
        this.paymentCodeBarCode,
        this.items,
    });

    String? paymentCode;
    String? invoiceNumber;
    int? totalAmount;
    String? status;
    String? paymentCodeBarCode;
    List<Item>? items;

    factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        paymentCode: json["payment_code"],
        invoiceNumber: json["invoice_number"],
        totalAmount: json["total_amount"],
        status: json["status"],
        paymentCodeBarCode: json["payment_code_bar_code"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "payment_code": paymentCode,
        "invoice_number": invoiceNumber,
        "total_amount": totalAmount,
        "status": status,
        "payment_code_bar_code": paymentCodeBarCode,
        "items": List<dynamic>.from(items!.map((x) => x.toJson())),
    };
}

class Item {
    Item({
        this.feeCategoryName,
        this.feeCategoryId,
        this.semester,
        this.amount,
        this.currency,
        this.status,
    });

    String? feeCategoryName;
    int? feeCategoryId;
    String? semester;
    int? amount;
    String? currency;
    String? status;

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        feeCategoryName: json["fee_category_name"],
        feeCategoryId: json["fee_category_id"],
        semester: json["semester"] == null ? null : json["semester"],
        amount: json["amount"],
        currency: json["currency"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "fee_category_name": feeCategoryName,
        "fee_category_id": feeCategoryId,
        "semester": semester == null ? null : semester,
        "amount": amount,
        "currency": currency,
        "status": status,
    };
}
