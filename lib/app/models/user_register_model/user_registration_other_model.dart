// To parse this JSON data, do
//
//     final createOtherRegistrationModel = createOtherRegistrationModelFromJson(jsonString);

import 'dart:convert';

CreateOtherRegistrationModel createOtherRegistrationModelFromJson(String str) => CreateOtherRegistrationModel.fromJson(json.decode(str));

String createOtherRegistrationModelToJson(CreateOtherRegistrationModel data) => json.encode(data.toJson());

class CreateOtherRegistrationModel {
    CreateOtherRegistrationModel({
        this.status,
        this.data,
    });

    bool? status;
    CreateOtherRegistrationModelData? data;

    factory CreateOtherRegistrationModel.fromJson(Map<String, dynamic> json) => CreateOtherRegistrationModel(
        status: json["status"],
        data: CreateOtherRegistrationModelData.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data!.toJson(),
    };
}

class CreateOtherRegistrationModelData {
    CreateOtherRegistrationModelData({
        this.status,
        this.data,
    });

    bool? status;
    DataData? data;

    factory CreateOtherRegistrationModelData.fromJson(Map<String, dynamic> json) => CreateOtherRegistrationModelData(
        status: json["status"],
        data: DataData.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data!.toJson(),
    };
}

class DataData {
    DataData({
        this.studentId,
        this.ruppStudentId,
        this.facultyId,
        this.departmentId,
        this.majorId,
        this.academicyearId,
        this.stageId,
        this.degreeId,
        this.shiftSessionId,
        this.updatedAt,
        this.createdAt,
        this.id,
        this.getStudent,
        this.getDegree,
        this.getMajor,
        this.getDepartment,
        this.getAcademicyear,
        this.getStage,
        this.getShiftSession,
    });

    String? studentId;
    String? ruppStudentId;
    int? facultyId;
    int? departmentId;
    String? majorId;
    String? academicyearId;
    String? stageId;
    String? degreeId;
    String? shiftSessionId;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;
    GetStudent? getStudent;
    GetDegree? getDegree;
    GetMajor? getMajor;
    GetDepartment? getDepartment;
    GetAcademicyear? getAcademicyear;
    GetStage? getStage;
    GetShiftSession? getShiftSession;

    factory DataData.fromJson(Map<String, dynamic> json) => DataData(
        studentId: json["student_id"],
        ruppStudentId: json["rupp_student_id"],
        facultyId: json["faculty_id"],
        departmentId: json["department_id"],
        majorId: json["major_id"],
        academicyearId: json["academicyear_id"],
        stageId: json["stage_id"],
        degreeId: json["degree_id"],
        shiftSessionId: json["shift_session_id"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
        getStudent: GetStudent.fromJson(json["get_student"]),
        getDegree: GetDegree.fromJson(json["get_degree"]),
        getMajor: GetMajor.fromJson(json["get_major"]),
        getDepartment: GetDepartment.fromJson(json["get_department"]),
        getAcademicyear: GetAcademicyear.fromJson(json["get_academicyear"]),
        getStage: GetStage.fromJson(json["get_stage"]),
        getShiftSession: GetShiftSession.fromJson(json["get_shift_session"]),
    );

    Map<String, dynamic> toJson() => {
        "student_id": studentId,
        "rupp_student_id": ruppStudentId,
        "faculty_id": facultyId,
        "department_id": departmentId,
        "major_id": majorId,
        "academicyear_id": academicyearId,
        "stage_id": stageId,
        "degree_id": degreeId,
        "shift_session_id": shiftSessionId,
        "updated_at": updatedAt!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
        "id": id,
        "get_student": getStudent!.toJson(),
        "get_degree": getDegree!.toJson(),
        "get_major": getMajor!.toJson(),
        "get_department": getDepartment!.toJson(),
        "get_academicyear": getAcademicyear!.toJson(),
        "get_stage": getStage!.toJson(),
        "get_shift_session": getShiftSession!.toJson(),
    };
}

class GetAcademicyear {
    GetAcademicyear({
        this.id,
        this.name,
        this.nameEn,
        this.startYear,
        this.endYear,
        this.sortkey,
        this.isActive,
        this.isUsed,
        this.isNextActive,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? name;
    String? nameEn;
    int? startYear;
    int? endYear;
    int? sortkey;
    int? isActive;
    int? isUsed;
    int? isNextActive;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory GetAcademicyear.fromJson(Map<String, dynamic> json) => GetAcademicyear(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        startYear: json["start_year"],
        endYear: json["end_year"],
        sortkey: json["sortkey"],
        isActive: json["is_active"],
        isUsed: json["is_used"],
        isNextActive: json["is_next_active"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "start_year": startYear,
        "end_year": endYear,
        "sortkey": sortkey,
        "is_active": isActive,
        "is_used": isUsed,
        "is_next_active": isNextActive,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

class GetDegree {
    GetDegree({
        this.id,
        this.code,
        this.name,
        this.nameEn,
        this.sortkey,
        this.studyDurationYears,
        this.studyDurationSemesters,
        this.studyDurationRulesFullTime,
        this.studyDurationRulesPartTime,
        this.maxYearCredit,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? code;
    String? name;
    String? nameEn;
    int? sortkey;
    int? studyDurationYears;
    int? studyDurationSemesters;
    int? studyDurationRulesFullTime;
    int? studyDurationRulesPartTime;
    int? maxYearCredit;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory GetDegree.fromJson(Map<String, dynamic> json) => GetDegree(
        id: json["id"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        sortkey: json["sortkey"],
        studyDurationYears: json["study_duration_years"],
        studyDurationSemesters: json["study_duration_semesters"],
        studyDurationRulesFullTime: json["study_duration_rules_full_time"],
        studyDurationRulesPartTime: json["study_duration_rules_part_time"],
        maxYearCredit: json["max_year_credit"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "sortkey": sortkey,
        "study_duration_years": studyDurationYears,
        "study_duration_semesters": studyDurationSemesters,
        "study_duration_rules_full_time": studyDurationRulesFullTime,
        "study_duration_rules_part_time": studyDurationRulesPartTime,
        "max_year_credit": maxYearCredit,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

class GetDepartment {
    GetDepartment({
        this.id,
        this.facultyId,
        this.code,
        this.nameEn,
        this.name,
        this.isCorrect,
        this.inactive,
        this.notDelete,
        this.objectType,
        this.sortkey,
        this.nameForCertificate,
        this.nameForCertificateEn,
        this.updatedAt,
        this.createdAt,
    });

    int? id;
    int? facultyId;
    String? code;
    String? nameEn;
    String? name;
    int? isCorrect;
    int? inactive;
    int? notDelete;
    String? objectType;
    int? sortkey;
    String? nameForCertificate;
    String? nameForCertificateEn;
    DateTime? updatedAt;
    DateTime? createdAt;

    factory GetDepartment.fromJson(Map<String, dynamic> json) => GetDepartment(
        id: json["id"],
        facultyId: json["faculty_id"],
        code: json["code"],
        nameEn: json["name_en"],
        name: json["name"],
        isCorrect: json["is_correct"],
        inactive: json["inactive"],
        notDelete: json["not_delete"],
        objectType: json["object_type"],
        sortkey: json["sortkey"],
        nameForCertificate: json["name_for_certificate"],
        nameForCertificateEn: json["name_for_certificate_en"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "faculty_id": facultyId,
        "code": code,
        "name_en": nameEn,
        "name": name,
        "is_correct": isCorrect,
        "inactive": inactive,
        "not_delete": notDelete,
        "object_type": objectType,
        "sortkey": sortkey,
        "name_for_certificate": nameForCertificate,
        "name_for_certificate_en": nameForCertificateEn,
        "updated_at": updatedAt!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
    };
}

class GetMajor {
    GetMajor({
        this.id,
        this.name,
        this.nameEn,
        this.code,
        this.facultyId,
        this.inactive,
        this.departmentId,
        this.degreeId,
        this.isCorrect,
        this.stageIds,
        this.shiftSessionIds,
        this.description,
        this.descriptionEn,
        this.overview,
        this.overviewEn,
        this.introduction,
        this.introductionEn,
        this.registrationRequiredExam,
        this.notDelete,
        this.educationSystem,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? name;
    String? nameEn;
    String? code;
    int? facultyId;
    int? inactive;
    int? departmentId;
    int? degreeId;
    int? isCorrect;
    String? stageIds;
    dynamic shiftSessionIds;
    dynamic description;
    dynamic descriptionEn;
    String? overview;
    String? overviewEn;
    String? introduction;
    String? introductionEn;
    int? registrationRequiredExam;
    int? notDelete;
    int? educationSystem;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory GetMajor.fromJson(Map<String, dynamic> json) => GetMajor(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        code: json["code"],
        facultyId: json["faculty_id"],
        inactive: json["inactive"],
        departmentId: json["department_id"],
        degreeId: json["degree_id"],
        isCorrect: json["is_correct"],
        stageIds: json["stage_ids"],
        shiftSessionIds: json["shift_session_ids"],
        description: json["description"],
        descriptionEn: json["description_en"],
        overview: json["overview"],
        overviewEn: json["overview_en"],
        introduction: json["introduction"],
        introductionEn: json["introduction_en"],
        registrationRequiredExam: json["registration_required_exam"],
        notDelete: json["not_delete"],
        educationSystem: json["education_system"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "code": code,
        "faculty_id": facultyId,
        "inactive": inactive,
        "department_id": departmentId,
        "degree_id": degreeId,
        "is_correct": isCorrect,
        "stage_ids": stageIds,
        "shift_session_ids": shiftSessionIds,
        "description": description,
        "description_en": descriptionEn,
        "overview": overview,
        "overview_en": overviewEn,
        "introduction": introduction,
        "introduction_en": introductionEn,
        "registration_required_exam": registrationRequiredExam,
        "not_delete": notDelete,
        "education_system": educationSystem,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

class GetShiftSession {
    GetShiftSession({
        this.id,
        this.code,
        this.name,
        this.nameEn,
        this.keyValue,
        this.sortkey,
        this.startTime,
        this.endTime,
        this.endTimeSchedule,
        this.breakTimeStart,
        this.breakTimeEnd,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? code;
    String? name;
    String? nameEn;
    String? keyValue;
    int? sortkey;
    String? startTime;
    String? endTime;
    String? endTimeSchedule;
    String? breakTimeStart;
    dynamic breakTimeEnd;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory GetShiftSession.fromJson(Map<String, dynamic> json) => GetShiftSession(
        id: json["id"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        keyValue: json["key_value"],
        sortkey: json["sortkey"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        endTimeSchedule: json["end_time_schedule"],
        breakTimeStart: json["break_time_start"],
        breakTimeEnd: json["break_time_end"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "key_value": keyValue,
        "sortkey": sortkey,
        "start_time": startTime,
        "end_time": endTime,
        "end_time_schedule": endTimeSchedule,
        "break_time_start": breakTimeStart,
        "break_time_end": breakTimeEnd,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

class GetStage {
    GetStage({
        this.id,
        this.code,
        this.name,
        this.nameEn,
        this.sortkey,
        this.termCount,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? code;
    String? name;
    String? nameEn;
    int? sortkey;
    int? termCount;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory GetStage.fromJson(Map<String, dynamic> json) => GetStage(
        id: json["id"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        sortkey: json["sortkey"],
        termCount: json["term_count"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "sortkey": sortkey,
        "term_count": termCount,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

class GetStudent {
    GetStudent({
        this.id,
        this.userId,
        this.code,
        this.bId,
        this.ruppCardId,
        this.fullname,
        this.fullnameEn,
        this.firstname,
        this.firstnameEn,
        this.lastname,
        this.lastnameEn,
        this.gender,
        this.birthOrder,
        this.dateOfBirth,
        this.placeOfBirth,
        this.cardId,
        this.email,
        this.s3FileUrl,
        this.fatherName,
        this.fatherBirthDate,
        this.fatherNationality,
        this.fatherEthnicity,
        this.fatherDeadAlive,
        this.fatherOccupation,
        this.motherName,
        this.motherBirthDate,
        this.motherNationality,
        this.motherEthnicity,
        this.motherDeadAlive,
        this.motherOccupation,
        this.totalSiblings,
        this.totalSiblingsBoy,
        this.totalSiblingsGirl,
        this.placeOfBirthProvinceId,
        this.provinceCityId,
        this.isForeigner,
        this.idCardPassport,
        this.phone,
        this.single,
        this.nationalityId,
        this.nationality,
        this.ethnicity,
        this.ethnicityId,
        this.currentAddress,
        this.currentOccupation,
        this.whereOccupation,
        this.highschoolExamDate,
        this.fromHighschool,
        this.highschoolCertificateNumber,
        this.namePrimarySchool,
        this.primarySchoolProvinceCityId,
        this.primarySchoolYear,
        this.nameSecondarySchool,
        this.secondarySchoolProvinceCityId,
        this.secondarySchoolYear,
        this.nameHighSchool,
        this.highschoolExamPlace,
        this.highSchoolProvinceCityId,
        this.highSchoolYear,
        this.nameUniversity,
        this.universityProvinceCityId,
        this.universityYear,
        this.schoolCertificateYear,
        this.certificatLevelYear,
        this.step,
        this.personalInfoStep,
        this.test,
        this.isIndividualCreate,
        this.isIndividualExisted,
        this.isRegisterWithBanhji,
        this.isDelete,
        this.staffDeleterId,
        this.placeOfBirthEn,
        this.fatherNameEn,
        this.currentAddressEn,
        this.fatherOccupationEn,
        this.motherNameEn,
        this.motherOccupationEn,
        this.currentAddressProvinceCityId,
        this.familyStatus,
        this.passHighSchoolYear,
        this.isMigration,
        this.createDescription,
        this.oldStudentId,
        this.allowPaymentType,
        this.deleteUserId,
        this.scoreOrientedSubjectData,
        this.certificateGradeScore,
        this.certificateGrading,
        this.certificateYear,
        this.certificateNumber,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    int? userId;
    dynamic code;
    dynamic bId;
    String? ruppCardId;
    String? fullname;
    String? fullnameEn;
    String? firstname;
    String? firstnameEn;
    String? lastname;
    String? lastnameEn;
    int? gender;
    dynamic birthOrder;
    DateTime? dateOfBirth;
    dynamic placeOfBirth;
    dynamic cardId;
    String? email;
    dynamic s3FileUrl;
    dynamic fatherName;
    dynamic fatherBirthDate;
    dynamic fatherNationality;
    dynamic fatherEthnicity;
    dynamic fatherDeadAlive;
    dynamic fatherOccupation;
    dynamic motherName;
    dynamic motherBirthDate;
    dynamic motherNationality;
    dynamic motherEthnicity;
    dynamic motherDeadAlive;
    dynamic motherOccupation;
    dynamic totalSiblings;
    dynamic totalSiblingsBoy;
    dynamic totalSiblingsGirl;
    dynamic placeOfBirthProvinceId;
    int? provinceCityId;
    int? isForeigner;
    String? idCardPassport;
    String? phone;
    dynamic single;
    dynamic nationalityId;
    dynamic nationality;
    dynamic ethnicity;
    dynamic ethnicityId;
    String? currentAddress;
    dynamic currentOccupation;
    dynamic whereOccupation;
    dynamic highschoolExamDate;
    dynamic fromHighschool;
    String? highschoolCertificateNumber;
    dynamic namePrimarySchool;
    dynamic primarySchoolProvinceCityId;
    dynamic primarySchoolYear;
    dynamic nameSecondarySchool;
    dynamic secondarySchoolProvinceCityId;
    dynamic secondarySchoolYear;
    String? nameHighSchool;
    dynamic highschoolExamPlace;
    dynamic highSchoolProvinceCityId;
    String? highSchoolYear;
    dynamic nameUniversity;
    dynamic universityProvinceCityId;
    dynamic universityYear;
    dynamic schoolCertificateYear;
    dynamic certificatLevelYear;
    int? step;
    int? personalInfoStep;
    dynamic test;
    int? isIndividualCreate;
    int? isIndividualExisted;
    dynamic isRegisterWithBanhji;
    int? isDelete;
    dynamic staffDeleterId;
    dynamic placeOfBirthEn;
    dynamic fatherNameEn;
    String? currentAddressEn;
    dynamic fatherOccupationEn;
    dynamic motherNameEn;
    dynamic motherOccupationEn;
    dynamic currentAddressProvinceCityId;
    int? familyStatus;
    dynamic passHighSchoolYear;
    int? isMigration;
    dynamic createDescription;
    dynamic oldStudentId;
    dynamic allowPaymentType;
    dynamic deleteUserId;
    String? scoreOrientedSubjectData;
    String? certificateGradeScore;
    String? certificateGrading;
    String? certificateYear;
    String? certificateNumber;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory GetStudent.fromJson(Map<String, dynamic> json) => GetStudent(
        id: json["id"],
        userId: json["user_id"],
        code: json["code"],
        bId: json["b_id"],
        ruppCardId: json["rupp_card_id"],
        fullname: json["fullname"],
        fullnameEn: json["fullname_en"],
        firstname: json["firstname"],
        firstnameEn: json["firstname_en"],
        lastname: json["lastname"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        birthOrder: json["birth_order"],
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        placeOfBirth: json["place_of_birth"],
        cardId: json["card_id"],
        email: json["email"],
        s3FileUrl: json["s3_file_url"],
        fatherName: json["father_name"],
        fatherBirthDate: json["father_birth_date"],
        fatherNationality: json["father_nationality"],
        fatherEthnicity: json["father_ethnicity"],
        fatherDeadAlive: json["father_dead_alive"],
        fatherOccupation: json["father_occupation"],
        motherName: json["mother_name"],
        motherBirthDate: json["mother_birth_date"],
        motherNationality: json["mother_nationality"],
        motherEthnicity: json["mother_ethnicity"],
        motherDeadAlive: json["mother_dead_alive"],
        motherOccupation: json["mother_occupation"],
        totalSiblings: json["total_siblings"],
        totalSiblingsBoy: json["total_siblings_boy"],
        totalSiblingsGirl: json["total_siblings_girl"],
        placeOfBirthProvinceId: json["place_of_birth_province_id"],
        provinceCityId: json["province_city_id"],
        isForeigner: json["is_foreigner"],
        idCardPassport: json["id_card_passport"],
        phone: json["phone"],
        single: json["single"],
        nationalityId: json["nationality_id"],
        nationality: json["nationality"],
        ethnicity: json["ethnicity"],
        ethnicityId: json["ethnicity_id"],
        currentAddress: json["current_address"],
        currentOccupation: json["current_occupation"],
        whereOccupation: json["where_occupation"],
        highschoolExamDate: json["highschool_exam_date"],
        fromHighschool: json["from_highschool"],
        highschoolCertificateNumber: json["highschool_certificate_number"],
        namePrimarySchool: json["name_primary_school"],
        primarySchoolProvinceCityId: json["primary_school_province_city_id"],
        primarySchoolYear: json["primary_school_year"],
        nameSecondarySchool: json["name_secondary_school"],
        secondarySchoolProvinceCityId: json["secondary_school_province_city_id"],
        secondarySchoolYear: json["secondary_school_year"],
        nameHighSchool: json["name_high_school"],
        highschoolExamPlace: json["highschool_exam_place"],
        highSchoolProvinceCityId: json["high_school_province_city_id"],
        highSchoolYear: json["high_school_year"],
        nameUniversity: json["name_university"],
        universityProvinceCityId: json["university_province_city_id"],
        universityYear: json["university_year"],
        schoolCertificateYear: json["school_certificate_year"],
        certificatLevelYear: json["certificat_level_year"],
        step: json["step"],
        personalInfoStep: json["personal_info_step"],
        test: json["test"],
        isIndividualCreate: json["is_individual_create"],
        isIndividualExisted: json["is_individual_existed"],
        isRegisterWithBanhji: json["is_register_with_banhji"],
        isDelete: json["is_delete"],
        staffDeleterId: json["staff_deleter_id"],
        placeOfBirthEn: json["place_of_birth_en"],
        fatherNameEn: json["father_name_en"],
        currentAddressEn: json["current_address_en"],
        fatherOccupationEn: json["father_occupation_en"],
        motherNameEn: json["mother_name_en"],
        motherOccupationEn: json["mother_occupation_en"],
        currentAddressProvinceCityId: json["current_address_province_city_id"],
        familyStatus: json["family_status"],
        passHighSchoolYear: json["pass_high_school_year"],
        isMigration: json["is_migration"],
        createDescription: json["create_description"],
        oldStudentId: json["old_student_id"],
        allowPaymentType: json["allow_payment_type"],
        deleteUserId: json["delete_user_id"],
        scoreOrientedSubjectData: json["score_oriented_subject_data"],
        certificateGradeScore: json["certificate_grade_score"],
        certificateGrading: json["certificate_grading"],
        certificateYear: json["certificate_year"],
        certificateNumber: json["certificate_number"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "code": code,
        "b_id": bId,
        "rupp_card_id": ruppCardId,
        "fullname": fullname,
        "fullname_en": fullnameEn,
        "firstname": firstname,
        "firstname_en": firstnameEn,
        "lastname": lastname,
        "lastname_en": lastnameEn,
        "gender": gender,
        "birth_order": birthOrder,
        "date_of_birth": "${dateOfBirth!.year.toString().padLeft(4, '0')}-${dateOfBirth!.month.toString().padLeft(2, '0')}-${dateOfBirth!.day.toString().padLeft(2, '0')}",
        "place_of_birth": placeOfBirth,
        "card_id": cardId,
        "email": email,
        "s3_file_url": s3FileUrl,
        "father_name": fatherName,
        "father_birth_date": fatherBirthDate,
        "father_nationality": fatherNationality,
        "father_ethnicity": fatherEthnicity,
        "father_dead_alive": fatherDeadAlive,
        "father_occupation": fatherOccupation,
        "mother_name": motherName,
        "mother_birth_date": motherBirthDate,
        "mother_nationality": motherNationality,
        "mother_ethnicity": motherEthnicity,
        "mother_dead_alive": motherDeadAlive,
        "mother_occupation": motherOccupation,
        "total_siblings": totalSiblings,
        "total_siblings_boy": totalSiblingsBoy,
        "total_siblings_girl": totalSiblingsGirl,
        "place_of_birth_province_id": placeOfBirthProvinceId,
        "province_city_id": provinceCityId,
        "is_foreigner": isForeigner,
        "id_card_passport": idCardPassport,
        "phone": phone,
        "single": single,
        "nationality_id": nationalityId,
        "nationality": nationality,
        "ethnicity": ethnicity,
        "ethnicity_id": ethnicityId,
        "current_address": currentAddress,
        "current_occupation": currentOccupation,
        "where_occupation": whereOccupation,
        "highschool_exam_date": highschoolExamDate,
        "from_highschool": fromHighschool,
        "highschool_certificate_number": highschoolCertificateNumber,
        "name_primary_school": namePrimarySchool,
        "primary_school_province_city_id": primarySchoolProvinceCityId,
        "primary_school_year": primarySchoolYear,
        "name_secondary_school": nameSecondarySchool,
        "secondary_school_province_city_id": secondarySchoolProvinceCityId,
        "secondary_school_year": secondarySchoolYear,
        "name_high_school": nameHighSchool,
        "highschool_exam_place": highschoolExamPlace,
        "high_school_province_city_id": highSchoolProvinceCityId,
        "high_school_year": highSchoolYear,
        "name_university": nameUniversity,
        "university_province_city_id": universityProvinceCityId,
        "university_year": universityYear,
        "school_certificate_year": schoolCertificateYear,
        "certificat_level_year": certificatLevelYear,
        "step": step,
        "personal_info_step": personalInfoStep,
        "test": test,
        "is_individual_create": isIndividualCreate,
        "is_individual_existed": isIndividualExisted,
        "is_register_with_banhji": isRegisterWithBanhji,
        "is_delete": isDelete,
        "staff_deleter_id": staffDeleterId,
        "place_of_birth_en": placeOfBirthEn,
        "father_name_en": fatherNameEn,
        "current_address_en": currentAddressEn,
        "father_occupation_en": fatherOccupationEn,
        "mother_name_en": motherNameEn,
        "mother_occupation_en": motherOccupationEn,
        "current_address_province_city_id": currentAddressProvinceCityId,
        "family_status": familyStatus,
        "pass_high_school_year": passHighSchoolYear,
        "is_migration": isMigration,
        "create_description": createDescription,
        "old_student_id": oldStudentId,
        "allow_payment_type": allowPaymentType,
        "delete_user_id": deleteUserId,
        "score_oriented_subject_data": scoreOrientedSubjectData,
        "certificate_grade_score": certificateGradeScore,
        "certificate_grading": certificateGrading,
        "certificate_year": certificateYear,
        "certificate_number": certificateNumber,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
