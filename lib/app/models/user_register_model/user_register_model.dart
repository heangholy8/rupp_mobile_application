// To parse this JSON data, do
//
//     final userRegisterModel = userRegisterModelFromJson(jsonString);

import 'dart:convert';

UserRegisterModel userRegisterModelFromJson(String str) => UserRegisterModel.fromJson(json.decode(str));

String userRegisterModelToJson(UserRegisterModel data) => json.encode(data.toJson());

class UserRegisterModel {
    UserRegisterModel({
        this.status,
        this.data,
    });

    bool? status;
    Data? data;

    factory UserRegisterModel.fromJson(Map<String, dynamic> json) => UserRegisterModel(
        status: json["status"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.userId,
        this.fullname,
        this.fullnameEn,
        this.lastname,
        this.firstname,
        this.lastnameEn,
        this.firstnameEn,
        this.gender,
        this.dateOfBirth,
        this.phone,
        this.email,
        this.provinceCityId,
        this.nameHighSchool,
        this.highSchoolYear,
        this.highschoolCertificateNumber,
        this.certificateNumber,
        this.certificateYear,
        this.certificateGrading,
        this.certificateGradeScore,
        this.scoreOrientedSubjectData,
        this.updatedAt,
        this.createdAt,
        this.id,
        this.ruppCardId,
        this.studentId,
        this.userInformation,
    });

    int? userId;
    String? fullname;
    String? fullnameEn;
    String? lastname;
    String? firstname;
    String? lastnameEn;
    String? firstnameEn;
    String? gender;
    DateTime? dateOfBirth;
    String? phone;
    String? email;
    String? provinceCityId;
    String? nameHighSchool;
    String? highSchoolYear;
    String? highschoolCertificateNumber;
    String? certificateNumber;
    String? certificateYear;
    String? certificateGrading;
    String? certificateGradeScore;
    List<ScoreOrientedSubjectDatum>? scoreOrientedSubjectData;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;
    String? ruppCardId;
    int? studentId;
    UserInformation? userInformation;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        userId: json["user_id"],
        fullname: json["fullname"],
        fullnameEn: json["fullname_en"],
        lastname: json["lastname"],
        firstname: json["firstname"],
        lastnameEn: json["lastname_en"],
        firstnameEn: json["firstname_en"],
        gender: json["gender"],
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        phone: json["phone"],
        email: json["email"],
        provinceCityId: json["province_city_id"],
        nameHighSchool: json["name_high_school"],
        highSchoolYear: json["high_school_year"],
        highschoolCertificateNumber: json["highschool_certificate_number"],
        certificateNumber: json["certificate_number"],
        certificateYear: json["certificate_year"],
        certificateGrading: json["certificate_grading"],
        certificateGradeScore: json["certificate_grade_score"],
        scoreOrientedSubjectData: List<ScoreOrientedSubjectDatum>.from(json["score_oriented_subject_data"].map((x) => ScoreOrientedSubjectDatum.fromJson(x))),
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
        ruppCardId: json["rupp_card_id"],
        studentId: json["student_id"],
        userInformation: UserInformation.fromJson(json["user_information"]),
    );

    Map<String, dynamic> toJson() => {
        "user_id": userId,
        "fullname": fullname,
        "fullname_en": fullnameEn,
        "lastname": lastname,
        "firstname": firstname,
        "lastname_en": lastnameEn,
        "firstname_en": firstnameEn,
        "gender": gender,
        "date_of_birth": "${dateOfBirth!.year.toString().padLeft(4, '0')}-${dateOfBirth!.month.toString().padLeft(2, '0')}-${dateOfBirth!.day.toString().padLeft(2, '0')}",
        "phone": phone,
        "email": email,
        "province_city_id": provinceCityId,
        "name_high_school": nameHighSchool,
        "high_school_year": highSchoolYear,
        "highschool_certificate_number": highschoolCertificateNumber,
        "certificate_number": certificateNumber,
        "certificate_year": certificateYear,
        "certificate_grading": certificateGrading,
        "certificate_grade_score": certificateGradeScore,
        "score_oriented_subject_data": List<dynamic>.from(scoreOrientedSubjectData!.map((x) => x.toJson())),
        "updated_at": updatedAt!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
        "id": id,
        "rupp_card_id": ruppCardId,
        "student_id": studentId,
        "user_information": userInformation!.toJson(),
    };
}

class ScoreOrientedSubjectDatum {
    ScoreOrientedSubjectDatum({
        this.subjectName,
        this.subjectGrading,
    });

    String? subjectName;
    String? subjectGrading;

    factory ScoreOrientedSubjectDatum.fromJson(Map<String, dynamic> json) => ScoreOrientedSubjectDatum(
        subjectName: json["subject_name"],
        subjectGrading: json["subject_grading"],
    );

    Map<String, dynamic> toJson() => {
        "subject_name": subjectName,
        "subject_grading": subjectGrading,
    };
}

class UserInformation {
    UserInformation({
        this.code,
        this.password,
        this.accessToken,
    });

    String? code;
    String? password;
    String? accessToken;

    factory UserInformation.fromJson(Map<String, dynamic> json) => UserInformation(
        code: json["code"],
        password: json["password"],
        accessToken: json["access_token"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "password": password,
        "access_token": accessToken,
    };
}
