import 'dart:convert';

LoginModel loginModelFromJson(String str) => LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
    LoginModel({
        this.data,
        this.status,
    });

    Data? data;
    bool? status;

    factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        data: Data.fromJson(json["data"]),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
        "status": status,
    };
}

class Data {
    Data({
        this.id,
        this.userId,
        this.code,
        this.bId,
        this.ruppCardId,
        this.fullname,
        this.fullnameEn,
        this.firstname,
        this.firstnameEn,
        this.lastname,
        this.lastnameEn,
        this.gender,
        this.birthOrder,
        this.dateOfBirth,
        this.placeOfBirth,
        this.cardId,
        this.email,
        this.s3FileUrl,
        this.fatherName,
        this.fatherBirthDate,
        this.fatherNationality,
        this.fatherEthnicity,
        this.fatherDeadAlive,
        this.fatherOccupation,
        this.motherName,
        this.motherBirthDate,
        this.motherNationality,
        this.motherEthnicity,
        this.motherDeadAlive,
        this.motherOccupation,
        this.totalSiblings,
        this.totalSiblingsBoy,
        this.totalSiblingsGirl,
        this.placeOfBirthProvinceId,
        this.provinceCityId,
        this.isForeigner,
        this.idCardPassport,
        this.phone,
        this.single,
        this.nationalityId,
        this.nationality,
        this.ethnicity,
        this.ethnicityId,
        this.currentAddress,
        this.currentOccupation,
        this.whereOccupation,
        this.highschoolExamDate,
        this.fromHighschool,
        this.highschoolCertificateNumber,
        this.namePrimarySchool,
        this.primarySchoolProvinceCityId,
        this.primarySchoolYear,
        this.nameSecondarySchool,
        this.secondarySchoolProvinceCityId,
        this.secondarySchoolYear,
        this.nameHighSchool,
        this.highschoolExamPlace,
        this.highSchoolProvinceCityId,
        this.highSchoolYear,
        this.nameUniversity,
        this.universityProvinceCityId,
        this.universityYear,
        this.schoolCertificateYear,
        this.certificatLevelYear,
        this.step,
        this.personalInfoStep,
        this.test,
        this.isIndividualCreate,
        this.isIndividualExisted,
        this.isRegisterWithBanhji,
        this.isDelete,
        this.staffDeleterId,
        this.placeOfBirthEn,
        this.fatherNameEn,
        this.currentAddressEn,
        this.fatherOccupationEn,
        this.motherNameEn,
        this.motherOccupationEn,
        this.currentAddressProvinceCityId,
        this.familyStatus,
        this.passHighSchoolYear,
        this.isMigration,
        this.createDescription,
        this.oldStudentId,
        this.allowPaymentType,
        this.deleteUserId,
        this.scoreOrientedSubjectData,
        this.certificateGradeScore,
        this.certificateGrading,
        this.certificateYear,
        this.certificateNumber,
        this.createdAt,
        this.updatedAt,
        this.studentId,
        this.userInformation,
    });

    int? id;
    int? userId;
    dynamic code;
    dynamic bId;
    String? ruppCardId;
    String? fullname;
    String? fullnameEn;
    String? firstname;
    String? firstnameEn;
    String? lastname;
    String? lastnameEn;
    int? gender;
    dynamic birthOrder;
    DateTime? dateOfBirth;
    dynamic placeOfBirth;
    dynamic cardId;
    String? email;
    dynamic s3FileUrl;
    dynamic fatherName;
    dynamic fatherBirthDate;
    dynamic fatherNationality;
    dynamic fatherEthnicity;
    dynamic fatherDeadAlive;
    dynamic fatherOccupation;
    dynamic motherName;
    dynamic motherBirthDate;
    dynamic motherNationality;
    dynamic motherEthnicity;
    dynamic motherDeadAlive;
    dynamic motherOccupation;
    dynamic totalSiblings;
    dynamic totalSiblingsBoy;
    dynamic totalSiblingsGirl;
    dynamic placeOfBirthProvinceId;
    int? provinceCityId;
    int? isForeigner;
    String? idCardPassport;
    String? phone;
    dynamic single;
    dynamic nationalityId;
    dynamic nationality;
    dynamic ethnicity;
    dynamic ethnicityId;
    String? currentAddress;
    dynamic currentOccupation;
    dynamic whereOccupation;
    dynamic highschoolExamDate;
    dynamic fromHighschool;
    String? highschoolCertificateNumber;
    dynamic namePrimarySchool;
    dynamic primarySchoolProvinceCityId;
    dynamic primarySchoolYear;
    dynamic nameSecondarySchool;
    dynamic secondarySchoolProvinceCityId;
    dynamic secondarySchoolYear;
    String? nameHighSchool;
    dynamic highschoolExamPlace;
    dynamic highSchoolProvinceCityId;
    String ?highSchoolYear;
    dynamic nameUniversity;
    dynamic universityProvinceCityId;
    dynamic universityYear;
    dynamic schoolCertificateYear;
    dynamic certificatLevelYear;
    int? step;
    int ?personalInfoStep;
    dynamic test;
    int? isIndividualCreate;
    int? isIndividualExisted;
    dynamic isRegisterWithBanhji;
    int? isDelete;
    dynamic staffDeleterId;
    dynamic placeOfBirthEn;
    dynamic fatherNameEn;
    String? currentAddressEn;
    dynamic fatherOccupationEn;
    dynamic motherNameEn;
    dynamic motherOccupationEn;
    dynamic currentAddressProvinceCityId;
    int? familyStatus;
    dynamic passHighSchoolYear;
    int? isMigration;
    dynamic createDescription;
    dynamic oldStudentId;
    dynamic allowPaymentType;
    dynamic deleteUserId;
    List<ScoreOrientedSubjectDatum>? scoreOrientedSubjectData;
    String? certificateGradeScore;
    String? certificateGrading;
    String? certificateYear;
    String? certificateNumber;
    DateTime? createdAt;
    DateTime? updatedAt;
    int? studentId;
    UserInformation? userInformation;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        userId: json["user_id"],
        code: json["code"],
        bId: json["b_id"],
        ruppCardId: json["rupp_card_id"],
        fullname: json["fullname"],
        fullnameEn: json["fullname_en"],
        firstname: json["firstname"],
        firstnameEn: json["firstname_en"],
        lastname: json["lastname"],
        lastnameEn: json["lastname_en"],
        gender: json["gender"],
        birthOrder: json["birth_order"],
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        placeOfBirth: json["place_of_birth"],
        cardId: json["card_id"],
        email: json["email"],
        s3FileUrl: json["s3_file_url"],
        fatherName: json["father_name"],
        fatherBirthDate: json["father_birth_date"],
        fatherNationality: json["father_nationality"],
        fatherEthnicity: json["father_ethnicity"],
        fatherDeadAlive: json["father_dead_alive"],
        fatherOccupation: json["father_occupation"],
        motherName: json["mother_name"],
        motherBirthDate: json["mother_birth_date"],
        motherNationality: json["mother_nationality"],
        motherEthnicity: json["mother_ethnicity"],
        motherDeadAlive: json["mother_dead_alive"],
        motherOccupation: json["mother_occupation"],
        totalSiblings: json["total_siblings"],
        totalSiblingsBoy: json["total_siblings_boy"],
        totalSiblingsGirl: json["total_siblings_girl"],
        placeOfBirthProvinceId: json["place_of_birth_province_id"],
        provinceCityId: json["province_city_id"],
        isForeigner: json["is_foreigner"],
        idCardPassport: json["id_card_passport"],
        phone: json["phone"],
        single: json["single"],
        nationalityId: json["nationality_id"],
        nationality: json["nationality"],
        ethnicity: json["ethnicity"],
        ethnicityId: json["ethnicity_id"],
        currentAddress: json["current_address"],
        currentOccupation: json["current_occupation"],
        whereOccupation: json["where_occupation"],
        highschoolExamDate: json["highschool_exam_date"],
        fromHighschool: json["from_highschool"],
        highschoolCertificateNumber: json["highschool_certificate_number"],
        namePrimarySchool: json["name_primary_school"],
        primarySchoolProvinceCityId: json["primary_school_province_city_id"],
        primarySchoolYear: json["primary_school_year"],
        nameSecondarySchool: json["name_secondary_school"],
        secondarySchoolProvinceCityId: json["secondary_school_province_city_id"],
        secondarySchoolYear: json["secondary_school_year"],
        nameHighSchool: json["name_high_school"],
        highschoolExamPlace: json["highschool_exam_place"],
        highSchoolProvinceCityId: json["high_school_province_city_id"],
        highSchoolYear: json["high_school_year"],
        nameUniversity: json["name_university"],
        universityProvinceCityId: json["university_province_city_id"],
        universityYear: json["university_year"],
        schoolCertificateYear: json["school_certificate_year"],
        certificatLevelYear: json["certificat_level_year"],
        step: json["step"],
        personalInfoStep: json["personal_info_step"],
        test: json["test"],
        isIndividualCreate: json["is_individual_create"],
        isIndividualExisted: json["is_individual_existed"],
        isRegisterWithBanhji: json["is_register_with_banhji"],
        isDelete: json["is_delete"],
        staffDeleterId: json["staff_deleter_id"],
        placeOfBirthEn: json["place_of_birth_en"],
        fatherNameEn: json["father_name_en"],
        currentAddressEn: json["current_address_en"],
        fatherOccupationEn: json["father_occupation_en"],
        motherNameEn: json["mother_name_en"],
        motherOccupationEn: json["mother_occupation_en"],
        currentAddressProvinceCityId: json["current_address_province_city_id"],
        familyStatus: json["family_status"],
        passHighSchoolYear: json["pass_high_school_year"],
        isMigration: json["is_migration"],
        createDescription: json["create_description"],
        oldStudentId: json["old_student_id"],
        allowPaymentType: json["allow_payment_type"],
        deleteUserId: json["delete_user_id"],
        scoreOrientedSubjectData: List<ScoreOrientedSubjectDatum>.from(json["score_oriented_subject_data"].map((x) => ScoreOrientedSubjectDatum.fromJson(x))),
        certificateGradeScore: json["certificate_grade_score"],
        certificateGrading: json["certificate_grading"],
        certificateYear: json["certificate_year"],
        certificateNumber: json["certificate_number"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        studentId: json["student_id"],
        userInformation: UserInformation.fromJson(json["user_information"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "code": code,
        "b_id": bId,
        "rupp_card_id": ruppCardId,
        "fullname": fullname,
        "fullname_en": fullnameEn,
        "firstname": firstname,
        "firstname_en": firstnameEn,
        "lastname": lastname,
        "lastname_en": lastnameEn,
        "gender": gender,
        "birth_order": birthOrder,
        "date_of_birth": "${dateOfBirth!.year.toString().padLeft(4, '0')}-${dateOfBirth?.month.toString().padLeft(2, '0')}-${dateOfBirth?.day.toString().padLeft(2, '0')}",
        "place_of_birth": placeOfBirth,
        "card_id": cardId,
        "email": email,
        "s3_file_url": s3FileUrl,
        "father_name": fatherName,
        "father_birth_date": fatherBirthDate,
        "father_nationality": fatherNationality,
        "father_ethnicity": fatherEthnicity,
        "father_dead_alive": fatherDeadAlive,
        "father_occupation": fatherOccupation,
        "mother_name": motherName,
        "mother_birth_date": motherBirthDate,
        "mother_nationality": motherNationality,
        "mother_ethnicity": motherEthnicity,
        "mother_dead_alive": motherDeadAlive,
        "mother_occupation": motherOccupation,
        "total_siblings": totalSiblings,
        "total_siblings_boy": totalSiblingsBoy,
        "total_siblings_girl": totalSiblingsGirl,
        "place_of_birth_province_id": placeOfBirthProvinceId,
        "province_city_id": provinceCityId,
        "is_foreigner": isForeigner,
        "id_card_passport": idCardPassport,
        "phone": phone,
        "single": single,
        "nationality_id": nationalityId,
        "nationality": nationality,
        "ethnicity": ethnicity,
        "ethnicity_id": ethnicityId,
        "current_address": currentAddress,
        "current_occupation": currentOccupation,
        "where_occupation": whereOccupation,
        "highschool_exam_date": highschoolExamDate,
        "from_highschool": fromHighschool,
        "highschool_certificate_number": highschoolCertificateNumber,
        "name_primary_school": namePrimarySchool,
        "primary_school_province_city_id": primarySchoolProvinceCityId,
        "primary_school_year": primarySchoolYear,
        "name_secondary_school": nameSecondarySchool,
        "secondary_school_province_city_id": secondarySchoolProvinceCityId,
        "secondary_school_year": secondarySchoolYear,
        "name_high_school": nameHighSchool,
        "highschool_exam_place": highschoolExamPlace,
        "high_school_province_city_id": highSchoolProvinceCityId,
        "high_school_year": highSchoolYear,
        "name_university": nameUniversity,
        "university_province_city_id": universityProvinceCityId,
        "university_year": universityYear,
        "school_certificate_year": schoolCertificateYear,
        "certificat_level_year": certificatLevelYear,
        "step": step,
        "personal_info_step": personalInfoStep,
        "test": test,
        "is_individual_create": isIndividualCreate,
        "is_individual_existed": isIndividualExisted,
        "is_register_with_banhji": isRegisterWithBanhji,
        "is_delete": isDelete,
        "staff_deleter_id": staffDeleterId,
        "place_of_birth_en": placeOfBirthEn,
        "father_name_en": fatherNameEn,
        "current_address_en": currentAddressEn,
        "father_occupation_en": fatherOccupationEn,
        "mother_name_en": motherNameEn,
        "mother_occupation_en": motherOccupationEn,
        "current_address_province_city_id": currentAddressProvinceCityId,
        "family_status": familyStatus,
        "pass_high_school_year": passHighSchoolYear,
        "is_migration": isMigration,
        "create_description": createDescription,
        "old_student_id": oldStudentId,
        "allow_payment_type": allowPaymentType,
        "delete_user_id": deleteUserId,
        "score_oriented_subject_data": List<dynamic>.from(scoreOrientedSubjectData!.map((x) => x.toJson())),
        "certificate_grade_score": certificateGradeScore,
        "certificate_grading": certificateGrading,
        "certificate_year": certificateYear,
        "certificate_number": certificateNumber,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "student_id": studentId,
        "user_information": userInformation!.toJson(),
    };
}

class ScoreOrientedSubjectDatum {
    ScoreOrientedSubjectDatum({
        this.subjectName,
        this.subjectGrading,
    });

    String ?subjectName;
    String? subjectGrading;

    factory ScoreOrientedSubjectDatum.fromJson(Map<String, dynamic> json) => ScoreOrientedSubjectDatum(
        subjectName: json["subject_name"],
        subjectGrading: json["subject_grading"],
    );

    Map<String, dynamic> toJson() => {
        "subject_name": subjectName,
        "subject_grading": subjectGrading,
    };
}

class UserInformation {
    UserInformation({
        this.code,
        this.password,
        this.accessToken,
    });

    String? code;
    String? password;
    String? accessToken;

    factory UserInformation.fromJson(Map<String, dynamic> json) => UserInformation(
        code: json["code"],
        password: json["password"],
        accessToken: json["access_token"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "password": password,
        "access_token": accessToken,
    };
}
