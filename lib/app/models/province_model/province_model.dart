// To parse this JSON data, do
//
//     final provinceModel = provinceModelFromJson(jsonString);

import 'dart:convert';

ProvinceModel provinceModelFromJson(String str) => ProvinceModel.fromJson(json.decode(str));

String provinceModelToJson(ProvinceModel data) => json.encode(data.toJson());

class ProvinceModel {
    ProvinceModel({
        this.status,
        this.data,
    });

    bool? status;
    List<Datum>? data;

    factory ProvinceModel.fromJson(Map<String, dynamic> json) => ProvinceModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.id,
        this.name,
        this.nameEn,
        this.sortkey,
        this.isCity,
        this.parent,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? name;
    String? nameEn;
    int? sortkey;
    int? isCity;
    int? parent;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        sortkey: json["sortkey"],
        isCity: json["is_city"],
        parent: json["parent"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "sortkey": sortkey,
        "is_city": isCity,
        "parent": parent,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
