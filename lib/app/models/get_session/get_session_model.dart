// To parse this JSON data, do
//
//     final sessionModel = sessionModelFromJson(jsonString);

import 'dart:convert';

SessionModel sessionModelFromJson(String str) => SessionModel.fromJson(json.decode(str));

String sessionModelToJson(SessionModel data) => json.encode(data.toJson());

class SessionModel {
    SessionModel({
        this.status,
        this.data,
    });

    bool? status;
    List<Datum>? data;

    factory SessionModel.fromJson(Map<String, dynamic> json) => SessionModel(
        status: json["status"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        this.shiftSessionId,
        this.name,
        this.nameEn,
        this.enrollAvailable,
        this.isSelected,
    });

    int? shiftSessionId;
    String? name;
    String? nameEn;
    int? enrollAvailable;
    int? isSelected;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        shiftSessionId: json["shift_session_id"],
        name: json["name"],
        nameEn: json["name_en"],
        enrollAvailable: json["enroll_available"],
        isSelected: json["is_selected"],
    );

    Map<String, dynamic> toJson() => {
        "shift_session_id": shiftSessionId,
        "name": name,
        "name_en": nameEn,
        "enroll_available": enrollAvailable,
        "is_selected": isSelected,
    };
}
