class MajorModel {
    MajorModel({
        this.status,
        this.currentSchoolyear,
        this.data,
    });

    bool? status;
    CurrentSchoolyear? currentSchoolyear;
    List<Datum>? data;

    factory MajorModel.fromJson(Map<String, dynamic> json) => MajorModel(
        status: json["status"],
        currentSchoolyear: CurrentSchoolyear.fromJson(json["current_schoolyear"]),
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "current_schoolyear": currentSchoolyear!.toJson(),
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class CurrentSchoolyear {
    CurrentSchoolyear({
        this.id,
        this.name,
        this.nameEn,
        this.startYear,
        this.endYear,
        this.sortkey,
        this.isActive,
        this.isUsed,
        this.isNextActive,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? name;
    String? nameEn;
    int? startYear;
    int? endYear;
    int? sortkey;
    int? isActive;
    int? isUsed;
    int? isNextActive;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory CurrentSchoolyear.fromJson(Map<String, dynamic> json) => CurrentSchoolyear(
        id: json["id"],
        name: json["name"],
        nameEn: json["name_en"],
        startYear: json["start_year"],
        endYear: json["end_year"],
        sortkey: json["sortkey"],
        isActive: json["is_active"],
        isUsed: json["is_used"],
        isNextActive: json["is_next_active"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_en": nameEn,
        "start_year": startYear,
        "end_year": endYear,
        "sortkey": sortkey,
        "is_active": isActive,
        "is_used": isUsed,
        "is_next_active": isNextActive,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

class Datum {
    Datum({
        this.facultyId,
        this.code,
        this.name,
        this.nameEn,
        this.majors,
    });

    int? facultyId;
    String? code;
    String? name;
    String? nameEn;
    List<Major>? majors;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        facultyId: json["faculty_id"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        majors: List<Major>.from(json["majors"].map((x) => Major.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "faculty_id": facultyId,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "majors": List<dynamic>.from(majors!.map((x) => x.toJson())),
    };
}

class Major {
    Major({
        this.majorId,
        this.code,
        this.name,
        this.nameEn,
        this.degreeId,
        this.isAvailable,
        this.isSelected,
        this.schoolFee,
        this.rule,
    });

    int? majorId;
    String? code;
    String? name;
    String? nameEn;
    int? degreeId;
    int? isAvailable;
    int? isSelected;
    SchoolFee? schoolFee;
    Rule? rule;

    factory Major.fromJson(Map<String, dynamic> json) => Major(
        majorId: json["major_id"],
        code: json["code"],
        name: json["name"],
        nameEn: json["name_en"],
        degreeId: json["degree_id"],
        isAvailable: json["is_available"] == null ? null : json["is_available"],
        isSelected: json["is_selected"],
        schoolFee: json["school_fee"] == null ? null : SchoolFee.fromJson(json["school_fee"]),
        rule: json["rule"] == null ? null : Rule.fromJson(json["rule"]),
    );

    Map<String, dynamic> toJson() => {
        "major_id": majorId,
        "code": code,
        "name": name,
        "name_en": nameEn,
        "degree_id": degreeId,
        "is_available": isAvailable == null ? null : isAvailable,
        "is_selected": isSelected,
        "school_fee": schoolFee == null ? null : schoolFee!.toJson(),
        "rule": rule == null ? null : rule!.toJson(),
    };
}

class Rule {
    Rule({
        this.isMajorDegreeAvailable,
        this.orientationSubjects,
        this.orientationSubjectsEn,
        this.orientationSubject1,
        this.orientationSubject2,
        this.orientationSubjectEn1,
        this.orientationSubjectEn2,
        this.subjectGrading,
    });

    int? isMajorDegreeAvailable;
    String? orientationSubjects;
    String? orientationSubjectsEn;
    String? orientationSubject1;
    String? orientationSubject2;
    String? orientationSubjectEn1;
    String? orientationSubjectEn2;
    String? subjectGrading;

    factory Rule.fromJson(Map<String, dynamic> json) => Rule(
        isMajorDegreeAvailable: json["is_major_degree_available"] == null ? null : json["is_major_degree_available"],
        orientationSubjects: json["orientation_subjects"],
        orientationSubjectsEn: json["orientation_subjects_en"],
        orientationSubject1: json["orientation_subject_1"],
        orientationSubject2: json["orientation_subject_2"],
        orientationSubjectEn1: json["orientation_subject_en_1"],
        orientationSubjectEn2: json["orientation_subject_en_2"],
        subjectGrading: json["subject_grading"],
    );

    Map<String, dynamic> toJson() => {
        "is_major_degree_available": isMajorDegreeAvailable == null ? null : isMajorDegreeAvailable,
        "orientation_subjects": orientationSubjects,
        "orientation_subjects_en": orientationSubjectsEn,
        "orientation_subject_1": orientationSubject1,
        "orientation_subject_2": orientationSubject2,
        "orientation_subject_en_1": orientationSubjectEn1,
        "orientation_subject_en_2": orientationSubjectEn2,
        "subject_grading": subjectGrading,
    };
}

class SchoolFee {
    SchoolFee({
        this.id,
        this.majorId,
        this.degreeId,
        this.stageId,
        this.nameEn,
        this.name,
        this.academicyearId,
        this.domesticAmount,
        this.amountExamOrientationSubjects,
        this.amountExamOrientationSubjectsCurrency,
        this.scholarshipAmount,
        this.isWorking,
        this.scholarshipCurrency,
        this.amountExam,
        this.amountCurrency,
        this.amountExamCurrency,
        this.paymentOption,
        this.isScholarship,
        this.internationalAmount,
        this.discountPercentage,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    int? majorId;
    int? degreeId;
    dynamic stageId;
    String? nameEn;
    String? name;
    int? academicyearId;
    int? domesticAmount;
    int? amountExamOrientationSubjects;
    String? amountExamOrientationSubjectsCurrency;
    int? scholarshipAmount;
    int? isWorking;
    String? scholarshipCurrency;
    int? amountExam;
    String? amountCurrency;
    String? amountExamCurrency;
    int? paymentOption;
    int? isScholarship;
    int? internationalAmount;
    int? discountPercentage;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory SchoolFee.fromJson(Map<String, dynamic> json) => SchoolFee(
        id: json["id"],
        majorId: json["major_id"],
        degreeId: json["degree_id"],
        stageId: json["stage_id"],
        nameEn: json["name_en"],
        name: json["name"],
        academicyearId: json["academicyear_id"],
        domesticAmount: json["domestic_amount"],
        amountExamOrientationSubjects: json["amount_exam_orientation_subjects"],
        amountExamOrientationSubjectsCurrency: json["amount_exam_orientation_subjects_currency"],
        scholarshipAmount: json["scholarship_amount"],
        isWorking: json["is_working"] == null ? null : json["is_working"],
        scholarshipCurrency: json["scholarship_currency"],
        amountExam: json["amount_exam"],
        amountCurrency: json["amount_currency"],
        amountExamCurrency: json["amount_exam_currency"],
        paymentOption: json["payment_option"],
        isScholarship: json["is_scholarship"] == null ? null : json["is_scholarship"],
        internationalAmount: json["international_amount"],
        discountPercentage: json["discount_percentage"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "major_id": majorId,
        "degree_id": degreeId,
        "stage_id": stageId,
        "name_en": nameEn,
        "name": name,
        "academicyear_id": academicyearId,
        "domestic_amount": domesticAmount,
        "amount_exam_orientation_subjects": amountExamOrientationSubjects,
        "amount_exam_orientation_subjects_currency": amountExamOrientationSubjectsCurrency,
        "scholarship_amount": scholarshipAmount,
        "is_working": isWorking == null ? null : isWorking,
        "scholarship_currency": scholarshipCurrency,
        "amount_exam": amountExam,
        "amount_currency": amountCurrency,
        "amount_exam_currency": amountExamCurrency,
        "payment_option": paymentOption,
        "is_scholarship": isScholarship == null ? null : isScholarship,
        "international_amount": internationalAmount,
        "discount_percentage": discountPercentage,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
