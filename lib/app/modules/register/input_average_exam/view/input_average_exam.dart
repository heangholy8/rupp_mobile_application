import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/register/choose_secession/controller/choose_secession_model.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/button/button_widget.dart';

class InputAveragrExamScreen extends StatefulWidget {
  final String typeRout;
  const InputAveragrExamScreen({Key? key, this.typeRout = "normal"}) : super(key: key);

  @override
  State<InputAveragrExamScreen> createState() => _InputAveragrExamScreenState();
}

class _InputAveragrExamScreenState extends State<InputAveragrExamScreen> {
  final datasecession = SecessionData.generate();
  FocusNode? focusNode;
  UserSecureStroage _prefs = UserSecureStroage();
  TextEditingController imputleft = TextEditingController();
  TextEditingController imputright = TextEditingController();
  String? averageright;
  String? averagetotal;
  bool leftinput = true;
  bool rightinput = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("ការចុះឈ្មោះ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: Container(
                  margin:const EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(top: 60,left: 35,right: 35,bottom: 10),
                        child: Text("លំដាប់ពិន្ទុរួម",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                      ),
                      const SizedBox(height: 8,),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 70,
                              child: TextFormField(
                                maxLength: 3,
                                onChanged:(value) {
                                 setState(() {
                                  if(int.parse(value)>=11 && int.parse(value)<=99) {
                                      FocusScope.of(context).nextFocus();
                                  }
                                  else if(int.parse(value)==1 || int.parse(value)>100){
                                    imputleft.text= "100";
                                    imputright.text = "000";
                                    FocusScope.of(context).nextFocus();
                                  }
                                  else if(int.parse(value)==10){
                                    imputleft.text= "0";
                                    imputright.text = "000";
                                  }
                                  imputleft.text;
                                 });
                                },
                                controller: imputleft,
                                textAlign: TextAlign.center,
                                style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor),
                                keyboardType: TextInputType.number,
                                autofocus: leftinput,
                                focusNode: focusNode,
                                inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                                decoration:const InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                    borderSide: BorderSide(color: ColorContant.primaryColor),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                    borderSide: BorderSide(color: ColorContant.primaryColor),
                                    ),
                                    contentPadding: EdgeInsets.only(
                                      top: 12.0, bottom: 12.0),
                                        ),
                              ),
                              
                            ),
                            const SizedBox(width: 8,),
                            Container(
                              alignment: Alignment.bottomCenter,
                              child: Text(","),),
                            const SizedBox(width: 8,),
                            Container(
                              width: 120,
                              child: TextFormField(
                                maxLength: 3,
                                onChanged:(value) {
                                 setState(() {
                                  if(imputleft.text== "100"){
                                    imputright.text = "000";
                                  }
                                  else{
                                    imputright.text;
                                  }
                                 });
                                },
                                controller: imputright,
                                textAlign: TextAlign.center,
                                style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor),
                                keyboardType: TextInputType.number,
                                autofocus: rightinput,
                                focusNode: focusNode,
                                inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                                decoration:const InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                    borderSide: BorderSide(color: ColorContant.primaryColor),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                    borderSide: BorderSide(color: ColorContant.primaryColor),
                                    ),
                                    contentPadding: EdgeInsets.only(
                                      top: 12.0, bottom: 12.0),
                                        ),
                              ),
                              
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.only(top: 50,bottom: 20),
                        child: ButtonWidget(
                          buttonColor:imputleft.text!=""&&imputright.text!=""?ColorContant.primaryColor:ColorContant.duckDisableColor,
                          textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                          weightButton: 140,
                          panddingVerButton: 5,
                          panddinHorButton: 16,
                          onTap:imputleft.text=="" && imputright.text==""?null:(){
                            averageright = imputright.text.length==1?(imputright.text+"00"):imputright.text.length==2?(imputright.text+"0"):imputright.text;
                            averagetotal = imputleft.text + ","+averageright.toString();
                            _prefs.setAverageExam(averageExam: averagetotal.toString());

                            if(widget.typeRout=="edit"){
                                    Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                  }
                                  else{
                                    Navigator.pushNamed(context, Routes.CHOOSEGREADSCREEN);
                                  }
                            
                            
                          },
                          title: "បន្ទាប់",
                          radiusButton: 10,
                        ),
                      ),
                    ],
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}