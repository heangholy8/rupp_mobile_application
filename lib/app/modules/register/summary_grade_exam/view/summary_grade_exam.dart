import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';

import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/button/button_widget.dart';

class SummaryGradeScreen extends StatefulWidget {
  const SummaryGradeScreen({Key? key}) : super(key: key);

  @override
  State<SummaryGradeScreen> createState() => _SummaryGradeScreenState();
}

class _SummaryGradeScreenState extends State<SummaryGradeScreen> {
  String? orientation_subject_1;
  String? orientation_subject_2;
  int? price;
  String? payType;
  String? majorName;
  String? session;
  String? year;
  String? gradetotal;
  String? gradeOrie1;
  String? gradeOrie2;
  String? averageExam;
  String?token;
  UserSecureStroage _prefs = UserSecureStroage();
  void getstorage() async{
    var majorModel = await _prefs.getMajorModel;
    var sessionModel =await _prefs.getSessionModel;
    var indexSession = await _prefs.getIndexSession();
    var indexFaculty = await _prefs.getIndexFaculty();
    var indexmajor = await _prefs.getIndexMajors();
    var paytype = await _prefs.getPayType();
    var grade1 = await _prefs.getOrientedSubject1Grade();
    var grade2 = await _prefs.getOrientedSubject2Grade();
    var grade = await _prefs.getOrientedGrade();
    var average = await _prefs.getAverageExam();
    var accessToken = await _prefs.getLoginSucess();
    setState(() {
      majorName = majorModel.data![indexFaculty!.toInt()].majors![indexmajor!.toInt()].name.toString();
      payType = paytype.toString();
      session = sessionModel.data![indexSession!].name.toString();
      year = majorModel.currentSchoolyear!.name.toString();
      token = accessToken.toString();
      gradetotal = grade.toString();
      gradeOrie1 = grade1.toString();
      gradeOrie2 = grade2.toString();
      averageExam = average.toString();
      orientation_subject_1 = majorModel.data![indexFaculty.toInt()].majors![indexmajor.toInt()].rule!.orientationSubject1.toString();
      orientation_subject_2 = majorModel.data![indexFaculty.toInt()].majors![indexmajor.toInt()].rule!.orientationSubject2.toString();
      price = (payType=="2"?(majorModel.data![indexFaculty].majors![indexmajor].schoolFee!.domesticAmount! + 5).toInt():((majorModel.data![indexFaculty].majors![indexmajor].schoolFee!.domesticAmount! / 2)+5).toInt());
    });
  }
  @override
  void initState() {
    setState(() {
      getstorage();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("សង្ខេបព័ត៌មាន",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 35),
                child: Text("ជម្រើសដែលអ្នកបានជ្រើស",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.symmetric(horizontal: 41),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.only(top: 16),
                          width: MediaQuery.of(context).size.width,
                          padding:const EdgeInsets.symmetric(vertical: 16,horizontal: 14),
                          decoration: BoxDecoration(
                            border: Border.all(width: 2,color: ColorContant.primaryColor),
                            borderRadius:const BorderRadius.all(Radius.circular(12))
                          ),
                          child: Column(
                            children: [
                              // Container(
                              //   margin:const EdgeInsets.only(bottom: 5,top: 0),
                              //   child: Container(
                              //     child: Stack(
                              //       alignment: AlignmentDirectional.center,
                              //       children: [
                              //         Container(
                              //           alignment: Alignment.center,
                              //           child:const Divider(height: 1,color: ColorContant.primaryColor,thickness: 0.8,),
                              //         ),
                              //         Container(
                              //           alignment: Alignment.center,
                              //           margin:const EdgeInsets.only(left: 40,right: 40),
                              //           child: Center(child: Container(padding:const EdgeInsets.symmetric(horizontal: 8),color: ColorContant.whiteColor,child: Text("សូមចុចលើព័ត៌មានដែលចង់ កែសម្រួល ដើម្បីកែប្រែ",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,))),
                              //         ),
                              //       ],
                              //     ),
                              //   ),
                              // ),
                              Container(
                                child: Text("និទ្ទេសប្រឡង",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                              ),
                              orientation_subject_1=="--" || orientation_subject_1==""?Container():Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("$orientation_subject_1៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(gradeOrie1=="NO"?"មិនមាន" :gradeOrie1.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              orientation_subject_2=="--" || orientation_subject_2==""?Container():Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("$orientation_subject_2៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(gradeOrie2=="NO"?"មិនមាន":gradeOrie2.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("លំដាប់ពិន្ទុរួម៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(averageExam.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("និទ្ទេសរួម៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(gradetotal.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(vertical: 16),
                          width: MediaQuery.of(context).size.width,
                          padding:const EdgeInsets.symmetric(vertical: 16,horizontal: 14),
                          decoration: BoxDecoration(
                            border: Border.all(width: 2,color: ColorContant.primaryColor),
                            borderRadius:const BorderRadius.all(Radius.circular(12))
                          ),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("កម្មវិធីសិក្សា៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text("បរិញ្ញាបត្រ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ដេប៉ាតេម៉ង់៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(majorName.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("វេនសិក្សា៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(session.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ឆ្នាំទី៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text("១",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ឆ្នាំសិក្សា៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(year.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ការបង់ប្រាក់៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(payType=="2"?"បង់ជាឆ្នាំ":"បង់ជាឆមាស",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ទឹកប្រាក់ត្រូវបង់៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text("\$"+price.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin:const EdgeInsets.symmetric(vertical: 8),
                                child: Text("សិស្សទាំងអស់ ត្រូវបង់ប្រាក់ថ្លៃរដ្ឋបាល\$5",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                              ),
                              // Container(
                              //   margin:const EdgeInsets.symmetric(vertical: 8),
                              //   child: ButtonWidget(
                              //     buttonColor: ColorContant.primaryColor,
                              //     panddinHorButton: 12,
                              //     panddingVerButton: 5,
                              //     title: "កែសម្រួល",
                              //     weightButton: 120,
                              //     heightButton: 45,
                              //     textStyleButton: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                              //     radiusButton: 10,
                              //     onTap: (){
                              //       Navigator.pushNamed(context, Routes.CHOOSEDEPARTMENTSCREEN);
                              //     },
                              //   ),
                              // )
                            ],
                          ),
                        ),
                        Container(
                          child: Text("សូមបញ្ជាក់ព័ត៌មានសង្ខេបខាងលើអោយបានត្រឹមត្រូវមុននឹងបន្ត",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(vertical: 8),
                          child: ButtonWidget(
                            buttonColor: ColorContant.primaryColor,
                            panddinHorButton: 12,
                            panddingVerButton: 7,
                            title: "បន្តចូលបំពេញឈ្មោះ",
                            textStyleButton: ThemeConstant.texttheme.headline5!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                            radiusButton: 10,
                            onTap: (){
                              Navigator.pushNamed(context, Routes.INPUTFRISTNAMESCREEN);
                            },
                          ),
                        ),
                        Container(
                          margin:const EdgeInsets.only(bottom: 5,top: 15),
                          child: Container(
                            child: Stack(
                              alignment: AlignmentDirectional.center,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child:const Divider(height: 1,color: ColorContant.ducktextColor,thickness: 0.8,),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  margin:const EdgeInsets.only(left: 40,right: 40),
                                  child: Center(child: Container(padding:const EdgeInsets.symmetric(horizontal: 8),color: ColorContant.whiteColor,child: Text("ឬរក្សារព័ត៌មានទុកសិន",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,))),
                                ),
                              ],
                            ),
                          ),
                        ),
                        token==""? Container(
                          margin:const EdgeInsets.symmetric(vertical: 8),
                          child: ButtonWidget(
                            buttonColor: ColorContant.primaryColor,
                            panddinHorButton: 12,
                            panddingVerButton: 5,
                            title: "រក្សារទុកសិន",
                            weightButton: 160,
                            heightButton: 55,
                            textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                            radiusButton: 10,
                            onTap: (){
                              Navigator.pushNamedAndRemoveUntil(context, Routes.OPTIONREGISANDLOGINSCREEN, (route) => false);
                              _prefs.setStep(step: "2");
                            },
                          ),
                        ):Container(),
                        const SizedBox(height: 55,),
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}