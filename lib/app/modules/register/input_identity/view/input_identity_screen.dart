import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/register/choose_secession/controller/choose_secession_model.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/button/button_widget.dart';
import '../../../../widgets/textfromfiled_custom.dart';

class InputIdentityScreen extends StatefulWidget {
  const InputIdentityScreen({Key? key}) : super(key: key);

  @override
  State<InputIdentityScreen> createState() => _InputIdentityScreenState();
}

class _InputIdentityScreenState extends State<InputIdentityScreen> {
  final datasecession = SecessionData.generate();
  UserSecureStroage _prefs = UserSecureStroage();
  TextEditingController identity = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("ការចុះឈ្មោះ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.only(top: 60,left: 35,right: 35,bottom: 30),
                          child: Text("លេខសញ្ញាបត្រ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        const SizedBox(height: 8,),
                        Container(
                          child: TextFromFiledWidget(
                            controller: identity,
                            keytype: TextInputType.number,
                            onChange: (value) {
                             setState(() {
                               identity.text;
                             });
                            },
                            
                          ),
                          
                        ),
                        Container(
                          margin:const EdgeInsets.only(top: 50,bottom: 20),
                          child: ButtonWidget(
                            buttonColor:identity.text!=""?ColorContant.primaryColor:ColorContant.duckDisableColor,
                            textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                            weightButton: 140,
                            panddingVerButton: 5,
                            panddinHorButton: 16,
                            onTap:identity.text==""?null:(){
                              //FocusManager.instance.primaryFocus?.unfocus();
                              _prefs.setCertificateExam(certificateExam: identity.text);
                              Navigator.pushNamed(context, Routes.INPUTIPHONENUMBERSCREEN);
                            },
                            title: "បន្ទាប់",
                            radiusButton: 10,
                          ),
                        ),
                        Container(
                          child: TextButton(onPressed: (){
                            _prefs.setCertificateExam(certificateExam: "មិនចាំ");
                              Navigator.pushNamed(context, Routes.INPUTIPHONENUMBERSCREEN);
                          },
                           child: Text("មិនចាំលេខសញ្ញាបត្រ",style: ThemeConstant.texttheme.headline6!.copyWith(shadows: const[Shadow(offset: Offset(0, -2), color: ColorContant.primaryColor)],decorationColor: ColorContant.primaryColor,color: Colors.transparent,decoration: TextDecoration.underline,decorationThickness: 3,fontWeight: FontWeight.w700,),),),
                        ),
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}