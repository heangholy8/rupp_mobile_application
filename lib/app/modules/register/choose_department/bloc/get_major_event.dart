part of 'get_major_bloc.dart';

@immutable
abstract class GetMajorEvent {}

class GetAllMajorEvent extends GetMajorEvent{}
