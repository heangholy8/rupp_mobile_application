import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:rupp_application/app/models/get_major_model/major_model.dart';
import 'package:rupp_application/storages/user_storage.dart';

import '../../../../../services/apis/get_major_api/get_major_api.dart';

part 'get_major_event.dart';
part 'get_major_state.dart';

class GetMajorBloc extends Bloc<GetMajorEvent, GetMajorState> {
  final GetMajorApi getMajor;
  final UserSecureStroage _Prefs = UserSecureStroage();
  GetMajorBloc({required this.getMajor}) : super(GetMajorInitial()) {
    on<GetAllMajorEvent>((event, emit) async {
      emit(GetMajorLoading(message: "Loading....."));
      try {
        var dataMajor = await getMajor.majorRequestApi();
        _Prefs.setMajorModel(majorModel: json.encode(dataMajor));
        emit(GetMajorLoaded(getMajorModel: dataMajor));
        print("Holyholy ${dataMajor.toString()}");
      } catch (e) {
        emit(GetMajorError(error: "Error Data"));
      }
    });
  }
}
