part of 'get_major_bloc.dart';

@immutable
abstract class GetMajorState {}

class GetMajorInitial extends GetMajorState {}

class GetMajorLoading extends GetMajorState{
  final String message;
  GetMajorLoading({required this.message});
}

class GetMajorLoaded extends GetMajorState{
  final MajorModel getMajorModel;
  GetMajorLoaded({required this.getMajorModel});
}

class GetMajorError extends GetMajorState{
  final String error;
  GetMajorError({required this.error});
}