class Major{
  final String? number;
  final String? namemajor;
  final String? status;
  final bool? disable;

  Major({this.number, this.namemajor, this.status, this.disable});
}

class Department {
  String? department;
  List<Major>? major;

  Department({this.department,this.major,});

  static List<Department> generate(){
    return [
      Department(
        department: "មហាវិទ្យាល័យ​វិស្វកម្ម",
        major:[
          Major(
            number: "១",
            namemajor: "វិស្វកម្ម",
            status: "",
            disable: false,
          ),
          Major(
            number: "២",
            namemajor: "បច្ចេកទេស",
            status: "ពេញ",
            disable: true,
          ),
          Major(
            number: "៣",
            namemajor: "សំណង់",
            status: "",
            disable: false,
          ),
          Major(
            number: "៤",
            namemajor: "ព័ត៌មានវិទ្យា",
            status: "ពេញ",
            disable: true,
          ),
          Major(
            number: "៥",
            namemajor: "ជួសជុល",
            status: "",
            disable: false,
          ),
        ]
        
      ),
      Department(
        department: "មហាវិទ្យាល័យភាសាបរទេស",
        major:[
          Major(
            number: "៦",
            namemajor: "ភាសាអង់គ្លេស",
            status: "",
            disable: false,
          ),
          Major(
            number: "៧",
            namemajor: "ភាសាថៃ",
            status: "",
            disable: false,
          ),
          Major(
            number: "៨",
            namemajor: "ភាសាចិន",
            status: "",
            disable: false,
          ),
          Major(
            number: "៩",
            namemajor: "ភាសាជប៉ុន",
            status: "",
            disable: false,
          ),
          Major(
            number: "១០",
            namemajor: "ភាសាកូរ៉េ",
            status: "ពេញ",
            disable: true,
          ),
        ]
        
      ),
    ];
  }
}