import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/register/choose_department/bloc/get_major_bloc.dart';
import 'package:rupp_application/app/modules/register/choose_secession/bloc/getsession_bloc.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/toast_nointernet.dart';

class ChooseDepartmentScreen extends StatefulWidget {
  const ChooseDepartmentScreen({Key? key}) : super(key: key);

  @override
  State<ChooseDepartmentScreen> createState() => _ChooseDepartmentScreenState();
}

class _ChooseDepartmentScreenState extends State<ChooseDepartmentScreen> {
  UserSecureStroage _prefs = UserSecureStroage();
  StreamSubscription? sub;
  bool connection = true;
  void getApi(){
    BlocProvider.of<GetMajorBloc>(context).add(GetAllMajorEvent());
  }
  @override
  void initState() {
    //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection == true){
          setState(() {
          });
        }
        else{
          setState(() {
            connection = false;
          });
        }
      });
    });
   
   //=============Check internet====================
    setState(() {
      getApi();
    });
    super.initState();
  }
  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 12, bottom: 5, left: 25, right: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      child: SvgPicture.asset(
                        ImageAssets.arrow_back_icon,
                        width: 22,
                        height: 22,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Expanded(
                    child: Container(
                      child: Text(
                        "ជំហានទី១",
                        style: ThemeConstant.texttheme.headline6!.copyWith(
                            color: ColorContant.ducktextColor,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: SvgPicture.asset(
                      ImageAssets.arrow_back_icon,
                      width: 22,
                      height: 22,
                      color: Colors.transparent,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 35),
              child: Text(
                "ជ្រើសរើសដេប៉ាតេម៉ង់/ជំនាញ",
                style: ThemeConstant.texttheme.headline6!.copyWith(
                    color: ColorContant.primaryColor,
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Expanded(
              child: BlocBuilder<GetMajorBloc, GetMajorState>(
                builder: (context, state) {
                  if(state is GetMajorLoading){
                    return const Center(child: CircularProgressIndicator(),);
                  }
                  else if(state is GetMajorLoaded){
                    var datafacilty = state.getMajorModel.data!;
                    return SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                top: 12.0, left: 35, right: 35),
                            child: Text(
                              "តើអ្នកចង់រៀនមុខជំនាញអ្វី?",
                              style: ThemeConstant.texttheme.subtitle1!
                                  .copyWith(
                                      color: ColorContant.ducktextColor,
                                      fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          ListView.builder(
                            padding: const EdgeInsets.all(0),
                            physics: const ScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: datafacilty.length,
                            itemBuilder: (context, index) {
                              var datamajor = datafacilty[index].majors;
                              return Container(
                                child: Column(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(
                                          bottom: 5, top: 15),
                                      child: Container(
                                        child: Stack(
                                          alignment:
                                              AlignmentDirectional.center,
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              child: const Divider(
                                                height: 1,
                                                color:
                                                    ColorContant.ducktextColor,
                                                thickness: 0.8,
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.center,
                                              margin: const EdgeInsets.only(
                                                  left: 40, right: 40),
                                              child: Center(
                                                  child: Container(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 8),
                                                      color: ColorContant
                                                          .whiteColor,
                                                      child: Text(datafacilty[index].name.toString(),
                                                        style: ThemeConstant
                                                            .texttheme
                                                            .subtitle1!
                                                            .copyWith(
                                                                color: ColorContant
                                                                    .ducktextColor,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ))),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: ListView.builder(
                                        padding: const EdgeInsets.all(0),
                                        physics: const ScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount:datamajor!.length,
                                        itemBuilder: (context, indexMajor) {
                                          var disable = datamajor[indexMajor].isAvailable==1?false:true;
                                          return Container(
                                            margin: const EdgeInsets.only(
                                                bottom: 14),
                                            child: MaterialButton(
                                              disabledColor: ColorContant
                                                  .duckoptionfullColor,
                                              onPressed: disable == false && datamajor[indexMajor].isSelected==0
                                                  ? connection==false?(){showtoastInternet(context);}:() {
                                                        _prefs.setIndexFaculty(indexFaculty: index);
                                                        _prefs.setIndexMajors(indexMajor: indexMajor);
                                                        Navigator.pushNamed(
                                                            context,
                                                            Routes
                                                                .CHOOSESECESSIONSCREEN);
                                                        BlocProvider.of<GetsessionBloc>(context).add(GetAllSessionEvent());
                                                    }:null,
                                              color: disable == false && datamajor[indexMajor].isSelected==0
                                                  ? ColorContant.primaryColor
                                                  : ColorContant
                                                      .duckoptionfullColor,
                                              shape:
                                                  const RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10))),
                                              padding: const EdgeInsets.only(
                                                  left: 34, right: 18),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    child: Text((indexMajor+1).toString(),
                                                      style: ThemeConstant
                                                          .texttheme.headline3!
                                                          .copyWith(
                                                              color: const Color
                                                                      .fromRGBO(
                                                                  255,
                                                                  255,
                                                                  255,
                                                                  0.5),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 8.0,
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      child: Text(datamajor[indexMajor].name.toString(),
                                                        style: ThemeConstant
                                                            .texttheme
                                                            .headline6!
                                                            .copyWith(
                                                                color: ColorContant
                                                                    .whiteColor,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 8.0,
                                                  ),
                                                  Container(
                                                    child: Text(datamajor[indexMajor].isSelected==1?("( ជ្រើសរួច )"):disable==true?"( ពេញ )":"",
                                                      style: ThemeConstant
                                                          .texttheme.subtitle1!
                                                          .copyWith(
                                                              color:disable == false && datamajor[indexMajor].isSelected==0
                                                                  ? Colors
                                                                      .transparent: ColorContant
                                                                      .whiteColor,
                                                                  
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                  );
                  }
                  else{
                    return Center(child: Text("Error"),);
                  }
                  
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
