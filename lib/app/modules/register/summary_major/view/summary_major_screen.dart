import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/button/button_widget.dart';

class SummaryMajorScreen extends StatefulWidget {
  const SummaryMajorScreen({Key? key}) : super(key: key);

  @override
  State<SummaryMajorScreen> createState() => _SummaryMajorScreenState();
}

class _SummaryMajorScreenState extends State<SummaryMajorScreen> {
  String? orientation_subject_1;
  String? orientation_subject_2;
  String? price;
  String? payType;
  String? majorName;
  String? session;
  String? year;
  String?token;
  String? phoneNumber;
  UserSecureStroage _prefs = UserSecureStroage();
  void getstorage() async{
    var majorModel = await _prefs.getMajorModel;
    var sessionModel =await _prefs.getSessionModel;
    var indexSession = await _prefs.getIndexSession();
    var indexFaculty = await _prefs.getIndexFaculty();
    var indexmajor = await _prefs.getIndexMajors();
    var priceselect = await _prefs.getPriceSelected();
    var paytype = await _prefs.getPayType();
    var accessToken = await _prefs.getLoginSucess();
    var phonenumber = await _prefs.getPhoneNumber();
    setState(() {
      majorName = majorModel.data![indexFaculty!.toInt()].majors![indexmajor!.toInt()].name.toString();
      price = priceselect;
      payType = paytype.toString();
      token = accessToken.toString();
      phoneNumber = phonenumber.toString();
      session = sessionModel.data![indexSession!].name.toString();
      year = majorModel.currentSchoolyear!.name.toString();
      orientation_subject_1 = majorModel.data![indexFaculty.toInt()].majors![indexmajor.toInt()].rule!.orientationSubject1.toString();
      orientation_subject_2 = majorModel.data![indexFaculty.toInt()].majors![indexmajor.toInt()].rule!.orientationSubject2.toString();
    });
  }
  @override
  void initState() {
    setState(() {
      getstorage();
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("សង្ខេបព័ត៌មាន",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 35),
                child: Text("ជម្រើសដែលអ្នកបានជ្រើស",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.symmetric(horizontal: 41),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.symmetric(vertical: 16),
                          width: MediaQuery.of(context).size.width,
                          padding:const EdgeInsets.symmetric(vertical: 16,horizontal: 14),
                          decoration: BoxDecoration(
                            border: Border.all(width: 2,color: ColorContant.primaryColor),
                            borderRadius:const BorderRadius.all(Radius.circular(12))
                          ),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("កម្មវិធីសិក្សា៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text("បរិញ្ញាបត្រ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ដេប៉ាតេម៉ង់៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(majorName.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("វេនសិក្សា៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(session.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ឆ្នាំទី៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text("១",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ឆ្នាំសិក្សា៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(year.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ការបង់ប្រាក់៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text(payType=="2"?"បង់ជាឆ្នាំ":"បង់ជាឆមាស",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: Text("ទឹកប្រាក់ត្រូវបង់៖",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  const SizedBox(width: 24,),
                                  Expanded(
                                    child: Container(
                                      child: Text("\$"+price.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),textAlign: TextAlign.start,),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Text("សូមបញ្ជាក់ព័ត៌មានសង្ខេបខាងលើអោយបានត្រឹមត្រូវមុននឹងបន្ត",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(vertical: 8),
                          child: ButtonWidget(
                            buttonColor: ColorContant.primaryColor,
                            panddinHorButton: 12,
                            panddingVerButton: 7,
                            title: "បន្តចូលបំពេញនិទ្ទេស",
                            textStyleButton: ThemeConstant.texttheme.headline5!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                            radiusButton: 10,
                            onTap: (){
                              if(orientation_subject_1=="--"||orientation_subject_1=="" && orientation_subject_2=="--"||orientation_subject_2=="" ){
                                if(token !="" || phoneNumber!=""){
                                  Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                }else{
                                  Navigator.pushNamed(context, Routes.INPUTAVERAGEEXAMSCREEN);
                                }
                                
                              }
                              else{
                                Navigator.pushNamed(context, Routes.CHOOSEGREADSUBJSCREEN);
                              } 
                            },
                          ),
                        ),
                        Container(
                          margin:const EdgeInsets.only(bottom: 5,top: 15),
                          child: Container(
                            child: Stack(
                              alignment: AlignmentDirectional.center,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child:const Divider(height: 1,color: ColorContant.ducktextColor,thickness: 0.8,),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  margin:const EdgeInsets.only(left: 40,right: 40),
                                  child: Center(child: Container(padding:const EdgeInsets.symmetric(horizontal: 8),color: ColorContant.whiteColor,child: Text("ឬរក្សារព័ត៌មានទុកសិន",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,))),
                                ),
                              ],
                            ),
                          ),
                        ),
                        token==""?Container(
                          margin:const EdgeInsets.symmetric(vertical: 8),
                          child: ButtonWidget(
                            buttonColor: ColorContant.primaryColor,
                            panddinHorButton: 12,
                            panddingVerButton: 5,
                            title: "រក្សារទុកសិន",
                            weightButton: 160,
                            heightButton: 55,
                            textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                            radiusButton: 10,
                            onTap: (){
                              Navigator.pushNamedAndRemoveUntil(context, Routes.OPTIONREGISANDLOGINSCREEN, (route) => false);
                              _prefs.setStep(step: "1");
                            },
                          ),
                        ):Container(),
                        const SizedBox(height: 55,),
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}