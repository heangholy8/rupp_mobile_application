import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/button/button_widget.dart';
import '../controller/grade_exam_model.dart';

class ChooseGradeSubScreen extends StatefulWidget {
  final String typeRout;
  const ChooseGradeSubScreen({Key? key, this.typeRout = "normal"}) : super(key: key);

  @override
  State<ChooseGradeSubScreen> createState() => _ChooseGradeSubScreenState();
}

class _ChooseGradeSubScreenState extends State<ChooseGradeSubScreen> {
  final dataSubjectGrade = Subject.generate();
  String? grade1;
  String? grade2;
  bool checkExamGrade1 =false;
  bool checkExamGrade2 =false;
  String? numberPhone;
  String? token;
  String? orientation_subject_1;
  String? orientation_subject_2;
  UserSecureStroage _prefs = UserSecureStroage();
  void getstorage() async{
    var majorModel = await _prefs.getMajorModel;
    var indexFaculty = await _prefs.getIndexFaculty();
    var indexmajor = await _prefs.getIndexMajors();
    var accessToken = await _prefs.getLoginSucess();
    var numberphone = await _prefs.getPhoneNumber();
    setState(() {
      orientation_subject_1 = majorModel.data![indexFaculty!.toInt()].majors![indexmajor!.toInt()].rule!.orientationSubject1.toString();
      orientation_subject_2 = majorModel.data![indexFaculty.toInt()].majors![indexmajor.toInt()].rule!.orientationSubject2.toString();
      token = accessToken.toString();
      numberPhone = numberphone.toString();
    });
  }
  @override
  void initState() {
    setState(() {
      getstorage();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("និទ្ទេសប្រលង",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.only(left: 28,right: 28,top: 40),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 35),
                          child: Text("សូមជ្រើសរើសនិទ្ទេសដែលអ្នកទទួលបាន",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        Container(
                              child: Column(
                                children: [
                                  orientation_subject_1=="--"||orientation_subject_1==""?Container():Container(
                                    margin:const EdgeInsets.only(bottom: 5,top: 15,left: 12,right: 12),
                                    child: Container(
                                      child: Stack(
                                        alignment: AlignmentDirectional.center,
                                        children: [
                                          Container(
                                            alignment: Alignment.center,
                                            child:const Divider(height: 1,color: ColorContant.primaryColor,thickness: 0.8,),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            margin:const EdgeInsets.only(left: 30,right: 30),
                                            child: Center(
                                              child: Container(
                                                width: MediaQuery.of(context).size.width,
                                                padding:const EdgeInsets.symmetric(horizontal: 8),
                                                color: ColorContant.whiteColor,
                                                 child: Text(orientation_subject_1.toString(),
                                                 style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,)
                                              )
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  orientation_subject_1=="--"||orientation_subject_1==""?Container():Container(
                                    margin:const EdgeInsets.symmetric(vertical: 5),
                                    width: MediaQuery.of(context).size.width,
                                    child: GridView.builder(
                                       physics:const NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: dataSubjectGrade[0].grade!.length,
                                          crossAxisSpacing: 2.0,
                                          mainAxisSpacing: 5.0,
                                        ),
                                      itemCount: dataSubjectGrade[0].grade!.length,
                                      itemBuilder: (BuildContext ctx, indexgrade) {
                                        var gradedata = dataSubjectGrade[0].grade!;
                                        return Container(
                                          padding:const EdgeInsets.symmetric(horizontal: 4),
                                          child: MaterialButton(
                                            elevation: 0,
                                            color:dataSubjectGrade[0].grade![indexgrade].checkgrade==true?ColorContant.primaryColor:ColorContant.whiteColor,
                                            shape:const RoundedRectangleBorder(
                                              side: BorderSide(color: ColorContant.primaryColor,width: 1.5),
                                              borderRadius: BorderRadius.all(Radius.circular(10))),
                                            padding:const EdgeInsets.all(0),
                                            onPressed: (){
                                              setState(() {
                                                for (var element in gradedata) {
                                                  element.checkgrade = false;
                                                }
                                                gradedata[indexgrade].checkgrade = true;
                                                checkExamGrade1 = true;
                                                grade1 = gradedata[indexgrade].grade.toString();
                                                
                                              });
                                            },
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Text(gradedata[indexgrade].grade!,style: ThemeConstant.texttheme.headline6!.copyWith(color:gradedata[indexgrade].checkgrade == true?ColorContant.whiteColor:ColorContant.ducktextColor,fontWeight: FontWeight.w700),),
                                            ),
                                          ),
                                        );
                                      }),
                                  ),
                                  orientation_subject_2=="--"||orientation_subject_2==""?Container():Container(
                                    margin:const EdgeInsets.only(bottom: 5,top: 15,left: 12,right: 12),
                                    child: Container(
                                      child: Stack(
                                        alignment: AlignmentDirectional.center,
                                        children: [
                                          Container(
                                            alignment: Alignment.center,
                                            child:const Divider(height: 1,color: ColorContant.primaryColor,thickness: 0.8,),
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            margin:const EdgeInsets.only(left: 30,right: 30),
                                            child: Center(
                                              child: Container(
                                                width: MediaQuery.of(context).size.width,
                                                padding:const EdgeInsets.symmetric(horizontal: 8),
                                                color: ColorContant.whiteColor,
                                                 child: Text(orientation_subject_2.toString(),
                                                 style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,)
                                              )
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  orientation_subject_2=="--"||orientation_subject_2==""?Container():Container(
                                    margin:const EdgeInsets.symmetric(vertical: 5),
                                    width: MediaQuery.of(context).size.width,
                                    child: GridView.builder(
                                       physics:const NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: dataSubjectGrade[1].grade!.length,
                                          crossAxisSpacing: 2.0,
                                          mainAxisSpacing: 5.0,
                                        ),
                                      itemCount: dataSubjectGrade[1].grade!.length,
                                      itemBuilder: (BuildContext ctx, indexgrade) {
                                        var gradedata = dataSubjectGrade[1].grade!;
                                        return Container(
                                          padding:const EdgeInsets.symmetric(horizontal: 4),
                                          child: MaterialButton(
                                            elevation: 0,
                                            color:dataSubjectGrade[1].grade![indexgrade].checkgrade==true?ColorContant.primaryColor:ColorContant.whiteColor,
                                            shape:const RoundedRectangleBorder(
                                              side: BorderSide(color: ColorContant.primaryColor,width: 1.5),
                                              borderRadius: BorderRadius.all(Radius.circular(10))),
                                            padding:const EdgeInsets.all(0),
                                            onPressed: (){
                                              setState(() {
                                                for (var element in gradedata) {
                                                  element.checkgrade = false;
                                                }
                                                gradedata[indexgrade].checkgrade = true;
                                                checkExamGrade2 = true;
                                                grade2 = gradedata[indexgrade].grade.toString();
                                              });
                                            },
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Text(gradedata[indexgrade].grade!,style: ThemeConstant.texttheme.headline6!.copyWith(color:gradedata[indexgrade].checkgrade == true?ColorContant.whiteColor:ColorContant.ducktextColor,fontWeight: FontWeight.w700),),
                                            ),
                                          ),
                                        );
                                      }),
                                  )
                                ],
                              ),
                            ),
                        Container(
                          margin:const EdgeInsets.only(right: 35,left: 35,top: 25,bottom: 0),
                          child: Text("សូមផ្ទៀងផ្ទាត់និទ្ទេសអោយបានត្រឹមត្រូវមុននឹងបន្ត",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.duckDisableColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(vertical: 12),
                          child: ButtonWidget(
                            buttonColor:checkExamGrade1 && checkExamGrade2 ==false ?ColorContant.duckDisableColor:ColorContant.primaryColor,
                            panddinHorButton: 12,
                            panddingVerButton: 5,
                            title: "បន្ទាប់",
                            weightButton: 110,
                            heightButton: 45,
                            textStyleButton: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                            radiusButton: 10,
                            onTap:checkExamGrade1 && checkExamGrade2 ==true?(){
                              _prefs.setOrientedSubject1Grade(OrientedSubject1Grade: grade1.toString());
                              _prefs.setOrientedSubject2Grade(OrientedSubject2Grade: grade2.toString());
                              if(widget.typeRout=="edit"){
                                   Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                }
                                else{
                                   if(token !="" || numberPhone!=""){
                                      Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                    }
                                    else{
                                      Navigator.pushNamed(context, Routes.INPUTAVERAGEEXAMSCREEN);
                                    }
                                }
                            }:null,
                          ),
                        ),
                        Container(
                          child: TextButton(onPressed: (){
                            _prefs.setOrientedSubject1Grade(OrientedSubject1Grade: "NO");
                            _prefs.setOrientedSubject2Grade(OrientedSubject2Grade: "NO");
                          
                                if(widget.typeRout=="edit"){
                                   Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                }
                                else{
                                   if(token !="" || numberPhone!=""){
                                      Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                    }
                                    else{
                                      Navigator.pushNamed(context, Routes.INPUTAVERAGEEXAMSCREEN);
                                    }
                                }
                            
                              
                          },
                           child: Text("អាចរំលងបាន",style: ThemeConstant.texttheme.headline6!.copyWith(shadows: const[Shadow(offset: Offset(0, -2), color: ColorContant.primaryColor)],decorationColor: ColorContant.primaryColor,color: Colors.transparent,decoration: TextDecoration.underline,decorationThickness: 3,fontWeight: FontWeight.w700,),),),
                        ),
                        const SizedBox(height: 50,)
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}