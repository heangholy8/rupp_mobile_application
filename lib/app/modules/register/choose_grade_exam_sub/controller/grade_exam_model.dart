class Grade{
  final String? grade;
  bool? checkgrade;

  Grade({this.grade, this.checkgrade,});
}

class Subject {
  String? subjectname;
  List<Grade>? grade;

  Subject({this.subjectname,this.grade,});

  static List<Subject> generate(){
    return [
      Subject(
        subjectname: "ជីវវិទ្យា",
        grade:[
          Grade(
            checkgrade: false,
            grade: "A"
          ),
          Grade(
            checkgrade: false,
            grade: "B"
          ),
          Grade(
            checkgrade: false,
            grade: "C"
          ),
          Grade(
            checkgrade: false,
            grade: "D"
          ),
          Grade(
            checkgrade: false,
            grade: "E"
          ),
          Grade(
            checkgrade: false,
            grade: "F"
          ),
        ]
        
      ),
      Subject(
        subjectname: "គីមី",
        grade:[
          Grade(
            checkgrade: false,
            grade: "A"
          ),
          Grade(
            checkgrade: false,
            grade: "B"
          ),
          Grade(
            checkgrade: false,
            grade: "C"
          ),
          Grade(
            checkgrade: false,
            grade: "D"
          ),
          Grade(
            checkgrade: false,
            grade: "E"
          ),
          Grade(
            checkgrade: false,
            grade: "F"
          ),
        ]
        
      ),
    ];
  }
}