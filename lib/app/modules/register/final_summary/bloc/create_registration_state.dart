part of 'create_registration_bloc.dart';

@immutable
abstract class CreateRegistrationState {}

class CreateRegistrationInitial extends CreateRegistrationState {}

class PostRegistrationLoading extends CreateRegistrationState{
  final String message;
  PostRegistrationLoading({required this.message});
}

class PostRegistrationLoaded extends CreateRegistrationState{
  final UserRegisterModel postRegisterModel;
  PostRegistrationLoaded({required this.postRegisterModel,});
}

class PostRegistrationOtherLoaded extends CreateRegistrationState{
  final CreateOtherRegistrationModel postRegisterOhterModel;
  PostRegistrationOtherLoaded({required this.postRegisterOhterModel,});
}

class PostRegistrationError extends CreateRegistrationState{
  final String error;
  PostRegistrationError({required this.error});
}
