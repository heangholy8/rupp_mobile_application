import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:rupp_application/app/models/user_register_model/user_register_model.dart';
import 'package:rupp_application/services/apis/create_registration/create_registration_other_api.dart';
import 'package:rupp_application/storages/user_storage.dart';
import '../../../../../services/apis/create_registration/create_register_api.dart';
import '../../../../models/user_register_model/user_registration_other_model.dart';
part 'create_registration_event.dart';
part 'create_registration_state.dart';

class CreateRegistrationBloc extends Bloc<CreateRegistrationEvent, CreateRegistrationState> {
  final CreateRegistration createRigis;
  CreateRegistrationBloc({required this.createRigis,}) : super(CreateRegistrationInitial()) {
    final UserSecureStroage _prefs = UserSecureStroage();
    on<CreateRegisrationPostEvent>((event, emit) async {
       emit(PostRegistrationLoading(message: "Loading....."));
      try {
        var dataRegistration = await createRigis.postRegistration(
          academicyear_id: event.academicyear_id.toString(), 
          certificate_grade_score: event.certificate_grade_score.toString(), 
          certificate_number: event.certificate_number.toString(), 
          certificate_year: event.certificate_year.toString(), 
          date_of_birth: event.date_of_birth.toString(), 
          degree_id: event.degree_id.toString(), 
          firstname: event.firstname.toString(), 
          fullname_en: event.fullname_en.toString(), 
          gender: event.gender.toString(), 
          grade_orientation: event.grade_orientation.toString(), 
          grade_subject_orientation_1: event.grade_subject_orientation_1.toString(), 
          grade_subject_orientation_2: event.grade_subject_orientation_2.toString(), 
          lastname: event.lastname.toString(), 
          major_id: event.major_id.toString(), 
          name_high_school: event.name_high_school.toString(), 
          pay_type: event.pay_type.toString(), 
          phone: event.phone.toString(), 
          province_city_id: event.province_city_id.toString(), 
          shift_session_id: event.shift_session_id.toString(), 
          stage_id: event.stage_id.toString(),
        );
        _prefs.setLoginSucess(loginsucess: dataRegistration.data!.userInformation!.accessToken.toString());
        _prefs.setusernameLogin(usernamelogin: dataRegistration.data!.userInformation!.code.toString());
        _prefs.setpassword(password: dataRegistration.data!.userInformation!.password.toString());
        _prefs.setStudentId(studentId: dataRegistration.data!.studentId!.toString());
        _prefs.setStep(step: "");
        emit(PostRegistrationLoaded(postRegisterModel: dataRegistration));
      } catch (e) {
        emit(PostRegistrationError(error: "Error Data"));
      }
    });
  }
}

class CreateRegistrationOtherBloc extends Bloc<CreateRegistrationEvent, CreateRegistrationState> {
  CreateRegistrationOther createRegisOther;
  CreateRegistrationOtherBloc({required this.createRegisOther,}) : super(CreateRegistrationInitial()) {
    final UserSecureStroage _prefs = UserSecureStroage();
    on<CreateRegisrationOtherPostEvent>((event, emit) async {
       emit(PostRegistrationLoading(message: "Loading....."));
      try {
        var dataRegistrationOther = await createRegisOther.postRegistrationOther(
          academicyear_id: event.academicyear_id.toString(),  
          degree_id: event.degree_id.toString(),  
          grade_subject_orientation_1: event.grade_subject_orientation_1.toString(), 
          grade_subject_orientation_2: event.grade_subject_orientation_2.toString(), 
          major_id: event.major_id.toString(), 
          pay_type: event.pay_type.toString(), 
          shift_session_id: event.shift_session_id.toString(), 
          stage_id: event.stage_id.toString(), 
          studentid: event.studentid.toString(),
        );
        _prefs.setStep(step: "");
        emit(PostRegistrationOtherLoaded( postRegisterOhterModel: dataRegistrationOther));
      } catch (e) {
        emit(PostRegistrationError(error: "Error Data"));
      }
    });
  }
}
