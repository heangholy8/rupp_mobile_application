part of 'create_registration_bloc.dart';

@immutable
abstract class CreateRegistrationEvent {}

class CreateRegisrationPostEvent extends CreateRegistrationEvent{
    final String? lastname;
     final String? firstname;
     final String? fullname_en;
     final String? gender;
     final String? date_of_birth;
     final String? phone;
     final String? province_city_id;
     final String? name_high_school;
     final String? certificate_year;
     final String? certificate_number;
     final String? certificate_grade_score;
     final String? grade_orientation;
     final String? grade_subject_orientation_1;
     final String? grade_subject_orientation_2;
     final String? degree_id;
     final String? major_id;
     final String? shift_session_id;
     final String? stage_id;
     final String? academicyear_id;
     final String? pay_type; 
  CreateRegisrationPostEvent({required this.lastname,required  this.firstname,required this.fullname_en,required this.gender,required this.date_of_birth,required this.phone,required this.province_city_id,required this.name_high_school,required this.certificate_year,required this.certificate_number,required this.certificate_grade_score,required this.grade_orientation,required this.grade_subject_orientation_1,required this.grade_subject_orientation_2,required this.degree_id,required this.major_id,required this.shift_session_id,required this.stage_id,required this.academicyear_id,required this.pay_type,});
}

class CreateRegisrationOtherPostEvent extends CreateRegistrationEvent{
     final String? grade_subject_orientation_1;
     final String? grade_subject_orientation_2;
     final String? degree_id;
     final String? major_id;
     final String? shift_session_id;
     final String? stage_id;
     final String? academicyear_id;
     final String? pay_type;
     final String? studentid;
  CreateRegisrationOtherPostEvent({required this.grade_subject_orientation_1, required this.grade_subject_orientation_2, required this.degree_id, required this.major_id, required this.shift_session_id, required this.stage_id, required this.academicyear_id, required this.pay_type, required this.studentid,});
}
