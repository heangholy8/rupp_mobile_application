import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/genarate_use_pass_screen/view/genarate_use_pass_screen.dart';
import 'package:rupp_application/app/modules/register/choose_gender/view/choose_gender_screen.dart';
import 'package:rupp_application/app/modules/register/choose_grade_exam/view/choose_grade_exam.dart';
import 'package:rupp_application/app/modules/register/choose_provinces/view/choose_province_screen.dart';
import 'package:rupp_application/app/modules/register/final_summary/bloc/create_registration_bloc.dart';
import 'package:rupp_application/app/modules/register/input_average_exam/view/input_average_exam.dart';
import 'package:rupp_application/app/modules/register/input_date_of_birth/view/input_date_of_birth_screen.dart';
import 'package:rupp_application/app/routes/app_routes.dart';
import 'package:rupp_application/app/widgets/no_connection_alert_widget.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../widgets/button/button_widget.dart';
import '../../../../widgets/toast_nointernet.dart';
import '../../../invoice_screen/view/invoice_screen.dart';
import '../../choose_grade_exam_sub/view/choose_grade_exam_sub.dart';
import '../../choose_provinces/bloc/get_province_bloc.dart';

class SummaryFinalScreen extends StatefulWidget {
  const SummaryFinalScreen({Key? key}) : super(key: key);

  @override
  State<SummaryFinalScreen> createState() => _SummaryFinalScreenState();
}

class _SummaryFinalScreenState extends State<SummaryFinalScreen> {
  late bool isLoading = false;
  late bool isError = false;
  bool connection= true;
   StreamSubscription? sub;
  String? orientation_subject_1;
  String? orientation_subject_2;
  int? price;
  String? payType;
  String? majorName;
  String? majorId;
  String? session;
  String? sessionId;
  String? year;
   String? yearId;
  String? gradetotal;
  String? gradeOrie1;
  String? gradeOrie2;
  String? averageExam;
  String? fristName;
  String? lastName;
  String? fullName;
  String? gender;
  String? dataofBirht;
  String? province;
  String? provinceId;
  String? hightSchool;
  String? yearExam;
  String? certificateExam;
  String? phoneNumber;
  String?token;
  String?studentid;
  UserSecureStroage _prefs = UserSecureStroage();
  void getstorage() async {
    var provinceModel = await _prefs.getProvinceModel;
    var majorModel = await _prefs.getMajorModel;
    var sessionModel = await _prefs.getSessionModel;
    var indexSession = await _prefs.getIndexSession();
    var indexFaculty = await _prefs.getIndexFaculty();
    var indexmajor = await _prefs.getIndexMajors();
    var paytype = await _prefs.getPayType();
    var grade1 = await _prefs.getOrientedSubject1Grade();
    var grade2 = await _prefs.getOrientedSubject2Grade();
    var grade = await _prefs.getOrientedGrade();
    var average = await _prefs.getAverageExam();
    var firstname = await _prefs.getFristName();
    var lastname = await _prefs.getLastName();
    var fullnameEn = await _prefs.getFullNameEn();
    var genders = await _prefs.getGender();
    var datebirth = await _prefs.getDateOfBirht();
    var highschoolyear = await _prefs.getHightSchool();
    var yearexam = await _prefs.getYearExam();
    var certificate = await _prefs.getCertificateExam();
    var phonenumber = await _prefs.getPhoneNumber();
    var indexProvince = await _prefs.getIndexProvince();
    var accessToken = await _prefs.getLoginSucess();
    var studentId = await _prefs.getStudentId();
    setState(() {
      _prefs.setStep(step: "3");
      fristName = firstname.toString();
      lastName = lastname.toString();
      fullName = fullnameEn.toString();
      gender = genders.toString();
      dataofBirht = datebirth.toString();
      province = provinceModel.data![int.parse(indexProvince!)].name.toString();
      provinceId = provinceModel.data![int.parse(indexProvince)].id.toString();
      hightSchool = highschoolyear.toString();
      phoneNumber = phonenumber.toString();
      certificateExam = certificate.toString();
      yearExam = yearexam.toString();
      majorName = majorModel
          .data![indexFaculty!.toInt()].majors![indexmajor!.toInt()].name
          .toString();
      majorId = majorModel
          .data![indexFaculty.toInt()].majors![indexmajor.toInt()].majorId
          .toString();
      payType = paytype.toString();
      session = sessionModel.data![indexSession!].name.toString();
      sessionId = sessionModel.data![indexSession].shiftSessionId.toString();
      year = majorModel.currentSchoolyear!.name.toString();
      yearId = majorModel.currentSchoolyear!.id.toString();
      gradetotal = grade.toString();
      gradeOrie1 = grade1.toString();
      gradeOrie2 = grade2.toString();
      averageExam = average.toString();
      orientation_subject_1 = majorModel.data![indexFaculty.toInt()]
          .majors![indexmajor.toInt()].rule!.orientationSubject1
          .toString();
      orientation_subject_2 = majorModel.data![indexFaculty.toInt()]
          .majors![indexmajor.toInt()].rule!.orientationSubject2
          .toString();
      price = (payType == "2"
          ? (majorModel.data![indexFaculty].majors![indexmajor].schoolFee!
                      .domesticAmount! +
                  5)
              .toInt()
          : ((majorModel.data![indexFaculty].majors![indexmajor].schoolFee!
                          .domesticAmount! /
                      2) +
                  5)
              .toInt());
        token = accessToken.toString();
        studentid = studentId.toString();
    });
  }

  @override
  void initState() {
    setState(() {
      //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection == true){
          setState(() {
          });
        }
        else{
          setState(() {
            connection = false;
          });
        }
      });
    });
   
   //=============Check internet====================
      getstorage();
    });
    super.initState();
  }
  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: WillPopScope(
          onWillPop: () async{
            return false;
          },
          child: Container(
            child: Column(
              children: [
                connection==false?const NoConnectWidget():Container(),
                Container(
                  margin: const EdgeInsets.only(
                      top: 12, bottom: 5, left: 25, right: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // GestureDetector(
                      //   onTap: () {
                      //     Navigator.of(context).pop();
                      //   },
                      //   child: Container(
                      //     padding: const EdgeInsets.all(10),
                      //     child: SvgPicture.asset(
                      //       ImageAssets.arrow_back_icon,
                      //       width: 22,
                      //       height: 22,
                      //     ),
                      //   ),
                      // ),
                      const SizedBox(
                        width: 2,
                      ),
                      Expanded(
                        child: Container(
                          child: Text(
                            "សង្ខេបព័ត៌មាន",
                            style: ThemeConstant.texttheme.headline6!.copyWith(
                                color: ColorContant.ducktextColor,
                                fontWeight: FontWeight.w700),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 2,
                      ),
                      // Container(
                      //   padding: const EdgeInsets.all(10),
                      //   child: SvgPicture.asset(
                      //     ImageAssets.arrow_back_icon,
                      //     width: 22,
                      //     height: 22,
                      //     color: Colors.transparent,
                      //   ),
                      // ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 35),
                  child: Text(
                    "ការចុះឈ្មោះ",
                    style: ThemeConstant.texttheme.headline6!.copyWith(
                        color: ColorContant.primaryColor,
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(
                  height: 6,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 41),
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.symmetric(vertical: 16),
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.symmetric(
                                vertical: 16, horizontal: 14),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 2, color: ColorContant.primaryColor),
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(12))),
                            child: Column(
                              children: [
                                Container(
                                  margin:
                                      const EdgeInsets.only(bottom: 5, top: 0),
                                  child: Container(
                                    child: Stack(
                                      alignment: AlignmentDirectional.center,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          child: const Divider(
                                            height: 1,
                                            color: ColorContant.primaryColor,
                                            thickness: 0.8,
                                          ),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          margin: const EdgeInsets.only(
                                              left: 40, right: 40),
                                          child: Center(
                                              child: Container(
                                                  padding:
                                                      const EdgeInsets.symmetric(
                                                          horizontal: 8),
                                                  color: ColorContant.whiteColor,
                                                  child: Text(
                                                    "សូមចុចលើព័ត៌មានដែលចង់ កែសម្រួល ដើម្បីកែប្រែ",
                                                    style: ThemeConstant
                                                        .texttheme.subtitle1!
                                                        .copyWith(
                                                            color: ColorContant
                                                                .primaryColor,
                                                            fontWeight:
                                                                FontWeight.w400),
                                                    textAlign: TextAlign.center,
                                                  ))),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "គោត្តនាម៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                            ShowDailogEdit(context, 
                                            editNumber: 1,
                                            keytype: TextInputType.name,
                                            subtitleEdit: 'គោត្តនាម', 
                                            title: 'កែប្រែ គោត្តនាម', 
                                            texteditcontroller: TextEditingController(text: fristName.toString())
                                          );
                                        },
                                        child: Container(
                                          child: Text(
                                            fristName.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "នាម៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                            ShowDailogEdit(context, 
                                            editNumber: 2,
                                            keytype: TextInputType.name,
                                            subtitleEdit: 'នាម', 
                                            title: 'កែប្រែ នាម', 
                                            texteditcontroller: TextEditingController(text: lastName.toString())
                                          );
                                        },
                                        child: Container(
                                          child: Text(
                                            lastName.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "គោត្តនាម & នាម៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                            ShowDailogEdit(context, 
                                            editNumber: 3,
                                            keytype: TextInputType.name,
                                            subtitleEdit: 'គោត្តនាម & នាម', 
                                            title: 'កែប្រែ គោត្តនាម & នាម', 
                                            texteditcontroller: TextEditingController(text: fullName.toString())
                                          );
                                        },
                                        child: Container(
                                          child: Text(
                                            fullName.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ភេទ៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: (){
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseGenderScreen(typrRout:"edit")));
                                        },
                                        child: Container(
                                          child: Text(
                                            gender == "1" ? "ប្រុស" : "ស្រី",
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ថ្ងៃខែឆ្នាំកំណើត៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: (){
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => const InputDateOfBirthScreen(typrRout:"edit")));
                                        },
                                        child: Container(
                                          child: Text(
                                            dataofBirht.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ខេត្តកំណើត៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: (){
                                          BlocProvider.of<GetProvinceBloc>(context).add(GetProvince());
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseProvinceScreen(typeRout:"edit")));
                                        },
                                        child: Container(
                                          child: Text(
                                            province.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "មកពីវិទ្យាល័យ៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                            ShowDailogEdit(context, 
                                            editNumber: 4,
                                            keytype: TextInputType.name,
                                            subtitleEdit: 'មកពីវិទ្យាល័យ', 
                                            title: 'កែប្រែ វិទ្យាល័យ', 
                                            texteditcontroller: TextEditingController(text: hightSchool.toString())
                                          );
                                        },
                                        child: Container(
                                          child: Text(
                                            hightSchool.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ឆ្នាំជាប់បាក់ឌុប៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                            ShowDailogEdit(context, 
                                            editNumber: 5,
                                            keytype: TextInputType.number,
                                            subtitleEdit: 'ឆ្នាំជាប់បាក់ឌុប', 
                                            title: 'កែប្រែ ឆ្នាំជាប់បាក់ឌុប', 
                                            texteditcontroller: TextEditingController(text: yearExam.toString())
                                          );
                                        },
                                        child: Container(
                                          child: Text(
                                            yearExam.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "លេខសញ្ញាបត្រ៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                            ShowDailogEdit(context, 
                                            editNumber: 6,
                                            keytype: TextInputType.number,
                                            subtitleEdit: 'លេខសញ្ញាបត្រ', 
                                            title: 'កែប្រែ លេខសញ្ញាបត្រ', 
                                            texteditcontroller: TextEditingController(text: certificateExam.toString())
                                          );
                                        },
                                        child: Container(
                                          child: Text(
                                            certificateExam.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "លេខទូរស័ព្ទ៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          phoneNumber.toString(),
                                          style: ThemeConstant
                                              .texttheme.headline6!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Text(
                                    "និទ្ទេសប្រឡង",
                                    style: ThemeConstant.texttheme.subtitle1!
                                        .copyWith(
                                            color: ColorContant.primaryColor,
                                            fontWeight: FontWeight.w400),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                orientation_subject_1 == "--" ||
                                        orientation_subject_1 == ""
                                    ? Container()
                                    : Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                orientation_subject_1.toString(),
                                                style: ThemeConstant
                                                    .texttheme.subtitle1!
                                                    .copyWith(
                                                        color: ColorContant
                                                            .ducktextColor,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                textAlign: TextAlign.end,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 24,
                                          ),
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: (){
                                                Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseGradeSubScreen(typeRout:"edit")));
                                              },
                                              child: Container(
                                                child: Text(
                                                  gradeOrie1 == "NO"
                                                      ? "មិនមាន"
                                                      : gradeOrie1.toString(),
                                                  style: ThemeConstant
                                                      .texttheme.headline6!
                                                      .copyWith(
                                                          color: ColorContant
                                                              .ducktextColor,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                orientation_subject_2 == "--" ||
                                        orientation_subject_2 == ""
                                    ? Container()
                                    : Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                orientation_subject_2.toString(),
                                                style: ThemeConstant
                                                    .texttheme.subtitle1!
                                                    .copyWith(
                                                        color: ColorContant
                                                            .ducktextColor,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                textAlign: TextAlign.end,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 24,
                                          ),
                                          Expanded(
                                            child: GestureDetector(
                                              onTap: (){
                                                Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseGradeSubScreen(typeRout:"edit")));
                                              },
                                              child: Container(
                                                child: Text(
                                                  gradeOrie2 == "NO"
                                                      ? "មិនមាន"
                                                      : gradeOrie2.toString(),
                                                  style: ThemeConstant
                                                      .texttheme.headline6!
                                                      .copyWith(
                                                          color: ColorContant
                                                              .ducktextColor,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "លំដាប់ពិន្ទុរួម៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: (){
                                                Navigator.push(context, MaterialPageRoute(builder: (context) => const InputAveragrExamScreen(typeRout:"edit")));
                                              },
                                        child: Container(
                                          child: Text(
                                            averageExam.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "និទ្ទេសរួម៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: (){
                                                Navigator.push(context, MaterialPageRoute(builder: (context) => const ChooseGradeScreen(typeRout:"edit")));
                                              },
                                        child: Container(
                                          child: Text(
                                            gradetotal.toString(),
                                            style: ThemeConstant
                                                .texttheme.headline6!
                                                .copyWith(
                                                    color:
                                                        ColorContant.ducktextColor,
                                                    fontWeight: FontWeight.w700),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  margin:
                                      const EdgeInsets.only(bottom: 5, top: 0),
                                  child: Container(
                                    child: Stack(
                                      alignment: AlignmentDirectional.center,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          child: const Divider(
                                            height: 1,
                                            color: ColorContant.primaryColor,
                                            thickness: 0.8,
                                          ),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          margin: const EdgeInsets.only(
                                              left: 40, right: 40),
                                          child: Center(
                                              child: Container(
                                                  padding:
                                                      const EdgeInsets.symmetric(
                                                          horizontal: 8),
                                                  color: ColorContant.whiteColor,
                                                  child: Text(
                                                    "សូមចុចលើព័ត៌មានដែលចង់ កែសម្រួល ដើម្បីកែប្រែ",
                                                    style: ThemeConstant
                                                        .texttheme.subtitle1!
                                                        .copyWith(
                                                            color: ColorContant
                                                                .primaryColor,
                                                            fontWeight:
                                                                FontWeight.w400),
                                                    textAlign: TextAlign.center,
                                                  ))),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "កម្មវិធីសិក្សា៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "បរិញ្ញាបត្រ",
                                          style: ThemeConstant
                                              .texttheme.headline6!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ដេប៉ាតេម៉ង់៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          majorName.toString(),
                                          style: ThemeConstant
                                              .texttheme.headline6!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "វេនសិក្សា៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          session.toString(),
                                          style: ThemeConstant
                                              .texttheme.headline6!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ឆ្នាំទី៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "1",
                                          style: ThemeConstant
                                              .texttheme.headline6!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ឆ្នាំសិក្សា៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          year.toString(),
                                          style: ThemeConstant
                                              .texttheme.headline6!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          "ការបង់ប្រាក់៖",
                                          style: ThemeConstant
                                              .texttheme.subtitle1!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w400),
                                          textAlign: TextAlign.end,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 24,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          payType == "2"
                                              ? "បង់ជាឆ្នាំ"
                                              : "បង់ជាឆមាស",
                                          style: ThemeConstant
                                              .texttheme.headline6!
                                              .copyWith(
                                                  color:
                                                      ColorContant.ducktextColor,
                                                  fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: const EdgeInsets.symmetric(vertical: 8),
                                  child: Text(
                                    "សិស្សទាំងអស់ ត្រូវបង់ប្រាក់ថ្លៃរដ្ឋបាល\$5",
                                    style: ThemeConstant.texttheme.subtitle1!
                                        .copyWith(
                                            color: ColorContant.primaryColor,
                                            fontWeight: FontWeight.w400),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    "ទឹកប្រាក់ត្រូវបង់៖",
                                    style: ThemeConstant.texttheme.subtitle1!
                                        .copyWith(
                                            color: ColorContant.ducktextColor,
                                            fontWeight: FontWeight.w400),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    "\$" + price.toString(),
                                    style: ThemeConstant.texttheme.headline6!
                                        .copyWith(
                                            color: ColorContant.primaryColor,
                                            fontWeight: FontWeight.w700),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.symmetric(vertical: 8),
                                  child: ButtonWidget(
                                    buttonColor: ColorContant.primaryColor,
                                    panddinHorButton: 12,
                                    panddingVerButton: 5,
                                    title: "កែសម្រួល",
                                    weightButton: 120,
                                    heightButton: 45,
                                    textStyleButton: ThemeConstant
                                        .texttheme.subtitle1!
                                        .copyWith(
                                            color: ColorContant.whiteColor,
                                            fontWeight: FontWeight.w700),
                                    radiusButton: 10,
                                    onTap:connection==false?(){showtoastInternet(context);}: () {
                                      Navigator.pushNamed(context, Routes.CHOOSEDEPARTMENTSCREEN);
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Text(
                              "សូមបញ្ជាក់ព័ត៌មានសង្ខេបខាងលើអោយបានត្រឹមត្រូវមុននឹងបន្ត",
                              style: ThemeConstant.texttheme.subtitle1!.copyWith(
                                  color: ColorContant.ducktextColor,
                                  fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center,
                            ),
                          ),
        
                          token == ""?BlocListener<CreateRegistrationBloc, CreateRegistrationState>(
                            listener: (context, state) {
                              if(state is PostRegistrationLoading){
                                setState(() {
                                  isLoading = true;
                                  isError = false;
                                });
                              }
                              if(state is PostRegistrationLoaded){
                                setState(() {
                                  isLoading = false;
                                  isError = false;
                                });
                                 Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const GenarateUserPassScreen()),
                                      (Route<dynamic> route) => false);
                              }
                              if(state is PostRegistrationError){
                                setState(() {
                                isLoading = false;
                                isError = true;
                                });  
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(vertical: 8),
                              child:isLoading==false?ButtonWidget(
                                buttonColor: ColorContant.primaryColor,
                                panddinHorButton: 12,
                                panddingVerButton: 7,
                                title: "បញ្ចប់ការចុះឈ្មោះ",
                                textStyleButton:
                                    ThemeConstant.texttheme.headline5!.copyWith(
                                        color: ColorContant.whiteColor,
                                        fontWeight: FontWeight.w700),
                                radiusButton: 10,
                                onTap: connection==false?(){showtoastInternet(context);}: () {
                                  setState(() {
                                    isLoading = true;
                                     BlocProvider.of<CreateRegistrationBloc>(context).add(CreateRegisrationPostEvent(
                                      firstname: fristName.toString(), 
                                      lastname: lastName.toString(), 
                                      fullname_en: fullName.toString(), 
                                      gender: gender.toString(), 
                                      date_of_birth: dataofBirht.toString(), 
                                      phone: phoneNumber.toString(), 
                                      province_city_id: provinceId.toString(), 
                                      major_id: majorId.toString(), 
                                      name_high_school: hightSchool.toString(), 
                                      shift_session_id: sessionId.toString(), 
                                      certificate_number: certificateExam.toString(), 
                                      certificate_year: yearExam.toString(), 
                                      certificate_grade_score: averageExam.toString(), 
                                      academicyear_id: yearId.toString(), 
                                      degree_id: '1', 
                                      grade_orientation: gradetotal==""?"មិនមាន":gradetotal.toString(), 
                                      grade_subject_orientation_1: gradeOrie1=="" || gradeOrie1=="NO"?"មិនមាន":gradeOrie1.toString(), 
                                      grade_subject_orientation_2: gradeOrie2=="" || gradeOrie2=="NO"?"មិនមាន":gradeOrie2.toString(), 
                                      pay_type: payType.toString(), 
                                      stage_id: '1'
                                     ));
                                  });
                                },
                              ):const Center(child: CircularProgressIndicator(),),
                            ),
                          ):BlocListener<CreateRegistrationOtherBloc, CreateRegistrationState>(
                            listener: (context, state) {
                              if(state is PostRegistrationLoading){
                                setState(() {
                                  isLoading = true;
                                  isError = false;
                                });
                              }
                              if(state is PostRegistrationOtherLoaded){
                                setState(() {
                                  isLoading = false;
                                  isError = false;
                                });
                                 Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                               InvoiceScreen(routType: 1,)),
                                      (Route<dynamic> route) => false);
                              }
                              if(state is PostRegistrationError){
                                setState(() {
                                isLoading = false;
                                isError = true;
                                });  
                              }
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(vertical: 8),
                              child:isLoading==false?ButtonWidget(
                                buttonColor: ColorContant.primaryColor,
                                panddinHorButton: 12,
                                panddingVerButton: 7,
                                title: "បញ្ចប់ការចុះឈ្មោះ",
                                textStyleButton:
                                    ThemeConstant.texttheme.headline5!.copyWith(
                                        color: ColorContant.whiteColor,
                                        fontWeight: FontWeight.w700),
                                radiusButton: 10,
                                onTap:connection==false?(){showtoastInternet(context);}: () {
                                  setState(() {
                                    isLoading = true;
                                     BlocProvider.of<CreateRegistrationOtherBloc>(context).add(CreateRegisrationOtherPostEvent(
                                      major_id: majorId.toString(), 
                                      shift_session_id: sessionId.toString(), 
                                      academicyear_id: yearId.toString(), 
                                      degree_id: '1', 
                                      grade_subject_orientation_1: gradeOrie1=="" || gradeOrie1=="NO"?"មិនមាន":gradeOrie1.toString(), 
                                      grade_subject_orientation_2: gradeOrie2=="" || gradeOrie2=="NO"?"មិនមាន":gradeOrie2.toString(), 
                                      pay_type: payType.toString(), 
                                      stage_id: '1', 
                                      studentid: studentid.toString(),
                                     ));
                                  });
                                },
                              ):const Center(child: CircularProgressIndicator(),),
                            ),
                          ),
                          isError==true? Container(
                            child: Text(
                              "ការចុះឈ្មោះរបស់អ្នកបរាជ័យ។​​ សូមត្រួតពិនិត្យព័ត៌មានរបស់អ្នកឡើងវិញ សូមព្យាយាមម្ដងទៀត!",
                              style: ThemeConstant.texttheme.headline6!.copyWith(
                                  color: ColorContant.primaryColor,
                                  fontWeight: FontWeight.w400),
                              textAlign: TextAlign.center,
                            ),
                          ):Container(),
                          const SizedBox(
                            height: 55,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future ShowDailogEdit(context,{required TextEditingController? texteditcontroller,required TextInputType? keytype,required String? title,required String? subtitleEdit,required int? editNumber,}) async {
    showDialog(
      barrierDismissible: true,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return AlertDialog(
          shape:const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
          content: Container(
            height: 240,
            decoration:const BoxDecoration(
              color: ColorContant.whiteColor,
              borderRadius: BorderRadius.all(Radius.circular(12))
            ),
            child: Column(
              children: [
                Container(
                  margin:const EdgeInsets.symmetric(vertical: 12,horizontal: 22),
                  child: Text(title.toString(),style: ThemeConstant.texttheme.headline6,),
                ),
                Container(
                  margin:const EdgeInsets.symmetric(horizontal: 16),
                   alignment: Alignment.centerLeft,
                  child: Text(subtitleEdit.toString(),style: ThemeConstant.texttheme.subtitle1,),
                ),
                Container(
                  margin:const EdgeInsets.symmetric(vertical: 12,horizontal: 16),
                  child: TextFormField(
                    focusNode: FocusNode(),
                    autofocus: true,
                    keyboardType: keytype,
                    controller: texteditcontroller,
                    textAlign: TextAlign.center,
                    decoration:const InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                      borderSide: BorderSide(color: ColorContant.primaryColor),
                      ),
                      focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                      borderSide: BorderSide(color: ColorContant.primaryColor),
                      ),
                      contentPadding: EdgeInsets.only(
                        top: 12.0, bottom: 12.0),
                          ),
                  ),
                ),
                Container(
                  child: ButtonWidget(
                    buttonColor: ColorContant.primaryColor,
                    textStyleButton: ThemeConstant
                        .texttheme.subtitle1!
                        .copyWith(
                            color: ColorContant.whiteColor,
                            fontWeight: FontWeight.w700),
                    weightButton: 110,
                    panddingVerButton: 6,
                    panddinHorButton: 16,
                    onTap: (){
                      Navigator.of(context).pop();
                      if(editNumber! ==1){
                        fristName = texteditcontroller!.text;
                        _prefs.setFristName(fristname: texteditcontroller.text);
                      }
                      else if(editNumber ==2){
                        lastName = texteditcontroller!.text;
                        _prefs.setLastName(lastname: texteditcontroller.text);
                      }
                      else if(editNumber ==3){
                        fullName = texteditcontroller!.text;
                         _prefs.setFullNameLatin(fullLatinname: texteditcontroller.text);
                      }
                      else if(editNumber ==4){
                        hightSchool = texteditcontroller!.text;
                        _prefs.setHightSchool(hightSchool: texteditcontroller.text);
                      }
                      else if(editNumber ==5){
                        yearExam = texteditcontroller!.text;
                        _prefs.setYearExam(yearExam: texteditcontroller.text);
                      }
                      else if(editNumber ==6){
                        certificateExam = texteditcontroller!.text;
                        _prefs.setCertificateExam(certificateExam: texteditcontroller.text);
                      }
                    },
                    title: "កែប្រែ",
                    radiusButton: 10,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
