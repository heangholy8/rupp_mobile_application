import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';

class ChooseGenderScreen extends StatefulWidget {
  final String typrRout;
  const ChooseGenderScreen({Key? key, this.typrRout = "normal"}) : super(key: key);

  @override
  State<ChooseGenderScreen> createState() => _ChooseGenderScreenState();
}

class _ChooseGenderScreenState extends State<ChooseGenderScreen> {
  UserSecureStroage _prefs = UserSecureStroage();
  @override
  void initState() {
    setState(() {
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("ការចុះឈ្មោះ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.only(top: 60,left: 35,right: 35,bottom: 30),
                          child: Text("ភេទ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        const SizedBox(height: 8,),
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 10),
                          width: MediaQuery.of(context).size.width,
                          child: MaterialButton(
                            onPressed: (){
                              _prefs.setGender(gender: "1");
                              if(widget.typrRout=="edit"){
                                Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                              }
                              else{
                                Navigator.pushNamed(context, Routes.INPUTIDATEOFBIRTHSCREEN);
                              }
                            },
                            color:ColorContant.primaryColor,
                                  shape:const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                  padding:const EdgeInsets.only(left: 34,right: 18,top: 40,bottom: 40),
                            child: Container(
                              child: Text("ប្រុស",style: ThemeConstant.texttheme.headline5!.copyWith(color:ColorContant.whiteColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                            ),
                          ),
                        ),
                        const SizedBox(height: 37,),
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 10),
                          width: MediaQuery.of(context).size.width,
                          child: MaterialButton(
                            onPressed: (){
                              _prefs.setGender(gender: "2");
                              if(widget.typrRout=="edit"){
                                Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                              }
                              else{
                                Navigator.pushNamed(context, Routes.INPUTIDATEOFBIRTHSCREEN);
                              }
                            },
                            color:ColorContant.primaryColor,
                                  shape:const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                  padding:const EdgeInsets.only(left: 34,right: 18,top: 40,bottom: 40),
                            child: Container(
                              child: Text("ស្រី",style: ThemeConstant.texttheme.headline5!.copyWith(color:ColorContant.whiteColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}