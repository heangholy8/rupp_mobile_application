import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/register/choose_provinces/bloc/get_province_bloc.dart';
import 'package:rupp_application/app/modules/register/choose_secession/controller/choose_secession_model.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/button/button_widget.dart';
import '../../../../widgets/textfromfiled_custom.dart';

class InputDateOfBirthScreen extends StatefulWidget {
  final String typrRout;
  const InputDateOfBirthScreen({Key? key,  this.typrRout = "normal"}) : super(key: key);

  @override
  State<InputDateOfBirthScreen> createState() => _InputDateOfBirthScreenState();
}

class _InputDateOfBirthScreenState extends State<InputDateOfBirthScreen> {
  final datasecession = SecessionData.generate();
  UserSecureStroage _prefs = UserSecureStroage();
  TextEditingController datecontroller = TextEditingController();
  TextEditingController monthcontroller = TextEditingController();
  TextEditingController yearcontroller = TextEditingController();
  FocusNode focusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("ការចុះឈ្មោះ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: Container(
                  margin:const EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(top: 60,left: 35,right: 35,bottom: 10),
                        child: Text("ថ្ងៃ ខែ ឆ្នាំ កំណើត",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                      ),
                      const SizedBox(height: 8,),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Text("ថ្ងៃ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                                Container(
                                  width: 70,
                                  child: TextFormField(
                                    maxLength: 2,
                                    onChanged:(value) {
                                    setState(() {
                                      if(int.parse(value)>31) {
                                       datecontroller.text = "";
                                      }
                                      else if(datecontroller.text.length==2){
                                        FocusScope.of(context).nextFocus();
                                      }
                                      datecontroller.text;
                                    });
                                    },
                                    controller: datecontroller,
                                    textAlign: TextAlign.center,
                                    style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor),
                                    keyboardType: TextInputType.number,
                                    autofocus: true,
                                    focusNode: focusNode,
                                    inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                                    decoration:const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                        borderSide: BorderSide(color: ColorContant.primaryColor),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                        borderSide: BorderSide(color: ColorContant.primaryColor),
                                        ),
                                        contentPadding: EdgeInsets.only(
                                          top: 12.0, bottom: 12.0),
                                            ),
                                  ),
                                  
                                ),
                              ],
                            ),
                            const SizedBox(width: 18,),
                            Column(
                              children: [
                                Text("ខែ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                                Container(
                                  width: 70,
                                  child: TextFormField(
                                    maxLength: 2,
                                    onChanged:(value) {
                                    setState(() {
                                      if(int.parse(value)>12) {
                                       monthcontroller.text = "";
                                      }
                                      else if(monthcontroller.text.length==2){
                                        FocusScope.of(context).nextFocus();
                                      }
                                      monthcontroller.text;
                                    });
                                    },
                                    controller: monthcontroller,
                                    textAlign: TextAlign.center,
                                    style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor),
                                    keyboardType: TextInputType.number,
                                    autofocus: true,
                                    inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                                    decoration:const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                        borderSide: BorderSide(color: ColorContant.primaryColor),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                        borderSide: BorderSide(color: ColorContant.primaryColor),
                                        ),
                                        contentPadding: EdgeInsets.only(
                                          top: 12.0, bottom: 12.0),
                                            ),
                                  ),
                                  
                                ),
                              ],
                            ),
                            const SizedBox(width: 18,),
                            Column(
                              children: [
                                Text("ឆ្នាំ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                                Container(
                                  width: 120,
                                  child: TextFormField(
                                    maxLength: 4,
                                    onChanged:(value) {
                                    setState(() {

                                      yearcontroller.text;
                                    });
                                    },
                                    controller: yearcontroller,
                                    textAlign: TextAlign.center,
                                    style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor),
                                    keyboardType: TextInputType.number,
                                    autofocus: true,
                                    inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                                    decoration:const InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                        borderSide: BorderSide(color: ColorContant.primaryColor),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                        borderSide: BorderSide(color: ColorContant.primaryColor),
                                        ),
                                        contentPadding: EdgeInsets.only(
                                          top: 12.0, bottom: 12.0),
                                            ),
                                  ),
                                  
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin:const EdgeInsets.only(top: 50,bottom: 20),
                        child: ButtonWidget(
                          buttonColor:datecontroller.text!=""&&monthcontroller.text!=""&&yearcontroller.text!=""?ColorContant.primaryColor:ColorContant.duckDisableColor,
                          textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                          weightButton: 140,
                          panddingVerButton: 5,
                          panddinHorButton: 16,
                          onTap:datecontroller.text=="" && monthcontroller.text=="" && yearcontroller.text ==""?null:(){
                            //FocusManager.instance.primaryFocus?.unfocus();
                            _prefs.setDateOfBirth(birth: datecontroller.text+"/"+monthcontroller.text+"/"+yearcontroller.text);
                            BlocProvider.of<GetProvinceBloc>(context).add(GetProvince());
                            if(widget.typrRout=="edit"){
                                Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                              }
                              else{
                                Navigator.pushNamed(context, Routes.CHOOSEPROVINCESCREEN);
                              }  
                          },
                          title: "បន្ទាប់",
                          radiusButton: 10,
                        ),
                      ),
                    ],
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}