class ProvinceDataModel {
  final String provincename;
  final String idprovince;
  const ProvinceDataModel({required this.idprovince,required this.provincename,});

  static List<ProvinceDataModel> generate(){
    return [
      const ProvinceDataModel(idprovince:"01",provincename: "រាជធានីភ្នំពេញ"),
      const ProvinceDataModel(idprovince:"02",provincename: "ខេត្តតាកែវ"),
      const ProvinceDataModel(idprovince:"03",provincename: "ខេត្តកណ្តាល"),
      const ProvinceDataModel(idprovince:"04",provincename: "ខេត្តកំពង់ស្ពឺ"),
      const ProvinceDataModel(idprovince:"05",provincename: "ខេត្តកំពង់ចាម"),
      const ProvinceDataModel(idprovince:"06",provincename: "ខេត្តកំពង់ធំ"),
      const ProvinceDataModel(idprovince:"07",provincename: "ខេត្តកំពត"),
      const ProvinceDataModel(idprovince:"08",provincename: "ខេត្តព្រៃវែង"),
      const ProvinceDataModel(idprovince:"09",provincename: "ខេត្តបាត់ដំបង"),
      const ProvinceDataModel(idprovince:"10",provincename: "ខេត្តកំពង់ឆ្នាំង"),
      const ProvinceDataModel(idprovince:"11",provincename: "ខេត្តស្វាយរៀង"),
      const ProvinceDataModel(idprovince:"12",provincename: "ខេត្តពោធិសាត់"),
      const ProvinceDataModel(idprovince:"13",provincename: "ខេត្តរតនៈគិរី"),
      const ProvinceDataModel(idprovince:"14",provincename: "ខេត្តមណ្ឌលគិរី"),
      const ProvinceDataModel(idprovince:"15",provincename: "ខេត្តបន្ទាយមានជ័យ"),
      const ProvinceDataModel(idprovince:"16",provincename: "ខេត្តឧត្ដរមានជ័យ"),
      const ProvinceDataModel(idprovince:"17",provincename: "ខេត្តកោះកុង"),
      const ProvinceDataModel(idprovince:"18",provincename: "ខេត្តព្រះសីហនុ"),
      const ProvinceDataModel(idprovince:"19",provincename: "ខេត្តក្រចេះ"),
      const ProvinceDataModel(idprovince:"20",provincename: "ខេត្តត្បូងឃ្មុំ"),
      const ProvinceDataModel(idprovince:"21",provincename: "ខេត្តសៀមរាប"),
      const ProvinceDataModel(idprovince:"22",provincename: "ខេត្តប៉ៃលិន"),
      const ProvinceDataModel(idprovince:"23",provincename: "ខេត្តស្ទឹងត្រែង"),
      const ProvinceDataModel(idprovince:"24",provincename: "កែប"),
      const ProvinceDataModel(idprovince:"25",provincename: "ព្រះវិហារ"),
    
    ];
  }
}