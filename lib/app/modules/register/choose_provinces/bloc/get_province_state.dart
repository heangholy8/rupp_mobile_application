part of 'get_province_bloc.dart';

@immutable
abstract class GetProvinceState {}

class GetProvinceInitial extends GetProvinceState {}

class GetProvinceLoading extends GetProvinceState{
  final String message;
  GetProvinceLoading({required this.message});
}

class GetProvinceLoaded extends GetProvinceState{
  final ProvinceModel getProvinceModel;
  GetProvinceLoaded({required this.getProvinceModel,});
}

class GetProvinceError extends GetProvinceState{
  final String error;
  GetProvinceError({required this.error});
}
