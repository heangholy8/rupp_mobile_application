part of 'get_province_bloc.dart';

@immutable
abstract class GetProvinceEvent {}

class GetProvince extends GetProvinceEvent{}
