import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:rupp_application/services/apis/get_province/get_province_api.dart';

import '../../../../../storages/user_storage.dart';
import '../../../../models/province_model/province_model.dart';

part 'get_province_event.dart';
part 'get_province_state.dart';

class GetProvinceBloc extends Bloc<GetProvinceEvent, GetProvinceState> {
  final  GetProvinceApi getProvinceApi;
  final UserSecureStroage _Prefs = UserSecureStroage();
  GetProvinceBloc({required this.getProvinceApi}) : super(GetProvinceInitial()) {
    on<GetProvince>((event, emit) async{
      emit(GetProvinceLoading(message: "Loading....."));
      try {
        var dataProvince = await getProvinceApi.provinceRequestApi();
        emit(GetProvinceLoaded(getProvinceModel: dataProvince));
        _Prefs.setProvinceModel(provinceModel: json.encode(dataProvince));
        print("Holyholy ${dataProvince.toString()}");
      } catch (e) {
        emit(GetProvinceError(error: "Error Data"));
      }
    });
  }
}
