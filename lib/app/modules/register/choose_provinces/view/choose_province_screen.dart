import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/register/choose_provinces/bloc/get_province_bloc.dart';
import 'package:rupp_application/app/modules/register/choose_provinces/controller/province_data_model.dart';
import 'package:rupp_application/app/widgets/button/button_widget.dart';
import 'package:rupp_application/storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';

class ChooseProvinceScreen extends StatefulWidget {
  final String typeRout;
  const ChooseProvinceScreen({Key? key, this.typeRout = "normal"}) : super(key: key);

  @override
  State<ChooseProvinceScreen> createState() => _ChooseProvinceScreenState();
}

class _ChooseProvinceScreenState extends State<ChooseProvinceScreen> {
  UserSecureStroage _prefs = UserSecureStroage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 12, bottom: 5, left: 25, right: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      child: SvgPicture.asset(
                        ImageAssets.arrow_back_icon,
                        width: 22,
                        height: 22,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Expanded(
                    child: Container(
                      child: Text(
                        "ការចុះឈ្មោះ",
                        style: ThemeConstant.texttheme.headline6!.copyWith(
                            color: ColorContant.ducktextColor,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: SvgPicture.asset(
                      ImageAssets.arrow_back_icon,
                      width: 22,
                      height: 22,
                      color: Colors.transparent,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 35),
              child: Text(
                "រាជធានីឬខេត្តកំណើត",
                style: ThemeConstant.texttheme.headline6!.copyWith(
                    color: ColorContant.ducktextColor,
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              child: Text(
                "តើអ្នកកើតនៅរាជធានីឬខេត្តណា?",
                style: ThemeConstant.texttheme.subtitle1!.copyWith(
                    color: ColorContant.ducktextColor,
                    fontWeight: FontWeight.w400),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            BlocBuilder<GetProvinceBloc, GetProvinceState>(
              builder: (context, state) {
                if(state is GetProvinceLoading){
                  return const Expanded(child: Center(child: CircularProgressIndicator(),));
                }
                else if(state is GetProvinceLoaded){
                  var dataprovince = state.getProvinceModel.data!;
                  return Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 45),
                      child: Container(
                        padding: const EdgeInsets.only(top: 25, bottom: 35),
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: const ScrollPhysics(),
                          itemCount: dataprovince.length,
                          itemBuilder: (context, index) {
                            return Container(
                              padding: const EdgeInsets.only(bottom: 14),
                              child: ButtonWidget(
                                onTap: () {
                                  _prefs.setIndexProvince(indexProvince: index.toString());
                                  if(widget.typeRout=="edit"){
                                    Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                  }
                                  else{
                                    Navigator.pushNamed(context, Routes.INPUTHIGHTSCHOOLSCREEN);
                                  }
                                  
                                },
                                title: dataprovince[index].name.toString(),
                                buttonColor: ColorContant.primaryColor,
                                textStyleButton:
                                    ThemeConstant.texttheme.headline6!.copyWith(
                                        color: ColorContant.whiteColor,
                                        fontWeight: FontWeight.w700),
                                panddinHorButton: 16,
                                panddingVerButton: 8,
                                radiusButton: 12,
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                );
                }
                else{
                  return const Center(child: Text("Error"),);
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
