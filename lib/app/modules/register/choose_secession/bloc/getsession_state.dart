part of 'getsession_bloc.dart';

@immutable
abstract class GetsessionState {}

class GetsessionInitial extends GetsessionState {}

class GetSessionLoading extends GetsessionState{
  final String message;
  GetSessionLoading({required this.message});
}

class GetSessionLoaded extends GetsessionState{
  final SessionModel getSessionModel;
  GetSessionLoaded({required this.getSessionModel,});
}

class GetSessionError extends GetsessionState{
  final String error;
  GetSessionError({required this.error});
}
