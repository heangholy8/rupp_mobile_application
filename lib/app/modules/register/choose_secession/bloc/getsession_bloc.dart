import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:rupp_application/services/apis/get_session/get_session.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../models/get_session/get_session_model.dart';
part 'getsession_event.dart';
part 'getsession_state.dart';

class GetsessionBloc extends Bloc<GetsessionEvent, GetsessionState> {
  final  GetSession getSession;
  final UserSecureStroage _Prefs = UserSecureStroage();
  GetsessionBloc({required this.getSession}) : super(GetsessionInitial()) {
    on<GetAllSessionEvent>((event, emit) async {
      emit(GetSessionLoading(message: "Loading....."));
      try {
        var dataSession = await getSession.sessionRequestApi();
        _Prefs.setSessionModel(sessionModel: json.encode(dataSession));
        emit(GetSessionLoaded(getSessionModel: dataSession));
        print("Holyholy ${dataSession.toString()}");
      } catch (e) {
        emit(GetSessionError(error: "Error Data"));
      }
    });
  }
}
