part of 'getsession_bloc.dart';

@immutable
abstract class GetsessionEvent {}

class GetAllSessionEvent extends GetsessionEvent{}
