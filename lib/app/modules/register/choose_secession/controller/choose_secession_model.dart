class SecessionData{
  final String? secessionname;
  final String? secessionnum;
  final String? secessionstatus;
  final bool? disable;
  SecessionData({this.disable,this.secessionname,this.secessionnum,this.secessionstatus});

  static List<SecessionData> generate(){
    return [
      SecessionData(disable: true,secessionname: "វេនព្រឹក",secessionnum: "១",secessionstatus: "ពេញ"),
      SecessionData(disable: false,secessionname: "វេនរសៀល",secessionnum: "២",secessionstatus: ""),
      SecessionData(disable: false,secessionname: "វេនល្ងាច",secessionnum: "៣",secessionstatus: "")
    ];
  }
}