import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/storages/user_storage.dart';

import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';

class ChoosePriceScreen extends StatefulWidget {
  const ChoosePriceScreen({Key? key}) : super(key: key);

  @override
  State<ChoosePriceScreen> createState() => _ChoosePriceScreenState();
}

class _ChoosePriceScreenState extends State<ChoosePriceScreen> {
  int? priceyear;
  int? pricesemester;
  String? majorName;
  String? fristName;
  String? token;
  UserSecureStroage _prefs = UserSecureStroage();
  void getmajorstoreage() async{
    var majorModel = await _prefs.getMajorModel;
    var indexFaculty = await _prefs.getIndexFaculty();
    var indexmajor = await _prefs.getIndexMajors();
    var accessToken = await _prefs.getLoginSucess();
    var firstname = await _prefs.getFristName();
    setState(() {
      majorName = majorModel.data![indexFaculty!.toInt()].majors![indexmajor!.toInt()].name.toString();
      priceyear = majorModel.data![indexFaculty.toInt()].majors![indexmajor.toInt()].schoolFee!.domesticAmount;
      pricesemester = priceyear! ~/2;
      token = accessToken.toString();
      fristName = firstname.toString();
    });
  }
  @override
  void initState() {
    setState(() {
      getmajorstoreage();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("ជំហានទី៣",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 35),
                child: Text("ជ្រើសរើសការបង់ប្រាក់",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 10,vertical: 16),
                          width: MediaQuery.of(context).size.width,
                          padding:const EdgeInsets.symmetric(vertical: 16,horizontal: 22),
                          decoration: BoxDecoration(
                            border: Border.all(width: 2,color: ColorContant.primaryColor),
                            borderRadius:const BorderRadius.all(Radius.circular(12))
                          ),
                          child: Column(
                            children: [
                              Container(
                                child: Text("អ្នកបានជ្រើសរើសដេប៉ាតេម៉ង់",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                              ),
                              Container(
                                child: Text(majorName.toString(),style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin:const EdgeInsets.only(top: 12.0,left: 35,right: 35),
                          child: Text("តើអ្នកចង់បង់ប្រាក់យ៉ាងដូចម្តេច?",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        const SizedBox(height: 8,),
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 10),
                          width: MediaQuery.of(context).size.width,
                          child: MaterialButton(
                            onPressed: (){
                              _prefs.setPayType(payType: "1");
                              _prefs.setPriceSelected(priceSelected: pricesemester.toString());
                                 Navigator.pushNamed(context, Routes.SUMMARYMAJORSCREEN);
                            },
                            color:ColorContant.primaryColor,
                                  shape:const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                  padding:const EdgeInsets.only(left: 34,right: 18,top: 20,bottom: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: Text("បង់ជាឆមាស",style: ThemeConstant.texttheme.headline5!.copyWith(color:ColorContant.whiteColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                                ),
                                Container(
                                  child: Text("\$"+pricesemester.toString(),style: ThemeConstant.texttheme.headline1!.copyWith(color:ColorContant.whiteColor,fontWeight: FontWeight.w700,height: 1.3),textAlign: TextAlign.center,),
                                ),
                                
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 12,),
                        Container(
                          margin:const EdgeInsets.symmetric(horizontal: 10),
                          width: MediaQuery.of(context).size.width,
                          child: MaterialButton(
                            onPressed: (){
                              _prefs.setPayType(payType: "2");
                              _prefs.setPriceSelected(priceSelected: priceyear.toString());
                              Navigator.pushNamed(context, Routes.SUMMARYMAJORSCREEN);
                            },
                            color:ColorContant.primaryColor,
                                  shape:const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                  padding:const EdgeInsets.only(left: 34,right: 18,top: 20,bottom: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: Text("បង់ជាឆ្នាំ",style: ThemeConstant.texttheme.headline5!.copyWith(color:ColorContant.whiteColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                                ),
                                Container(
                                  child: Text("\$"+priceyear.toString(),style: ThemeConstant.texttheme.headline1!.copyWith(color:ColorContant.whiteColor,fontWeight: FontWeight.w700,height: 1.3),textAlign: TextAlign.center,),
                                ),
                                
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}