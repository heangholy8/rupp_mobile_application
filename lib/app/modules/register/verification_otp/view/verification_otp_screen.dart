import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pinput/pinput.dart';

import '../../../../core/constands/constands_color.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';

class VerificationScren extends StatefulWidget {
  const VerificationScren({Key? key}) : super(key: key);

  @override
  State<VerificationScren> createState() => _VerificationScrenState();
}

class _VerificationScrenState extends State<VerificationScren> {
 FocusNode focusNode =  FocusNode();

 @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }
  @override
  
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
  final defaultPinTheme = PinTheme(
      margin: const EdgeInsets.symmetric(horizontal: 0),
      width: width - 120,
      height: 50,
      textStyle: const TextStyle(
        fontSize: 20,
        color: ColorContant.ducktextColor,
        fontWeight: FontWeight.w400,
      ),
      decoration: BoxDecoration(
        border: Border.all(
          color: ColorContant.primaryColor,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
    );
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("ការចុះឈ្មោះ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.only(top: 60,left: 35,right: 35,bottom: 00),
                          child: Text("លេខកូដ៦ខ្ទង់",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        
                        const SizedBox(height: 8,),
                        Pinput(
                          length: 6,
                          preFilledWidget: const Icon(
                            Icons.circle,
                            size: 12.0,
                            color: ColorContant.primaryColor,
                          ),
                          onChanged: (value) {
                          },
                          onCompleted: (value) {
                            Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                          },
                          autofocus: true,
                          focusNode: focusNode,
                          defaultPinTheme: defaultPinTheme.copyDecorationWith(),
                          focusedPinTheme: defaultPinTheme.copyDecorationWith(
                            border: Border.all(
                                color: ColorContant.primaryColor),
                          ),
                        ),
                        const SizedBox(height: 8,),
                        Container(
                          margin:const EdgeInsets.only(top: 0,left: 35,right: 35,bottom: 30),
                          child: Text("សូមឆែកមើលសារក្នុងលេខទូរស័ព្ទដែលបានដាក់បញ្ជូល សម្រាប់លេខកូដ៦ខ្ទង់?",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.duckDisableColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}