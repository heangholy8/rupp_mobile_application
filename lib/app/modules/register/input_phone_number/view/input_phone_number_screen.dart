import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/register/choose_secession/controller/choose_secession_model.dart';
import '../../../../../storages/user_storage.dart';
import '../../../../core/resources/assets_resources.dart';
import '../../../../core/thems/thems_constands.dart';
import '../../../../routes/app_routes.dart';
import '../../../../widgets/button/button_widget.dart';
import '../../../../widgets/textfromfiled_custom.dart';

class InputPhoneNumberScreen extends StatefulWidget {
  const InputPhoneNumberScreen({Key? key}) : super(key: key);

  @override
  State<InputPhoneNumberScreen> createState() => _InputPhoneNumberScreenState();
}

class _InputPhoneNumberScreenState extends State<InputPhoneNumberScreen> {
  final datasecession = SecessionData.generate();
  UserSecureStroage _prefs = UserSecureStroage();
  FocusNode focusNode = FocusNode();
  TextEditingController phonenumber = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
                margin:const EdgeInsets.only(top: 12,bottom: 5,left: 25,right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:const EdgeInsets.all(10),
                        child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Expanded(
                      child: Container(
                        child: Text("ការចុះឈ្មោះ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    const SizedBox(width: 2,),
                    Container(
                      padding:const EdgeInsets.all(10),
                      child: SvgPicture.asset(ImageAssets.arrow_back_icon,width: 22,height: 22,color: Colors.transparent,),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 6,),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin:const EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: [
                        Container(
                          margin:const EdgeInsets.only(top: 60,left: 35,right: 35,bottom: 30),
                          child: Text("លេខទូរស័ព្ទ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                        ),
                        const SizedBox(height: 8,),
                        Container(
                          child: TextFromFiledWidget(
                            focusNode: focusNode,
                            controller: phonenumber,
                            keytype: TextInputType.phone,
                            onChange: (value) {
                             setState(() {
                               phonenumber.text;
                             });
                            },
                            
                          ),
                          
                        ),
                        Container(
                          margin:const EdgeInsets.symmetric(vertical: 50),
                          child: ButtonWidget(
                            buttonColor:phonenumber.text!=""?ColorContant.primaryColor:ColorContant.duckDisableColor,
                            textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                            weightButton: 140,
                            panddingVerButton: 5,
                            panddinHorButton: 16,
                            onTap:phonenumber.text==""?null:(){
                              //FocusManager.instance.primaryFocus?.unfocus();
                              _prefs.setPhoneNumber(phoneNumber: phonenumber.text);
                              Navigator.pushNamed(context, Routes.VERIFICATIONSCREEN);
                            },
                            title: "បន្ទាប់",
                            radiusButton: 10,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
              
          ],
        ),
      ),
    );
  }
}