import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/core/thems/thems_constands.dart';
import 'package:rupp_application/app/widgets/no_connection_alert_widget.dart';
import 'package:rupp_application/storages/user_storage.dart';
import '../../../core/resources/assets_resources.dart';
import '../../../routes/app_routes.dart';
import '../../../widgets/button/button_icon_widget.dart';
import '../../../widgets/button/button_widget.dart';
import '../../../widgets/toast_nointernet.dart';

class RegisterAndLoginOptionScreen extends StatefulWidget {
  const RegisterAndLoginOptionScreen({Key? key}) : super(key: key);

  @override
  State<RegisterAndLoginOptionScreen> createState() => _RegisterAndLoginOptionScreenState();
}

class _RegisterAndLoginOptionScreenState extends State<RegisterAndLoginOptionScreen> {
  UserSecureStroage _prefs = UserSecureStroage();
  String? step;
  StreamSubscription? sub;
  bool connection = true;
  void getLogcal() async{
    var steplogcal = await _prefs.getStep();
    setState(() {
      step = steplogcal.toString();
    });
  }
  @override
  void initState() {
    //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection == true){
          setState(() {
          });
        }
        else{
          setState(() {
            connection = false;
          });
        }
      });
    });
   
   //=============Check internet====================
    setState((){
      getLogcal();
    });
    super.initState();
  }
  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: WillPopScope(
          onWillPop:() => exit(0),
          child: SizedBox(
            child: Column(
              children: [
                connection==false?const NoConnectWidget():Container(),
                Container(height: 25,),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      child: Column(
                        children: [
                          Hero(
                            tag: "taghero",
                            child: Container(
                              margin:const EdgeInsets.only(top: 19.0,bottom: 8.0),
                              alignment: Alignment.center,
                              child:Image.asset("assets/logo/LOGO (1).png",width: 140.85,height: 140.85,)
                            ),
                          ),
                          Container(
                            child: Text("សាកលវិទ្យាល័យភូមិន្ទភ្នំពេញ",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w700),),
                          ),
                          Container(
                            margin:const EdgeInsets.only(top: 34.0,),
                            child: Text("សូមស្វាគមន៏",style: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.primaryColor,fontWeight: FontWeight.w700),),
                          ),
                          Container(
                            margin:const EdgeInsets.only(top: 26.0,),
                            child: Text("សម្រាប់និស្សិតថ្មី",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),),
                          ),
                          Container(
                            margin:const EdgeInsets.only(left:48,right: 48,top: 18,bottom: 24),
                            child: step==""? ButtonWidget(
                              buttonColor: ColorContant.primaryColor,
                              textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                              weightButton: MediaQuery.of(context).size.width,
                              panddingVerButton: 10,
                              panddinHorButton: 16,
                              onTap: connection==false?(){showtoastInternet(context);}:(){
                                Navigator.pushNamed(context, Routes.CHOOSEDEPARTMENTSCREEN);
                              },
                              title: "ចាប់ផ្តើម",
                              radiusButton: 10,
                            )
                            :ButtonIconWidget(
                              buttonColor: ColorContant.primaryColor,
                              textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                              weightButton: MediaQuery.of(context).size.width,
                              panddingVerButton: 10,
                              panddinHorButton: 16,
                              onTap: (){
                               if(step=="1"){
                                  Navigator.pushNamed(context, Routes.SUMMARYMAJORSCREEN);
                                }
                                if(step=="2"){
                                  Navigator.pushNamed(context, Routes.SUMMARYGREADSCREEN);
                                } 
                                if(step=="3"){
                                  Navigator.pushNamedAndRemoveUntil(context, Routes.FINALSUMMARYSCREEN,(Route<dynamic> route) => false);
                                }
                              },
                              title: "បន្តចុះឈ្មោះ",
                              radiusButton: 10,
                            ),
                          ),
                          Container(
                            margin:const EdgeInsets.only(left: 31,right: 31,bottom: 5,top: 15),
                            child: Container(
                              child: Stack(
                                alignment: AlignmentDirectional.center,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    child:const Divider(height: 1,color: ColorContant.ducktextColor,thickness: 0.6,),
                                  ),
                                  Container(
                                    color: ColorContant.whiteColor,
                                    alignment: Alignment.center,
                                    margin:const EdgeInsets.only(left: 40,right: 40),
                                    child: Text("សម្រាប់និសិត្យដែលមានគណនីរួចហើយ ឬនិស្សិតអាហារូបករណ៌",style: ThemeConstant.texttheme.subtitle1!.copyWith(color: ColorContant.ducktextColor,fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            margin:const EdgeInsets.symmetric(horizontal:48,),
                            child: ButtonWidget(
                              buttonColor: ColorContant.primaryColor,
                              textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(color: ColorContant.whiteColor,fontWeight: FontWeight.w700),
                              weightButton: MediaQuery.of(context).size.width,
                              panddingVerButton: 10,
                              panddinHorButton: 16,
                              onTap:connection==false?(){showtoastInternet(context);}: (){
                                Navigator.pushNamed(context, Routes.LOGNSCREEN);
                              },
                              title: "ចូលប្រើ",
                              radiusButton: 10,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}