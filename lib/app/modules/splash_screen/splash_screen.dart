import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/register_and_login/views/registration_and_login_screen.dart';
import 'package:rupp_application/app/modules/welcome_screen/view/welcome_screen.dart';
import 'package:rupp_application/app/widgets/no_connection_alert_widget.dart';
import 'package:rupp_application/storages/user_storage.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String loginsucess = "";
  StreamSubscription? sub;
  bool connection = false;
  void getAuthModel() async {
    UserSecureStroage _pref = UserSecureStroage();
    var successlogin = await _pref.getLoginSucess();
    setState(() {
      loginsucess = successlogin.toString();
    });
  }
  @override
  void initState() {
    getAuthModel();
    //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection == true){
          setState(() {
            Future.delayed(const Duration(milliseconds: 700), () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => loginsucess!=""?const WellcomeScreen():const RegisterAndLoginOptionScreen(
                        ),
                      ),
                    );
                  });
          });
        }
        else{
          setState(() {
            connection = false;
          });
        }
      });
    });
   
   //=============Check internet====================
    super.initState();
  }
  @override
  void dispose() {
    sub?.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: WillPopScope(
          onWillPop: (() async{
            return exit(0);
          }),
          child: Stack(
            children: [
              connection==false?const Positioned(
                top: 0,left: 0,right: 0,
                child:NoConnectWidget(),
              ):Container(),
              Center(
                child: Hero(
                  tag: "taghero",
                  child: Container(
                    child:Image.asset("assets/logo/LOGO (1).png",width: 140.85,height: 140.85,)
                  ),
                ),
              ),
              
              Positioned(
                bottom: 55,left: 0,right: 0,
                child: Center(
                  child: Container(width: 30,height: 30,child:const CircularProgressIndicator()),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}