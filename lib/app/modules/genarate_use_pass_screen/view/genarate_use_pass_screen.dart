import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rupp_application/app/modules/invoice_screen/view/invoice_screen.dart';
import 'package:rupp_application/storages/user_storage.dart';
import 'package:screenshot/screenshot.dart';
import '../../../core/constands/constands_color.dart';
import '../../../core/resources/assets_resources.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../widgets/button/button_widget.dart';
import '../../../widgets/no_connection_alert_widget.dart';
import '../../../widgets/toast_nointernet.dart';

class GenarateUserPassScreen extends StatefulWidget {
  const GenarateUserPassScreen({Key? key}) : super(key: key);

  @override
  State<GenarateUserPassScreen> createState() => _GenarateUserPassScreenState();
}

class _GenarateUserPassScreenState extends State<GenarateUserPassScreen> {
  UserSecureStroage _prefs = UserSecureStroage();
  String?userId;
  String?userPassword;
  bool connection= true;
  StreamSubscription? sub;

  void getlogcaldata()async{
    var userid = await _prefs.getUserNameLogin();
    var userpassword = await _prefs.getPaeewordLogin();
    setState(() {
      userId = userid.toString();
      userPassword = userpassword.toString();
    });
  }

  final screenShotColler = ScreenshotController();

  bool isWaiting = false;

  Future<String> saveImageToLocal(
    Uint8List btye,
  ) async {
    await [Permission.storage].request();
    // final result = await ImageGallerySaver.saveImage(image);

    // print("File Path: ${result}");
    // return result['filePath']!;
    final directory = await getApplicationDocumentsDirectory();
    final image = File('${directory.path}/${DateTime.now().toString()}.png');
    image.writeAsBytesSync(btye);
    GallerySaver.saveImage(image.path, albumName: "RUPP");
    return image.path;
  }

  @override
  void initState() {
    setState(() {
      //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection == true){
          setState(() {
          });
        }
        else{
          setState(() {
            connection = false;
          });
        }
      });
    });
   
   //=============Check internet====================
      getlogcaldata();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: SizedBox(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin: const EdgeInsets.only(top: 25),
                    child: Column(
                      children: [
                        connection ==false?const NoConnectWidget(): Container(),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(
                              top: 34.0, left: 30, right: 30),
                          child: Text(
                            "អបរអរសាទ",
                            style: ThemeConstant.texttheme.headline2!.copyWith(
                                color: ColorContant.primaryColor,
                                fontWeight: FontWeight.w700),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 48, right: 48, top: 40, bottom: 20),
                          child: Text(
                            "ការចុះឈ្មោះនិងជ្រើសរើសជំនាញត្រូវបានបញ្ចប់",
                            style: ThemeConstant.texttheme.headline6!.copyWith(
                                color: ColorContant.ducktextColor,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                            left: 28,
                            right: 28,
                          ),
                          child: const Divider(
                            color: ColorContant.duckDisableColor,
                          ),
                        ),
                        GenerateUserPaswwordBodyWidget(
                          userID: userId.toString(),
                          userPassword: userPassword.toString(),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(
                              horizontal: 48, vertical: 20),
                          child: ButtonWidget(
                            buttonColor: ColorContant.primaryColor,
                            textStyleButton: ThemeConstant.texttheme.subtitle1!
                                .copyWith(
                                    color: ColorContant.whiteColor,
                                    fontWeight: FontWeight.w700),
                                weightButton: 100,
                                 panddingVerButton: 8,
                                 panddinHorButton: 16,
                                 onTap: isWaiting
                                ? null
                                : () async {
                                    try {
                                      setState(() {
                                        isWaiting = true;
                                      });
                                      final image = await screenShotColler
                                          .captureFromWidget(Container(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                2,
                                        color: ColorContant.whiteColor,
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                40,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                40,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                40),
                                        child:
                                             GenerateUserPaswwordBodyWidget(
                                          userID: userId.toString(),
                                           userPassword: userPassword.toString(),
                                        ),
                                      ));
                                      setState(() {
                                        isWaiting = false;
                                      });
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              AlertDialog(
                                                scrollable: true,
                                                content: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Image.memory(
                                                      image,
                                                      fit: BoxFit.contain,
                                                    ),
                                                    InkWell(
                                                      onTap: () async {
                                                        try {
                                                          await saveImageToLocal(
                                                              image);
                                                          ScaffoldMessenger.of(
                                                                  context)
                                                              .showSnackBar(
                                                                  const SnackBar(
                                                                      content: Text(
                                                                          "Image saved successfully")));
                                                          Navigator.of(context)
                                                              .pop();
                                                        } catch (e) {
                                                          Navigator.of(context)
                                                              .pop();
                                                          ScaffoldMessenger.of(
                                                                  context)
                                                              .showSnackBar(
                                                                  const SnackBar(
                                                                      content: Text(
                                                                          "Image saved successfully")));
                                                        }
                                                      },
                                                      child: Container(
                                                        margin: const EdgeInsets
                                                                .symmetric(
                                                            vertical: 5),
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(8),
                                                              decoration: BoxDecoration(
                                                                  color: ColorContant
                                                                      .duckGreytextColor
                                                                      .withOpacity(
                                                                          0.2),
                                                                  borderRadius: const BorderRadius
                                                                          .all(
                                                                      Radius.circular(
                                                                          20))),
                                                              child: const Icon(
                                                                Icons
                                                                    .save_alt_rounded,
                                                                color: ColorContant
                                                                    .secondaryColor,
                                                                size: 25,
                                                              ),
                                                            ),
                                                            const SizedBox(
                                                              width: 10,
                                                            ),
                                                            Expanded(
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Text(
                                                                    "ទាញយករូបភាព",
                                                                    style: ThemeConstant
                                                                        .texttheme
                                                                        .caption
                                                                        ?.copyWith(
                                                                            color:
                                                                                ColorContant.secondaryColor,
                                                                            fontWeight: FontWeight.bold),
                                                                  ),
                                                                  Text(
                                                                    "ចាប់យក និងទាញរូបភាពព័ត៌មានលម្អិតអំពីប្រតិបត្តិការណ៍របស់អ្នក",
                                                                    style: ThemeConstant
                                                                        .texttheme
                                                                        .bodyText2
                                                                        ?.copyWith(
                                                                            color:
                                                                                ColorContant.ducktextColor.withOpacity(0.5),
                                                                            fontSize: 9),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ));
                                    } catch (e) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(const SnackBar(
                                              content: Text(
                                                  "Image saved successfully")));
                                    }
                                  },
                            title: "ថតទុក",
                            radiusButton: 10,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                    left: 48, right: 48, top: 38, bottom: 24),
                child: ButtonWidget(
                  buttonColor: ColorContant.primaryColor,
                  textStyleButton: ThemeConstant.texttheme.headline6!.copyWith(
                      color: ColorContant.whiteColor,
                      fontWeight: FontWeight.w700),
                  weightButton: MediaQuery.of(context).size.width,
                  panddingVerButton: 10,
                  panddinHorButton: 16,
                  onTap: connection==false?(){showtoastInternet(context);}: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => InvoiceScreen(
                                  routType: 1,
                                )),
                        (Route<dynamic> route) => false);
                  },
                  title: "មើលវិក្កយបត្រ",
                  radiusButton: 10,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class GenerateUserPaswwordBodyWidget extends StatelessWidget {
  final String userID, userPassword;
  const GenerateUserPaswwordBodyWidget({
    Key? key,
    required this.userID,
    required this.userPassword,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin:
              const EdgeInsets.only(left: 48, right: 48, top: 20, bottom: 10),
          child: Text(
            "នេះជាគណនីរបស់អ្នកសម្រាប់ការចូលប្រើប្រាស់ប្រព័ន្ធឡើងវិញ",
            style: ThemeConstant.texttheme.headline6!.copyWith(
                color: ColorContant.ducktextColor, fontWeight: FontWeight.w400),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 28, right: 28, top: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.only(),
                child: Text(
                  "លេខកូដ",
                  style: ThemeConstant.texttheme.headline6!.copyWith(
                      color: ColorContant.primaryColor,
                      fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              Container(
                padding: const EdgeInsets.only(
                    top: 6, bottom: 1, left: 25, right: 25),
                decoration: BoxDecoration(
                    border: Border.all(color: ColorContant.primaryColor),
                    borderRadius: const BorderRadius.all(Radius.circular(10))),
                child: Text(
                  userID,
                  style: ThemeConstant.texttheme.headline5!.copyWith(
                      color: ColorContant.primaryColor,
                      height: 1.4,
                      fontWeight: FontWeight.w700),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin:
              const EdgeInsets.only(left: 28, right: 28, top: 10, bottom: 25),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(),
                child: Text(
                  "ពាក្យសម្ងាត់",
                  style: ThemeConstant.texttheme.headline6!.copyWith(
                      color: ColorContant.primaryColor,
                      fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      color: ColorContant.primaryColor,
                      border: Border.all(color: ColorContant.primaryColor),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10))),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.only(top: 6, bottom: 3),
                          decoration: const BoxDecoration(
                              color: ColorContant.whiteColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  bottomLeft: Radius.circular(10))),
                          child: Text(
                            userPassword,
                            style: ThemeConstant.texttheme.headline5!.copyWith(
                                color: ColorContant.primaryColor,
                                height: 1.4,
                                fontWeight: FontWeight.w700),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Clipboard.setData(
                               ClipboardData(text: userPassword.toString()));
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            duration: const Duration(milliseconds: 500),
                            width: 100,
                            elevation: 2.0,
                            behavior: SnackBarBehavior.floating,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            content: Wrap(
                              children: [
                                Container(
                                  child: Center(
                                    child: Text(
                                      'Copied',
                                      style: ThemeConstant.texttheme.subtitle1!
                                          .copyWith(
                                              color: ColorContant.whiteColor),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ));
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 12),
                          child: SvgPicture.asset(
                            ImageAssets.copy_icon,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
            left: 48,
            right: 48,
          ),
          child: Text(
            "សូមកត់ទុកអោយបានល្អនិងសុវត្ថិភាព",
            style: ThemeConstant.texttheme.subtitle1!.copyWith(
                color: ColorContant.duckDisableColor,
                fontWeight: FontWeight.w400),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
