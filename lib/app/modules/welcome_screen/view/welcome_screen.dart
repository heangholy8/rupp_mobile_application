import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/core/thems/thems_constands.dart';
import 'package:rupp_application/app/widgets/no_connection_alert_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../routes/app_routes.dart';
import '../../../widgets/button/button_widget.dart';
import '../../../widgets/toast_nointernet.dart';
import '../../invoice_screen/view/invoice_screen.dart';
import '../bloc/user_info_bloc.dart';

class WellcomeScreen extends StatefulWidget {
  const WellcomeScreen({Key? key}) : super(key: key);

  @override
  State<WellcomeScreen> createState() => _WellcomeScreenState();
}

class _WellcomeScreenState extends State<WellcomeScreen> {
  StreamSubscription? sub;
  bool connection = true;
  void getData(){
     BlocProvider.of<UserInfoBloc>(context).add(UserInformationEvant());
  }
  @override
  void initState() {
      //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection == true){
          setState(() {
          });
        }
        else{
          setState(() {
            connection = false;
          });
        }
      });
    });
   
   //=============Check internet====================
   getData();
    super.initState();
  }
  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: WillPopScope(
          onWillPop: () => exit(0),
          child:BlocBuilder<UserInfoBloc, UserInfoState>(
            builder: (context, state) {
              if(state is GetUserInfoLoading){
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              else if(state is GetUserInfoLoaded){
                var data = state.getUserInfoModel.data!;
                return SizedBox(
                child: Column(
                  children: [
                    connection==false?const NoConnectWidget():Container(),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                  margin: const EdgeInsets.only(
                                      top: 19.0, bottom: 8.0),
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                    "assets/logo/LOGO (1).png",
                                    width: 140.85,
                                    height: 140.85,
                                  )),
                              Container(
                                child: Text(
                                  "សាកលវិទ្យាល័យភូមិន្ទភ្នំពេញ",
                                  style: ThemeConstant.texttheme.headline6!
                                      .copyWith(
                                          color: ColorContant.ducktextColor,
                                          fontWeight: FontWeight.w700),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(
                                    top: 34.0, left: 30, right: 30),
                                child: Text(
                                  "សូមស្វាគមន៏",
                                  style: ThemeConstant.texttheme.headline2!
                                      .copyWith(
                                          color: ColorContant.primaryColor,
                                          fontWeight: FontWeight.w700),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  top: 20.0,
                                ),
                                child: Text(
                                  data.fullname.toString(),
                                  style: ThemeConstant.texttheme.headline6!
                                      .copyWith(
                                          color: ColorContant.ducktextColor,
                                          fontWeight: FontWeight.w700),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                    left: 48, right: 48, top: 38, bottom: 24),
                                child: ButtonWidget(
                                  buttonColor: ColorContant.primaryColor,
                                  textStyleButton: ThemeConstant
                                      .texttheme.headline6!
                                      .copyWith(
                                          color: ColorContant.whiteColor,
                                          fontWeight: FontWeight.w700),
                                  weightButton:
                                      MediaQuery.of(context).size.width,
                                  panddingVerButton: 10,
                                  panddinHorButton: 16,
                                  onTap:connection==false?(){showtoastInternet(context);}:() async {
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) =>
                                      InvoiceScreen(routType: 2,)));
                                  },
                                  title: "មើលវិក្កយបត្រ",
                                  radiusButton: 10,
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 48, vertical: 50),
                                child: ButtonWidget(
                                  buttonColor: ColorContant.primaryColor,
                                  textStyleButton: ThemeConstant
                                      .texttheme.subtitle1!
                                      .copyWith(
                                          color: ColorContant.whiteColor,
                                          fontWeight: FontWeight.w700),
                                  weightButton: 100,
                                  panddingVerButton: 6,
                                  panddinHorButton: 16,
                                  onTap: () async {
                                    SharedPreferences _pref =
                                        await SharedPreferences.getInstance();
                                    _pref.remove("loginsucess");
                                    _pref.remove("studentid");
                                    Navigator.pushNamed(context,
                                        Routes.OPTIONREGISANDLOGINSCREEN);
                                  },
                                  title: "ចាកចេញ",
                                  radiusButton: 10,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
              }
              else {
                return connection==false? Container(child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child:const NoConnectWidget(),
                    ),
                    Container(
                      child: Image.asset("assets/gifs/no_connection.gif"),
                    ),
                    Container(
                      height: 50,
                    ),
                  ],
                )): Center(child: const Text("Error"));
              }
            },
          ),
        ),
      ),
    );
  }
}
