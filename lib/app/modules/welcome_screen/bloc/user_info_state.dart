part of 'user_info_bloc.dart';

@immutable
abstract class UserInfoState {}

class UserInfoInitial extends UserInfoState {}


class GetUserInfoLoading extends UserInfoState{
  final String message;
  GetUserInfoLoading({required this.message});
}

class GetUserInfoLoaded extends UserInfoState{
  final UserInforModel getUserInfoModel;
  GetUserInfoLoaded({required this.getUserInfoModel });
}

class GetUserInfoError extends UserInfoState{
  final String error;
  GetUserInfoError({required this.error});
}