import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:rupp_application/app/models/get_user_info_model/get_user_infor_model.dart';
import '../../../../services/apis/get_user_profile/get_user_profile.dart';
part 'user_info_event.dart';
part 'user_info_state.dart';

class UserInfoBloc extends Bloc<UserInfoEvent, UserInfoState> {
  final GetUserInfoApi getUserInfoApi;
  UserInfoBloc({required this.getUserInfoApi}) : super(UserInfoInitial()) {
    on<UserInformationEvant>((event, emit) async{
      emit(GetUserInfoLoading(message: "Loading....."));
      try {
        var dataUserinfo = await getUserInfoApi.UserInfoRequestApi();
        emit(GetUserInfoLoaded(getUserInfoModel: dataUserinfo));
      } catch (e) {
        emit(GetUserInfoError(error: "Error Data"));
      }
    });
  }
}
