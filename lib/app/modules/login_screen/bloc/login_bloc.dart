// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rupp_application/app/models/login_model/login_model.dart';
import 'package:rupp_application/services/apis/login_api/login_api.dart';
import 'package:rupp_application/storages/user_storage.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginApi loginApi;

  LoginBloc({required this.loginApi}) : super(LoginInitial()) {
    UserSecureStroage _prefs = UserSecureStroage();
    on<PostLoginEvent>((event, emit) async {
      emit(LoginLoadingState());
      try {
        var data = await loginApi
            .loginApi(codeNumber: event.codeNum, passWord: event.passWd)
            .then((value) {
          if (value!.status == true) {
            _prefs.setLoginSucess(loginsucess: "${value.data?.userInformation?.accessToken.toString()}");
            _prefs.setStudentId(studentId: "${value.data?.studentId.toString()}");
            _prefs.setStep(step: "");
            emit(LoginSuccessState(login: value));
          } else {
            emit(LoginErrorState(error: "Login is Error "));
          }
        });
      } catch (e) {
        debugPrint("Throw Exception -> $e");
        emit(
          LoginErrorState(error: e.toString()),
        );
      }
    });
  }
}
