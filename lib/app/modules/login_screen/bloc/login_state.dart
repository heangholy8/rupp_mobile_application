part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginLoadingState extends LoginState {}

class LoginSuccessState extends LoginState {
  final LoginModel? login;
  LoginSuccessState({required this.login});
}

class LoginErrorState extends LoginState {
  final String error;
  LoginErrorState({required this.error});
}
