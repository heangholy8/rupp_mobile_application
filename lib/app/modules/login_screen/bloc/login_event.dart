part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class PostLoginEvent extends LoginEvent {
  final String codeNum;
  final String passWd;
  PostLoginEvent({required this.codeNum, required this.passWd});
}
