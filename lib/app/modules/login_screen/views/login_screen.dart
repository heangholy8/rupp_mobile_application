import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/core/thems/thems_constands.dart';
import 'package:rupp_application/app/modules/login_screen/bloc/login_bloc.dart';
import 'package:rupp_application/app/routes/app_routes.dart';
import 'package:rupp_application/app/widgets/no_connection_alert_widget.dart';
import 'package:rupp_application/storages/user_storage.dart';
import '../../../core/resources/assets_resources.dart';
import '../../../widgets/button/button_widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool connection= true;
  StreamSubscription? sub;
  TextEditingController usernamecontroller = TextEditingController();
  TextEditingController password = TextEditingController();
  FocusNode focusNode = FocusNode();
  final UserSecureStroage _prefs = UserSecureStroage();
  late bool isLoading;
  late bool isError;
  @override
  void initState() {
    super.initState();
     setState(() {
      //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
      });
    });
   
   //=============Check internet====================
    });
    isLoading = false;
    isError = false;
  }

  @override
  void dispose() {
    sub!.cancel();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: BlocListener<LoginBloc, LoginState>(
            listener: (context, state) {
              if (state is LoginLoadingState) {
                setState(() {
                  isLoading = true;
                  // isError = false;
                  print("Loading,$isLoading ");
                });
              }
              if (state is LoginErrorState) {
                setState(() {
                  isLoading = false;
                  isError = true;
                  print("Error,$isLoading ");
                });
              }
              if (state is LoginSuccessState) {
                Navigator.pushNamed(context, Routes.WELLCOMESCREEN);
                setState(() {
                  isLoading = false;
                  isError = false;
                });
              }
            },
            child: Column(
              children: [
                connection==false?const NoConnectWidget():Container(),
                Container(
                  height: 65,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          padding: const EdgeInsets.only(left: 38),
                          child: SvgPicture.asset(
                            ImageAssets.arrow_back_icon,
                            width: 22,
                            height: 22,
                          ),
                        ),
                      ),
                      
                    ],
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                            margin:
                                const EdgeInsets.only(top: 19.0, bottom: 8.0),
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/logo/LOGO (1).png",
                              width: 140.85,
                              height: 140.85,
                            )),
                        Text(
                          "សាកលវិទ្យាល័យភូមិន្ទភ្នំពេញ",
                          style: ThemeConstant.texttheme.headline6!.copyWith(
                              color: ColorContant.ducktextColor,
                              fontWeight: FontWeight.w700),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                            top: 34.0,
                          ),
                          child: Text(
                            "ចូលប្រើ",
                            style: ThemeConstant.texttheme.headline6!.copyWith(
                                color: ColorContant.primaryColor,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: const EdgeInsets.only(top: 26.0, left: 48),
                          child: Text(
                            "លេខកូដ",
                            style: ThemeConstant.texttheme.subtitle1!.copyWith(
                                color: isError != true
                                    ? ColorContant.ducktextColor
                                    : Colors.red,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(right: 48, left: 48),
                          child: TextFormField(
                            cursorColor: ColorContant.ducktextColor,
                            onChanged: ((value) {
                              setState(() {
                                isError = false;
                              usernamecontroller.text.trim();
                              });
                              
                            }),
                            controller: usernamecontroller,
                            textAlign: TextAlign.left,
                            style: ThemeConstant.texttheme.subtitle1!
                                .copyWith(color: ColorContant.ducktextColor),
                            keyboardType: TextInputType.text,
                            cursorHeight: 28.0,
                            decoration: InputDecoration(
                              focusColor:isError==false? ColorContant.ducktextColor:ColorContant.primaryColor,
                              enabledBorder: OutlineInputBorder(
                                borderRadius:const BorderRadius.all(
                                  Radius.circular(12.0),
                                ),
                                borderSide: BorderSide(
                                    color:isError==false? ColorContant.duckoptionfullColor:ColorContant.primaryColor),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:const BorderRadius.all(
                                  Radius.circular(12.0),
                                ),
                                borderSide: BorderSide(
                                    color:isError==false?ColorContant.ducktextColor: ColorContant.primaryColor),
                              ),
                              contentPadding:const EdgeInsets.only(
                                  top: 12.0, bottom: 12.0, left: 22),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: const EdgeInsets.only(top: 16.0, left: 48),
                          child: Text(
                            "ពាក្យសម្ងាត់",
                            style: ThemeConstant.texttheme.subtitle1!.copyWith(
                                color: isError != true
                                    ? ColorContant.ducktextColor
                                    : Colors.red,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(right: 48, left: 48),
                          child: TextFormField(
                            cursorColor:isError==false? ColorContant.ducktextColor:ColorContant.primaryColor,
                            onChanged: ((value) {
                              setState(() {
                                isError = false;
                                password.text.trim();
                              });
                            }),
                            controller: password,
                            textAlign: TextAlign.left,
                            cursorHeight: 28.0,
                            style: ThemeConstant.texttheme.subtitle1!
                                .copyWith(color: ColorContant.ducktextColor),
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              focusColor:isError==false? ColorContant.ducktextColor:ColorContant.primaryColor,
                              enabledBorder: OutlineInputBorder(
                                borderRadius:const BorderRadius.all(
                                  Radius.circular(12.0),
                                ),
                                borderSide: BorderSide(
                                    color:isError==false? ColorContant.duckoptionfullColor:ColorContant.primaryColor),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:const BorderRadius.all(
                                  Radius.circular(12.0),
                                ),
                                borderSide: BorderSide(
                                    color:isError==false?ColorContant.ducktextColor: ColorContant.primaryColor),
                              ),
                              contentPadding:const EdgeInsets.only(
                                  top: 12.0, bottom: 12.0, left: 22),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 60,
                        ),
                        !isLoading
                            ? Container(
                                margin: const EdgeInsets.symmetric(
                                  horizontal: 48,
                                ),
                                child: ButtonWidget(
                                  buttonColor: usernamecontroller.text == "" ||
                                          password.text == ""
                                      ? ColorContant.duckDisableColor
                                      : ColorContant.primaryColor,
                                  textStyleButton: ThemeConstant
                                      .texttheme.headline6!
                                      .copyWith(
                                          color: ColorContant.whiteColor,
                                          fontWeight: FontWeight.w700),
                                  weightButton:
                                      MediaQuery.of(context).size.width,
                                  panddingVerButton: 10,
                                  panddinHorButton: 16,
                                  onTap: usernamecontroller.text == "" ||
                                          password.text == ""|| connection==false
                                      ?null
                                      :() async {
                                          // if(usernamecontroller.text=="12345678" && password.text=="123456"){
                                          //   _prefs.setLoginSucess(loginsucess: "sucess_login");
                                          //   Navigator.pushNamed(context, Routes.WELLCOMESCREEN);
                                          // }
                                          BlocProvider.of<LoginBloc>(context)
                                              .add(
                                            PostLoginEvent(
                                              codeNum: usernamecontroller.text,
                                              passWd: password.text,
                                            ),
                                          );
                                        },
                                  title: "ចូលប្រើ",
                                  radiusButton: 10,
                                ),
                              )
                            : const CircularProgressIndicator(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
