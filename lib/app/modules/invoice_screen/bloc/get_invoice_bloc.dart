import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:rupp_application/app/models/invoice_model/invoice_model.dart';
import 'package:rupp_application/services/apis/get_invoice/get_invoice_api.dart';
import 'package:rupp_application/storages/user_storage.dart';
part 'get_invoice_event.dart';
part 'get_invoice_state.dart';

class GetInvoiceBloc extends Bloc<GetInvoiceEvent, GetInvoiceState> {
  final GetInvoice getInvoiceApi;
  GetInvoiceBloc({required this.getInvoiceApi}) : super(GetInvoiceInitial()) {
    UserSecureStroage _prefs = UserSecureStroage();
    on<InvoiceRequestEvent>((event, emit) async{
      emit(GetInvoiceLoading(message: "Loading....."));
      try {
        var dataInvoice = await getInvoiceApi.invoiceRequestApi();
        emit(GetInvoiceLoaded(getInvoiceModel: dataInvoice));
        print("Holyholy ${dataInvoice.toString()}");
        _prefs.setStep(step: "");
      } catch (e) {
        emit(GetInvoiceError(error: "Error Data"));
      }
    });
  }
}
