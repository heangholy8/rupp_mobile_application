part of 'get_invoice_bloc.dart';

@immutable
abstract class GetInvoiceEvent {}

class InvoiceRequestEvent extends GetInvoiceEvent{}
