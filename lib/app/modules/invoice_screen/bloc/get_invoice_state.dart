part of 'get_invoice_bloc.dart';

@immutable
abstract class GetInvoiceState {}

class GetInvoiceInitial extends GetInvoiceState {}

class GetInvoiceLoading extends GetInvoiceState{
  final String message;
  GetInvoiceLoading({required this.message});
}

class GetInvoiceLoaded extends GetInvoiceState{
  final InvoiceModel getInvoiceModel;
  GetInvoiceLoaded({required this.getInvoiceModel});
}

class GetInvoiceError extends GetInvoiceState{
  final String error;
  GetInvoiceError({required this.error});
}
