class InvoiceData{
  final bool? paid;
  final String? date;
  final String? majorname;
  final String? pricepay;
  const InvoiceData({this.paid, this.date, this.majorname, this.pricepay,});

  static List<InvoiceData> generate(){
    return [
      const InvoiceData(paid: false,date: "31/11/2022",majorname: "ភាសាអង់គ្លេស",pricepay: "\$510",),
      //const InvoiceData(paid: true,date: "28/09/2022",majorname: "ព័ត៌មានវិទ្យា",pricepay: "\$400",),
    ];
  }
}