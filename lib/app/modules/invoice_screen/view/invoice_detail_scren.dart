import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/invoice_screen/controller/invoice_model.dart';
import 'package:screenshot/screenshot.dart';
import '../../../core/resources/assets_resources.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../widgets/button/button_widget.dart';

class InvoiceDetailScreen extends StatefulWidget {
  final String paid;
  final String major;
  final String price;
  final String studentName;
  final String bacode;
  final String createDate;
  final String expiredDate;
  final String paymentNumber;
  final String invoiceNumber;
  final String currency;
  const InvoiceDetailScreen(
      {Key? key, required this.paid, required this.major, required this.price, required this.studentName, required this.bacode, required this.createDate, required this.paymentNumber, required this.invoiceNumber, required this.currency, required this.expiredDate})
      : super(key: key);

  @override
  State<InvoiceDetailScreen> createState() => _InvoiceDetailScreenState();
}

class _InvoiceDetailScreenState extends State<InvoiceDetailScreen> {
  final dataivoice = InvoiceData.generate();
  final screenShotColler = ScreenshotController();

  bool isWaiting = false;

  Future<String> saveImageToLocal(
    Uint8List btye,
  ) async {
    await [Permission.storage].request();
    // final result = await ImageGallerySaver.saveImage(image);

    // print("File Path: ${result}");
    // return result['filePath']!;
    final directory = await getApplicationDocumentsDirectory();
    final image = File('${directory.path}/flutter.png');
    image.writeAsBytesSync(btye);
    GallerySaver.saveImage(image.path, albumName: "RUPP");
    return image.path;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  top: 12, bottom: 15, left: 25, right: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      child: SvgPicture.asset(
                        ImageAssets.arrow_back_icon,
                        width: 22,
                        height: 22,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Expanded(
                    child: Container(
                      child: Text(
                        "វិក្កយបត្រលម្អិត",
                        style: ThemeConstant.texttheme.headline6!.copyWith(
                            color: ColorContant.ducktextColor,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: SvgPicture.asset(
                      ImageAssets.arrow_back_icon,
                      width: 22,
                      height: 22,
                      color: Colors.transparent,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(left: 17, right: 17),
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      bottom: 55,
                      left: 0,
                      right: 0,
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 2,
                                color: widget.paid == "Open"
                                    ? ColorContant.primaryColor
                                    : ColorContant.secondaryColor),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(13))),
                        child: SingleChildScrollView(
                          child: Container(
                            padding: const EdgeInsets.only(
                                left: 12, top: 12, right: 12),
                            child: Column(
                              children: [
                                InvoiceDetailBodyWidget(
                                  isScreenShot: false,
                                  widget: widget,
                                  datainvoice: dataivoice,
                                ),
                                Container(
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Row(
                                    children: [
                                      Container(
                                          child: Text(
                                        "ចំណាំ៖",
                                        style: ThemeConstant
                                            .texttheme.subtitle1!
                                            .copyWith(
                                                color: widget.paid == "Paid"
                                                    ? ColorContant
                                                        .secondaryColor
                                                    : ColorContant.primaryColor,
                                                fontWeight: FontWeight.w400),
                                        textAlign: TextAlign.left,
                                      )),
                                      Expanded(
                                        child: Divider(
                                          color: widget.paid == "Paid"
                                              ? ColorContant.secondaryColor
                                              : ColorContant.primaryColor,
                                          thickness: 1,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 8),
                                  child: Column(
                                    children: [
                                      Container(
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                                child: Text(
                                              "1. ",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            )),
                                            Expanded(
                                                child: Text(
                                              "លោកអ្នកក្លាយជានិស្សិតផ្លវការរបស់ RUPP លុះត្រាតែលោកអ្នកបានបង់ប្រាក់ថ្លៃសិក្សា រួចរាល់។",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            ))
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                                child: Text(
                                              "2. ",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            )),
                                            Expanded(
                                                child: Text(
                                              "លោកអ្នកត្រូវធ្វើការបង់ប្រាក់ឲ្យបានមុន ថ្ងៃផុតកំណត់នៃការទទួលពាក្យ។",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            ))
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                                child: Text(
                                              "3. ",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            )),
                                            Expanded(
                                                child: Text(
                                              "សាលានឹងមិនទទួលយកការបង់ប្រាក់របស់ លោកអ្នកឡើយ ប្រសិនបើដេប៉ាតឺម៉ង់ដែលអ្នក បានដាក់ពាក្យនោះ ទទួលបាននិស្សិតផ្លូវការ គ្រប់ចំនួនហើយនោះ ទោះជាកាលបរិច្ឆេទ ទទួលពាក្យមិន ទាន់ផុតកំណត់ក៏ដោយ។",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            ))
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                                child: Text(
                                              "4. ",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            )),
                                            Expanded(
                                                child: Text(
                                              "ប្រសិនបើនិស្សិតចង់ចូលប្រើប្រាស់ប្រព័ន្ធឡើងវិញ សូមប្រើប្រាស់លេខកូដ 55435 និង ពាក្យសម្ងាត់ 123456",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.left,
                                            ))
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 50,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    widget.paid == "Open"
                        ? Positioned(
                            bottom: 30,
                            left: 0,
                            right: 0,
                            child: Center(
                              child: ButtonWidget(
                                buttonColor: ColorContant.primaryColor,
                                textStyleButton:
                                    ThemeConstant.texttheme.subtitle1!.copyWith(
                                        color: ColorContant.whiteColor,
                                        fontWeight: FontWeight.w700),
                                weightButton: 100,
                                panddingVerButton: 8,
                                panddinHorButton: 16,
                                onTap: () async {
                                  try {
                                    setState(() {
                                      isWaiting = true;
                                    });
                                    final image = await screenShotColler
                                        .captureFromWidget(Container(
                                      color: ColorContant.whiteColor,
                                      padding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              20,
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              20,
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              20),
                                      child: InvoiceDetailBodyWidget(
                                        isScreenShot: true,
                                        widget: widget,
                                        datainvoice: dataivoice,
                                      ),
                                    ));
                                    setState(() {
                                      isWaiting = false;
                                    });
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            AlertDialog(
                                              scrollable: true,
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 40),
                                                    child: Image.memory(
                                                      image,
                                                    ),
                                                  ),
                                                  InkWell(
                                                    onTap: () async {
                                                      try {
                                                        await saveImageToLocal(
                                                            image);
                                                        ScaffoldMessenger.of(
                                                                context)
                                                            .showSnackBar(
                                                                const SnackBar(
                                                                    content: Text(
                                                                        "Image saved successfully")));
                                                        Navigator.of(context)
                                                            .pop();
                                                      } catch (e) {
                                                        Navigator.of(context)
                                                            .pop();
                                                        ScaffoldMessenger.of(
                                                                context)
                                                            .showSnackBar(
                                                                const SnackBar(
                                                                    content: Text(
                                                                        "Image saved successfully")));
                                                      }
                                                    },
                                                    child: Container(
                                                      margin: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 5),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8),
                                                            decoration: BoxDecoration(
                                                                color: ColorContant
                                                                    .duckGreytextColor
                                                                    .withOpacity(
                                                                        0.2),
                                                                borderRadius:
                                                                    const BorderRadius
                                                                            .all(
                                                                        Radius.circular(
                                                                            20))),
                                                            child: const Icon(
                                                              Icons
                                                                  .save_alt_rounded,
                                                              color: ColorContant
                                                                  .secondaryColor,
                                                              size: 25,
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            width: 10,
                                                          ),
                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Text(
                                                                  "ទាញយករូបភាព",
                                                                  style: ThemeConstant
                                                                      .texttheme
                                                                      .caption
                                                                      ?.copyWith(
                                                                          color: ColorContant
                                                                              .secondaryColor,
                                                                          fontWeight:
                                                                              FontWeight.bold),
                                                                ),
                                                                Text(
                                                                  "ចាប់យក និងទាញរូបភាពព័ត៌មានលម្អិតអំពីប្រតិបត្តិការណ៍របស់អ្នក",
                                                                  style: ThemeConstant
                                                                      .texttheme
                                                                      .bodyText2
                                                                      ?.copyWith(
                                                                          color: ColorContant.ducktextColor.withOpacity(
                                                                              0.5),
                                                                          fontSize:
                                                                              9),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ));
                                  } catch (e) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        const SnackBar(
                                            content: Text(
                                                "Image saved successfully")));
                                  }
                                },
                                title: "ថតទុក",
                                radiusButton: 10,
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class InvoiceDetailBodyWidget extends StatelessWidget {
  const InvoiceDetailBodyWidget({
    Key? key,
    required this.widget,
    required this.datainvoice,
    required this.isScreenShot,
  }) : super(key: key);

  final InvoiceDetailScreen widget;
  final List<InvoiceData> datainvoice;
  final bool isScreenShot;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                width: 8,
              ),
              Expanded(
                child: Container(
                    child: Column(
                  children: [
                    Container(
                      child: Text(
                        "កាលបរិច្ឆេទចុះឈ្មោះ៖",
                        style: ThemeConstant.texttheme.subtitle1!.copyWith(
                            color: ColorContant.duckDisableColor,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        widget.createDate.toString(),
                        style: ThemeConstant.texttheme.headline6!.copyWith(
                            color: ColorContant.ducktextColor,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        "និស្សិតឈ្មោះ៖",
                        style: ThemeConstant.texttheme.subtitle1!.copyWith(
                            color: ColorContant.duckDisableColor,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        widget.studentName.toString(),
                        style: ThemeConstant.texttheme.headline6!.copyWith(
                            color: ColorContant.ducktextColor,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                )),
              ),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                child: Container(
                    child: Column(
                  children: [
                    Container(
                      child: Text(
                        "ថ្ងៃផុតកំណត់ទទួលពាក្យ៖",
                        style: ThemeConstant.texttheme.subtitle1!.copyWith(
                            color: widget.paid == "Paid"
                                ? ColorContant.secondaryColor
                                : ColorContant.primaryColor,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        widget.expiredDate,
                        style: ThemeConstant.texttheme.headline6!.copyWith(
                            color: widget.paid == "Paid"
                                ? ColorContant.secondaryColor
                                : ColorContant.primaryColor,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        "និស្សិតឆ្នាំទី៖",
                        style: ThemeConstant.texttheme.subtitle1!.copyWith(
                            color: ColorContant.duckDisableColor,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        "១",
                        style: ThemeConstant.texttheme.headline6!.copyWith(
                            color: ColorContant.ducktextColor,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                )),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: Text(
            "ដេប៉ាតេម៉ង់/ជំនាញ៖",
            style: ThemeConstant.texttheme.subtitle1!.copyWith(
                color: ColorContant.duckDisableColor,
                fontWeight: FontWeight.w400),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          child: Text(
            widget.major,
            style: ThemeConstant.texttheme.headline6!.copyWith(
                color: ColorContant.ducktextColor, fontWeight: FontWeight.w700),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          child: Row(
            children: [
              isScreenShot
                  ? SizedBox(
                      width: 105,
                    )
                  : Transform.rotate(
                      angle: -0.5,
                      child: Container(
                        height: 105,
                        width: 105,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: widget.paid == "Paid"
                              ? ColorContant.secondaryColor
                              : ColorContant.primaryColor,
                        ),
                        child: Center(
                            child: Text(
                          "មិនទាន់បង់",
                          style: ThemeConstant.texttheme.headline6!.copyWith(
                              color: ColorContant.whiteColor,
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        )),
                      ),
                    ),
              Expanded(
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 8.0),
                        child: Text(
                          "សរុបទឹកប្រាក់ត្រូវបង់៖",
                          style: ThemeConstant.texttheme.subtitle1!.copyWith(
                              color: ColorContant.duckDisableColor,
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    //   Container(
                    //   child: Text(
                    //     "ថ្ងៃផុតកំណត់ទទួលពាក្យ៖",
                    //     style: ThemeConstant.texttheme.subtitle1!.copyWith(
                    //         color: widget.paid == "Paid"
                    //             ? ColorContant.secondaryColor
                    //             : ColorContant.primaryColor,
                    //         fontWeight: FontWeight.w400),
                    //     textAlign: TextAlign.center,
                    //   ),
                    // ),
                      Container(
                        child: Text(
                          "${widget.price} ${widget.currency}",
                          style: ThemeConstant.texttheme.headline2!.copyWith(
                              color: widget.paid == "Paid"
                                  ? ColorContant.secondaryColor
                                  : ColorContant.primaryColor,
                              height: 1.4,
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 105,
                width: 105,
              )
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 5, top: 0, left: 8, right: 8),
          child: Container(
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: const Divider(
                    height: 1,
                    color: ColorContant.ducktextColor,
                    thickness: 0.8,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(left: 40, right: 40),
                  child: Center(
                      child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          color: ColorContant.whiteColor,
                          child: Text(
                            "លេងកូដសម្រាប់បង់នៅធានាគារ FTB ឬ នៅបញ្ជរផ្ទាល់របស់ RUPP បន្ទប់​105 ឬ 107 អគារA ជាន់ទី​​1",
                            style: ThemeConstant.texttheme.subtitle1!.copyWith(
                                color: ColorContant.ducktextColor,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ))),
                ),
              ],
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10,),
          child: Image.network(
            widget.bacode.toString(),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 8,top: 15,),
          child: Text(
            widget.paymentNumber,
            style: ThemeConstant.texttheme.headline4!.copyWith(
                color: widget.paid == "Paid"
                    ? ColorContant.secondaryColor
                    : ColorContant.primaryColor,
                height: 1.4,
                fontWeight: FontWeight.w700),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 5, top: 0, left: 8, right: 8),
          child: Container(
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: const Divider(
                    height: 1,
                    color: ColorContant.ducktextColor,
                    thickness: 0.8,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(left: 40, right: 40),
                  child: Center(
                      child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          color: ColorContant.whiteColor,
                          child: Text(
                            "លេងកូដសម្រាប់បង់តាម​ App ធានាគារ ABA ឬ Acleda",
                            style: ThemeConstant.texttheme.subtitle1!.copyWith(
                                color: ColorContant.ducktextColor,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ))),
                ),
              ],
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
              color: widget.paid == "Paid"
                  ? ColorContant.secondaryColor
                  : ColorContant.primaryColor,
              border: Border.all(
                  color: widget.paid == "Paid"
                      ? ColorContant.secondaryColor
                      : ColorContant.primaryColor),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.only(top: 6, bottom: 3),
                  decoration: const BoxDecoration(
                      color: ColorContant.whiteColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10))),
                  child: Text(
                    widget.invoiceNumber.toString(),
                    style: ThemeConstant.texttheme.headline5!.copyWith(
                        color: widget.paid == "Paid"
                            ? ColorContant.secondaryColor
                            : ColorContant.primaryColor,
                        height: 1.4,
                        fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              isScreenShot
                  ? Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 12),
                      child: SvgPicture.asset(
                        ImageAssets.copy_icon,
                      ),
                    )
                  : InkWell(
                      onTap: () {
                        Clipboard.setData(
                             ClipboardData(text: widget.invoiceNumber.toString()));
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          duration: const Duration(milliseconds: 500),
                          width: 100,
                          elevation: 2.0,
                          behavior: SnackBarBehavior.floating,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          content: Wrap(
                            children: [
                              Container(
                                child: Center(
                                  child: Text(
                                    'Copied',
                                    style: ThemeConstant.texttheme.subtitle1!
                                        .copyWith(
                                            color: ColorContant.whiteColor),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ));
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 12),
                        child: SvgPicture.asset(
                          ImageAssets.copy_icon,
                        ),
                      ),
                    )
            ],
          ),
        ),
      ],
    );
  }
}
