import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rupp_application/app/core/constands/constands_color.dart';
import 'package:rupp_application/app/modules/invoice_screen/bloc/get_invoice_bloc.dart';
import 'package:rupp_application/app/modules/invoice_screen/view/invoice_detail_scren.dart';
import 'package:rupp_application/app/widgets/no_connection_alert_widget.dart';
import 'package:rupp_application/storages/user_storage.dart';
import '../../../core/resources/assets_resources.dart';
import '../../../core/thems/thems_constands.dart';
import '../../../routes/app_routes.dart';
import '../../../widgets/button/button_widget.dart';
import '../../../widgets/toast_nointernet.dart';

class InvoiceScreen extends StatefulWidget {
  int routType;
  InvoiceScreen({Key? key, required this.routType}) : super(key: key);

  @override
  State<InvoiceScreen> createState() => _InvoiceScreenState();
}

class _InvoiceScreenState extends State<InvoiceScreen> {
  UserSecureStroage _prefs = UserSecureStroage();
   bool connection= true;
   StreamSubscription? sub;
  void getApi(){
    _prefs.setStep(step: "1");
    BlocProvider.of<GetInvoiceBloc>(context).add(InvoiceRequestEvent());
  }
  @override
  void initState() {
    //=============Check internet====================
       sub = Connectivity().onConnectivityChanged.listen((event) {
        setState(() {
        connection = (event != ConnectivityResult.none);
        if(connection == true){
          setState(() {
          });
        }
        else{
          setState(() {
            connection = false;
          });
        }
      });
    });
   
   //=============Check internet====================
   getApi();
    super.initState();
  }
  @override
  void dispose() {
    sub!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorContant.whiteColor,
      body: SafeArea(
        bottom: false,
        child: WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: BlocBuilder<GetInvoiceBloc, GetInvoiceState>(
            builder: (context, state) {
              if(state is GetInvoiceLoading){
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if(state is GetInvoiceLoaded){
                var data = state.getInvoiceModel.data;
                return data!.isEmpty? Container(child:const Center(child: Text("No Data"))):Column(
                children: [
                  connection ==false?const NoConnectWidget(): Container(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 12, bottom: 15, left: 25, right: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            if (widget.routType == 2) {
                              Navigator.of(context).pop();
                            }
                            if (widget.routType == 1) {
                              if(connection==false){
                                exit(0);
                              }
                              else{
                                Navigator.pushNamed(
                                  context, Routes.WELLCOMESCREEN);
                              }
                              
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            child: SvgPicture.asset(
                              ImageAssets.arrow_back_icon,
                              width: 22,
                              height: 22,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "វិក្កយបត្រ",
                              style: ThemeConstant.texttheme.headline6!
                                  .copyWith(
                                      color: ColorContant.ducktextColor,
                                      fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 2,
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: SvgPicture.asset(
                            ImageAssets.arrow_back_icon,
                            width: 22,
                            height: 22,
                            color: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Text(
                          "និស្សិតឈ្មោះ៖",
                          style: ThemeConstant.texttheme.subtitle1!.copyWith(
                              color: ColorContant.duckDisableColor,
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.right,
                        ),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Container(
                        child: Text(
                          data[0].studentFullname.toString(),
                          style: ThemeConstant.texttheme.headline6!.copyWith(
                              color: ColorContant.ducktextColor,
                              fontWeight: FontWeight.w700),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 26, vertical: 0),
                      child: Column(
                        children: [
                          Expanded(
                            child: ListView.builder(
                              padding:
                                  const EdgeInsets.only(top: 15, bottom: 5),
                              shrinkWrap: true,
                              itemCount: data.length,
                              itemBuilder: (context, index) {
                                String barcode = data[index].payments![0].paymentCodeBarCode.toString();
                                return Container(
                                  margin: const EdgeInsets.only(bottom: 22),
                                  padding: const EdgeInsets.only(left: 9),
                                  decoration: BoxDecoration(
                                      color: data[index].payments![0].status== "Open"
                                          ? ColorContant.primaryColor
                                          : ColorContant.secondaryColor,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(13))),
                                  child: MaterialButton(
                                    elevation: 0,
                                    color: ColorContant.whiteColor,
                                    padding: const EdgeInsets.all(0),
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(13))),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                          pageBuilder: (context, animation1,
                                                  animation2) =>
                                              InvoiceDetailScreen(
                                                  paid: data[index].payments![0].status.toString(),
                                                  price: data[index].payments![0].totalAmount.toString(),
                                                  major: data[index].majorName.toString(),
                                                  createDate: data[index].registrationDate.toString(),
                                                  expiredDate: data[index].expiredDate.toString(),
                                                  studentName: data[index].studentFullname.toString(),
                                                  currency: data[index].payments![0].items![0].currency.toString(),
                                                  bacode: barcode,
                                                  paymentNumber: data[index].payments![0].paymentCode.toString(),
                                                  invoiceNumber: data[index].payments![0].invoiceNumber.toString(),
                                                ),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration:
                                              Duration.zero,
                                        ),
                                      );
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(13)),
                                        border: Border.all(
                                            width: 2,
                                            color: data[index].payments![0].status ==
                                                    "Open"
                                                ? ColorContant.primaryColor
                                                : ColorContant.secondaryColor),
                                      ),
                                      child: Container(
                                        margin: const EdgeInsets.only(
                                          left: 9,
                                        ),
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 16),
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(10),
                                                bottomRight:
                                                    Radius.circular(10))),
                                        child: Row(
                                          children: [
                                            const SizedBox(
                                              width: 8,
                                            ),
                                            Expanded(
                                              child: Container(
                                                  child: Column(
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      "កាលបរិច្ឆេទចុះឈ្មោះ៖",
                                                      style: ThemeConstant
                                                          .texttheme.subtitle1!
                                                          .copyWith(
                                                              color: ColorContant
                                                                  .duckDisableColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      data[index]
                                                          .registrationDate
                                                          .toString(),
                                                      style: ThemeConstant
                                                          .texttheme.headline6!
                                                          .copyWith(
                                                              color: ColorContant
                                                                  .ducktextColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      "ដេប៉ាតេម៉ង់/ជំនាញ៖",
                                                      style: ThemeConstant
                                                          .texttheme.subtitle1!
                                                          .copyWith(
                                                              color: ColorContant
                                                                  .duckDisableColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      data[index]
                                                          .majorName
                                                          .toString(),
                                                      style: ThemeConstant
                                                          .texttheme.headline6!
                                                          .copyWith(
                                                              color: ColorContant
                                                                  .ducktextColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ],
                                              )),
                                            ),
                                            const SizedBox(
                                              width: 8,
                                            ),
                                            Expanded(
                                              child: Container(
                                                  child: Column(
                                                children: [
                                                  Container(
                                                    child: Text(
                                                      "ស្ថានភាពវិក្កបត្រ៖",
                                                      style: ThemeConstant
                                                          .texttheme.subtitle1!
                                                          .copyWith(
                                                              color: ColorContant
                                                                  .duckDisableColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      data[index].payments![0].status ==
                                                              "Paid"
                                                          ? "ទូទាត់រួចរាល់"
                                                          : "មិនទាន់បង់",
                                                      style: ThemeConstant
                                                          .texttheme.headline6!
                                                          .copyWith(
                                                              color: data[index].payments![0].status ==
                                                              "Open"
                                                                  ? ColorContant
                                                                      .primaryColor
                                                                  : ColorContant
                                                                      .secondaryColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      "សរុបទឹកប្រាក់ត្រូវបង់៖",
                                                      style: ThemeConstant
                                                          .texttheme.subtitle1!
                                                          .copyWith(
                                                              color: ColorContant
                                                                  .duckDisableColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                      textAlign:TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text( 
                                                      data[index]
                                                          .payments![0].totalAmount.toString(),
                                                      style: ThemeConstant
                                                          .texttheme.headline2!
                                                          .copyWith(
                                                              color: data[index].payments![0].status ==
                                                              "Open"
                                                                  ? ColorContant
                                                                      .primaryColor
                                                                  : ColorContant
                                                                      .secondaryColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      data[index]
                                                          .payments![0].items![0].currency.toString(),
                                                      style: ThemeConstant
                                                          .texttheme.headline6!
                                                          .copyWith(
                                                            height: 1,
                                                              color: data[index].payments![0].status ==
                                                              "Open"
                                                                  ? ColorContant
                                                                      .primaryColor
                                                                  : ColorContant
                                                                      .secondaryColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                  
                                                ],
                                              )),
                                            ),
                                            Container(
                                              width: 30,
                                              child: Icon(
                                                Icons.arrow_forward_ios_sharp,
                                                size: 22,
                                                color: data[index].payments![0].status ==
                                                              "Open"
                                                    ? ColorContant.primaryColor
                                                    : ColorContant
                                                        .secondaryColor,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                bottom: 15, top: 10, left: 8, right: 8),
                            child: Container(
                              child: Stack(
                                alignment: AlignmentDirectional.center,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    child: const Divider(
                                      height: 1,
                                      color: ColorContant.ducktextColor,
                                      thickness: 0.8,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(
                                        left: 40, right: 40),
                                    child: Center(
                                        child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 8),
                                            color: ColorContant.whiteColor,
                                            child: Text(
                                              "សម្រាប់និស្សិតរៀនជំនាញច្រើន",
                                              style: ThemeConstant
                                                  .texttheme.subtitle1!
                                                  .copyWith(
                                                      color: ColorContant
                                                          .ducktextColor,
                                                      fontWeight:
                                                          FontWeight.w400),
                                              textAlign: TextAlign.center,
                                            ))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          ButtonWidget(
                            buttonColor: ColorContant.primaryColor,
                            textStyleButton: ThemeConstant.texttheme.headline6!
                                .copyWith(
                                    color: ColorContant.whiteColor,
                                    fontWeight: FontWeight.w700),
                            //weightButton: 100,
                            panddingVerButton: 12,
                            panddinHorButton: 16,
                            onTap:connection==false?(){showtoastInternet(context);}:() {
                              Navigator.of(context)
                                  .pushNamed(Routes.CHOOSEDEPARTMENTSCREEN);
                            },
                            title: "ចុះឈ្មោះជំនាញថ្មី",
                            radiusButton: 10,
                          ),
                          const SizedBox(
                            height: 35,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              );
              }
              else{
                return const Center(
                  child: Text("Error"),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
