import 'package:flutter/material.dart';

class ColorContant {
  static const primaryColor = Color(0xFFEE3436);
  static const secondaryColor = Color(0xFF2278F8);
  static const duckGreytextColor = Color(0xFF6B7588);
  static const ducktextColor = Color(0xFF3A3A3C);
  static const whiteColor = Color(0xFFFFFFFF);
  static const duckDisableColor = Color(0xFF6B7588);
  static const duckoptionfullColor = Color(0xFF979797);
}