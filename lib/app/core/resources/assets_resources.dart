// ignore: constant_identifier_names
// ignore_for_file: constant_identifier_names

const String ICONS_PATH_SVG = "assets/icons/svg";

class ImageAssets{
  static const String translate_icon = '$ICONS_PATH_SVG/icon_translate.svg';
  static const String copy_icon = '$ICONS_PATH_SVG/icon_copy.svg';
  static const String arrow_back_icon = '$ICONS_PATH_SVG/arrow_back.svg';
}