import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rupp_application/app/models/get_major_model/major_model.dart';
import 'package:rupp_application/app/models/get_session/get_session_model.dart';
import 'package:rupp_application/app/models/province_model/province_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserSecureStroage {
//===============Key=========================
//=============//=============//===================
  static const _keyfristname = "fristname";
  static const _keylastname = "lastname";
  static const _keyfullnameen = "fullname";
  static const _keygender = "gendr";
  static const _keydateofbrith = "date_of_birth";
  static const _Keyindexprovince = "index_province_selected";
  static const _hightschool = "hightschool";
  static const _yearexam = "yearexam";
  static const _certificateexam = "certificate_exam";
  static const _phonenumber = "phonenumber";
  static const _keyprovinceModel = "provincemodel";
  static const _majorModel = "majormodel";
  static const _indexfaculty = "indexfaculty";
  static const _indexmajor = "indexmajor";
  static const _keyloginsucess = "loginsucess";
  static const _studentid = "studentid";
  static const _indesSesion = "indexsession";
  static const _sesionModel = "sessionmodel";
  static const _payType = "paytype";
  static const _priceSelected = "priceSelected";
  static const _oriented_subject_1_grade = "orientedsubject1grade";
  static const _oriented_subject_2_grade = "orientedsubject2grade";
  static const _orientedgrade = "orientedgrade";
  static const _averageexam = "averageexam";
  static const _keyusername = "usernamekye";
  static const _keypassword = "passwordkey";
  static const _keystep = "step";
  static const _token = "token";

//==================Set to Storage ==========================//===========//==============
//=============//=============//=============================//===========//==============
  void setToken({required String token}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref.setString(_token, token).then((value) =>
          debugPrint("Token has store in local storage successful $value"));
    } catch (e) {
      debugPrint("Token was fail in local storage ");
    }
  }

  void setMajorModel({required String majorModel}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_majorModel, majorModel);
    } catch (e) {}
  }

  void setSessionModel({required String sessionModel}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_sesionModel, sessionModel);
    } catch (e) {}
  }

  void setProvinceModel({required String provinceModel}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keyprovinceModel, provinceModel);
      print("Secceusfdjasdfk Holy");
    } catch (e) {}
  }

  void setFristName({required String fristname}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keyfristname, fristname);
    } catch (e) {}
  }

  void setLastName({required String lastname}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keylastname, lastname);
    } catch (e) {}
  }

  void setFullNameLatin({required String fullLatinname}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keyfullnameen, fullLatinname);
    } catch (e) {}
  }

  void setLoginSucess({required String loginsucess}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keyloginsucess, loginsucess);
    } catch (e) {}
  }

  void setIndexFaculty({required int indexFaculty}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setInt(_indexfaculty, indexFaculty);
    } catch (e) {}
  }

  void setIndexMajors({required int indexMajor}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setInt(_indexmajor, indexMajor);
    } catch (e) {}
  }

  void setIndexSession({required int indexSession}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setInt(_indesSesion, indexSession);
    } catch (e) {}
  }

  void setStudentId({required String studentId}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_studentid, studentId);
    } catch (e) {}
  }

  void setPayType({required String payType}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_payType, payType);
    } catch (e) {}
  }

  void setPriceSelected({required String priceSelected}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_priceSelected, priceSelected);
    } catch (e) {}
  }

  void setOrientedSubject1Grade({required String OrientedSubject1Grade}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_oriented_subject_1_grade, OrientedSubject1Grade);
    } catch (e) {}
  }

  void setOrientedSubject2Grade({required String OrientedSubject2Grade}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_oriented_subject_2_grade, OrientedSubject2Grade);
    } catch (e) {}
  }

  void setOrientedGrade({required String OrientedGrade}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_orientedgrade, OrientedGrade);
    } catch (e) {}
  }

  void setAverageExam({required String averageExam}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_averageexam, averageExam);
    } catch (e) {}
  }

  void setGender({required String gender}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keygender, gender);
    } catch (e) {}
  }

  void setDateOfBirth({required String birth}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keydateofbrith, birth);
    } catch (e) {}
  }

  void setIndexProvince({required String indexProvince}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_Keyindexprovince, indexProvince);
    } catch (e) {}
  }

  void setHightSchool({required String hightSchool}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_hightschool, hightSchool);
    } catch (e) {}
  }

  void setYearExam({required String yearExam}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_yearexam, yearExam);
    } catch (e) {}
  }

  void setCertificateExam({required String certificateExam}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_certificateexam, certificateExam);
    } catch (e) {}
  }

  void setPhoneNumber({required String phoneNumber}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_phonenumber, phoneNumber);
    } catch (e) {}
  }
  void setusernameLogin({required String usernamelogin}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keyusername, usernamelogin);
    } catch (e) {}
  }
  void setpassword({required String password}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keypassword, password);
    } catch (e) {}
  }
  void setStep({required String step}) async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      _prefs.setString(_keystep, step);
    } catch (e) {}
  }

//==================Get from Storage ================//===========//==============
//=============//=============//==========================//===========//=========

  Future<MajorModel> get getMajorModel async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var datamajor = _prefs.getString(_majorModel) ?? "";
      MajorModel? majorModel;
      if (datamajor != null) {
        var result = json.decode(datamajor.toString());
        majorModel = MajorModel.fromJson(result);
        return majorModel;
      }
      return majorModel ?? MajorModel();
    } catch (e) {
      throw Exception("Get Major Unsuccess");
    }
  }

  Future<SessionModel> get getSessionModel async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var datasession = _prefs.getString(_sesionModel) ?? "";
      SessionModel? sessionModel;
      if (datasession != null) {
        var result = json.decode(datasession.toString());
        sessionModel = SessionModel.fromJson(result);
        return sessionModel;
      }
      return sessionModel ?? SessionModel();
    } catch (e) {
      throw Exception("Get Major Unsuccess");
    }
  }

  Future<ProvinceModel> get getProvinceModel async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var dataprovince = _prefs.getString(_keyprovinceModel) ?? "";
      ProvinceModel? dataprovinceModel;
      if (dataprovince != null) {
        var result = json.decode(dataprovince.toString());
        dataprovinceModel = ProvinceModel.fromJson(result);
        return dataprovinceModel;
      }
      return dataprovinceModel ?? ProvinceModel();
    } catch (e) {
      throw Exception("Get Major Unsuccess");
    }
  }

  Future<String?> getFristName() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var FristName = _prefs.getString(_keyfristname.toString()) ?? "";
      if (FristName != "") {
        return FristName;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getLastName() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var lastName = _prefs.getString(_keylastname.toString()) ?? "";
      if (lastName != "") {
        return lastName;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getFullNameEn() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var fullName = _prefs.getString(_keyfullnameen.toString()) ?? "";
      if (fullName != "") {
        return fullName;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getLoginSucess() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var accesstoken = _prefs.getString(_keyloginsucess.toString()) ?? "";
      if (accesstoken != "") {
        return accesstoken;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<int?> getIndexFaculty() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var indexFaculty = _prefs.getInt(_indexfaculty);
      if (indexFaculty != null) {
        return indexFaculty;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<int?> getIndexMajors() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var indexMajors = _prefs.getInt(_indexmajor);
      if (indexMajors != null) {
        return indexMajors;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<int?> getIndexSession() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var indexSession = _prefs.getInt(_indesSesion);
      if (indexSession != null) {
        return indexSession;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getStudentId() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var StudentId = _prefs.getString(_studentid.toString()) ?? "";
      if (StudentId != "") {
        return StudentId;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getPayType() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var payType = _prefs.getString(_payType.toString()) ?? "";
      if (payType != "") {
        return payType;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getPriceSelected() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var price = _prefs.getString(_priceSelected.toString()) ?? "";
      if (price != "") {
        return price;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getOrientedSubject1Grade() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var gradeoriented1 =
          _prefs.getString(_oriented_subject_1_grade.toString()) ?? "";
      if (gradeoriented1 != "") {
        return gradeoriented1;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getOrientedSubject2Grade() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var gradeoriented2 =
          _prefs.getString(_oriented_subject_2_grade.toString()) ?? "";
      if (gradeoriented2 != "") {
        return gradeoriented2;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getOrientedGrade() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var gradeoriented = _prefs.getString(_orientedgrade.toString()) ?? "";
      if (gradeoriented != "") {
        return gradeoriented;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getAverageExam() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var average = _prefs.getString(_averageexam.toString()) ?? "";
      if (average != "") {
        return average;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getGender() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var gender = _prefs.getString(_keygender.toString()) ?? "";
      if (gender != "") {
        return gender;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getDateOfBirht() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var birht = _prefs.getString(_keydateofbrith.toString()) ?? "";
      if (birht != "") {
        return birht;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getIndexProvince() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var indexpro = _prefs.getString(_Keyindexprovince.toString()) ?? "";
      if (indexpro != "") {
        return indexpro;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getHightSchool() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var hightSchool = _prefs.getString(_hightschool.toString()) ?? "";
      if (hightSchool != "") {
        return hightSchool;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getYearExam() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var examyear = _prefs.getString(_yearexam.toString()) ?? "";
      if (examyear != "") {
        return examyear;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getCertificateExam() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var certificate = _prefs.getString(_certificateexam.toString()) ?? "";
      if (certificate != "") {
        return certificate;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getPhoneNumber() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var phone = _prefs.getString(_phonenumber.toString()) ?? "";
      if (phone != "") {
        return phone;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }
  Future <String?> getUserNameLogin() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var username = _prefs.getString(_keyusername.toString()) ?? "";
      if (username != "") {
        return username;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }
  Future <String?> getPaeewordLogin() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var password = _prefs.getString(_keypassword.toString()) ?? "";
      if (password != "") {
        return password;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }
  Future <String?> getStep() async {
    try {
      final SharedPreferences _prefs = await SharedPreferences.getInstance();
      var step = _prefs.getString(_keystep.toString()) ?? "";
      if (step != "") {
        return step;
      } else {
        return "";
      }
    } catch (e) {
      return null;
    }
  }
  
}
