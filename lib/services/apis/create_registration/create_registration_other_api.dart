import 'package:rupp_application/storages/user_storage.dart';

import '../../../app/models/user_register_model/user_register_model.dart';
import '../../../app/models/user_register_model/user_registration_other_model.dart';
import '../../base_url/base_url.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class CreateRegistrationOther {
  final BaseUrl _baseUrl = BaseUrl();
  UserSecureStroage _prefs = UserSecureStroage();
  Future<CreateOtherRegistrationModel> postRegistrationOther({
     required String? grade_subject_orientation_1,
     required String? grade_subject_orientation_2,
     required String? degree_id,
     required String? major_id,
     required String? shift_session_id,
     required String? stage_id,
     required String? academicyear_id,
     required String? pay_type,
     required String? studentid,

  }) async {
    var token = await _prefs.getLoginSucess();
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/create-registration-other-major".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE",
          "Authorization": "Bearer $token"
        },
      body: {
        "student_id": studentid.toString() ,
        "grade_subject_orientation_1": grade_subject_orientation_1.toString()  ,
        "grade_subject_orientation_2": grade_subject_orientation_2.toString()  ,
        "degree_id": degree_id.toString()  ,
        "major_id": major_id.toString()  ,
        "shift_session_id": shift_session_id.toString()  ,
        "stage_id": stage_id.toString()  ,
        "academicyear_id": academicyear_id.toString()  ,
        "pay_type": pay_type.toString()  ,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return CreateOtherRegistrationModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}