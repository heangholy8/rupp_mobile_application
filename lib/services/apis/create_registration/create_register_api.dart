import '../../../app/models/user_register_model/user_register_model.dart';
import '../../base_url/base_url.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class CreateRegistration {
  final BaseUrl _baseUrl = BaseUrl();
  Future<UserRegisterModel> postRegistration({
     required String? lastname,
     required String? firstname,
     required String? fullname_en,
     required String? gender,
     required String? date_of_birth,
     required String? phone,
     required String? province_city_id,
     required String? name_high_school,
     required String? certificate_year,
     required String? certificate_number,
     required String? certificate_grade_score,
     required String? grade_orientation,
     required String? grade_subject_orientation_1,
     required String? grade_subject_orientation_2,
     required String? degree_id,
     required String? major_id,
     required String? shift_session_id,
     required String? stage_id,
     required String? academicyear_id,
     required String? pay_type,

  }) async {
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/create-registration".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE"
        },
      body: {
        "lastname": lastname.toString() ,
        "firstname": firstname.toString()  ,
        "fullname_en": fullname_en.toString()  ,
        "gender": gender.toString()  ,
        "date_of_birth": date_of_birth.toString()  ,
        "phone": phone.toString()  ,
        "province_city_id": province_city_id.toString()  ,
        "name_high_school": name_high_school.toString()  ,
        "certificate_year": certificate_year.toString()  ,
        "certificate_number": certificate_number.toString()  ,
        "certificate_grade_score": certificate_grade_score.toString()  ,
        "grade_orientation": grade_orientation.toString()  ,
        "grade_subject_orientation_1": grade_subject_orientation_1.toString()  ,
        "grade_subject_orientation_2": grade_subject_orientation_2.toString()  ,
        "degree_id": degree_id.toString()  ,
        "major_id": major_id.toString()  ,
        "shift_session_id": shift_session_id.toString()  ,
        "stage_id": stage_id.toString()  ,
        "academicyear_id": academicyear_id.toString()  ,
        "pay_type": pay_type.toString()  ,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return UserRegisterModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}