import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rupp_application/app/models/get_major_model/major_model.dart';
import 'package:rupp_application/storages/user_storage.dart';

import '../../base_url/base_url.dart';

class GetMajorApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<MajorModel> majorRequestApi() async {
    UserSecureStroage _prefs = UserSecureStroage();
    var studentid = await _prefs.getStudentId();
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/get-all-available-major".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE"
        },
      body: {
        "student_id": studentid.toString(),
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return MajorModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}