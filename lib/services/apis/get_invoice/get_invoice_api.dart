import 'dart:convert';

import 'package:http/http.dart' as http;
import '../../../app/models/invoice_model/invoice_model.dart';
import '../../../storages/user_storage.dart';
import '../../base_url/base_url.dart';

class GetInvoice {
  final BaseUrl _baseUrl = BaseUrl();
  Future<InvoiceModel> invoiceRequestApi() async {
    UserSecureStroage _prefs = UserSecureStroage();
    var accessToken = await _prefs.getLoginSucess();
    var studentid = await _prefs.getStudentId();
    http.Response response = await http.get(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/get-registrations/$studentid".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE",
          "Authorization":"Bearer $accessToken",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return InvoiceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}