import 'dart:convert';
import 'package:http/http.dart' as http;
import '../../../app/models/province_model/province_model.dart';
import '../../base_url/base_url.dart';

class GetProvinceApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<ProvinceModel> provinceRequestApi() async {
    http.Response response = await http.get(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/get-all-province".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE"
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return ProvinceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}