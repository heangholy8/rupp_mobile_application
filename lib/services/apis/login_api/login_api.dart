import 'dart:convert';

import 'package:rupp_application/app/models/login_model/login_model.dart';
import 'package:rupp_application/services/base_url/base_url.dart';
import 'package:http/http.dart' as http;

class LoginApi {
  final BaseUrl _baseUrl = BaseUrl();

  Future<LoginModel?> loginApi(
      {required String codeNumber, required String passWord}) async {
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/auth-login"),
      headers: <String, String>{
        "Accept": "application/json",
        "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE"
      },
      body: <String, String>{
        "student_code": codeNumber.toString(),
        "password": passWord.toString()
      },
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      // print(LoginModel.fromJson(jsonDecode(response.body)));
      return LoginModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception();
    }
  }
}
