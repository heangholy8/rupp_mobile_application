import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rupp_application/app/models/get_user_info_model/get_user_infor_model.dart';
import 'package:rupp_application/storages/user_storage.dart';
import '../../base_url/base_url.dart';

class GetUserInfoApi {
  final BaseUrl _baseUrl = BaseUrl();
  final UserSecureStroage _prefs = UserSecureStroage();
  Future<UserInforModel> UserInfoRequestApi() async {
    var token = await _prefs.getLoginSucess();
    http.Response response = await http.get(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/get-student-information".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE",
          "Authorization": "Bearer $token" 
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return UserInforModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}