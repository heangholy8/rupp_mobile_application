import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../app/models/get_session/get_session_model.dart';
import '../../../storages/user_storage.dart';
import '../../base_url/base_url.dart';

class GetSession {
  final BaseUrl _baseUrl = BaseUrl();
  Future<SessionModel> sessionRequestApi() async {
    UserSecureStroage _prefs = UserSecureStroage();
    var majormodel = await _prefs.getMajorModel;
    var indexFaculty = await _prefs.getIndexFaculty();
    var indexmajor = await _prefs.getIndexMajors();
    var studentid = await _prefs.getStudentId();
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.baseUrl}/student-mobile-api/v1/get-session-available-major".trim()),
      headers: <String, String>{
          "Accept": "application/json",
          "APPKEY": "L2tPsd3PJ26RQwuaQvNVyhabWqVcdE"
        },
      body: {
        "student_id": studentid.toString(),
        "major_id": majormodel.data![indexFaculty!.toInt()].majors![indexmajor!.toInt()].majorId.toString(),
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return SessionModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
      });
    }
  }
}